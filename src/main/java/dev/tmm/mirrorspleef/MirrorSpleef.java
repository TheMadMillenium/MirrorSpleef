package dev.tmm.mirrorspleef;

import dev.tmm.mirrorspleef.game.Game;
import dev.tmm.mirrorspleef.util.network.PacketInterceptor;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class MirrorSpleef extends JavaPlugin implements Listener {
    private Game game;
    private PacketInterceptor intp;

    @Override
    public void onEnable() {
        World world = Bukkit.getWorlds().get(0);

        this.game = new Game(this, world);

        this.getCommand("start").setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
                MirrorSpleef.this.game.start();

                return true;
            }
        });

        this.getCommand("end").setExecutor(new CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
                MirrorSpleef.this.game.end();

                return true;
            }
        });

        this.intp = new PacketInterceptor(this.getName());

        //Bukkit.getPluginManager().registerEvents(this.intp, this);
    }
}

/*
    - General Game Logic:
        - Mining speed (Not upgradable, just reasonable speeds)
    - Upgrades:
        - Perk duration/quantity
        - Perk potency (effect level, explosion strength, etc.)
        - Blocks in inventory
        - Lives count (From 3 lives to 8 lives)
        - Double jumps (From 0 djs to 7 djs)
        - Max number of collectibles in inventory
    - Collectibles:
        - All expire in 30 seconds
        - Refills:
            - Spawns only on a solid world
            - Can be destroyed by mining the world under it
            - Cannot be collected by players with perk items remaining
            - If a player attempts to camp the refill (stays within 5 blocks of the refill for 5 seconds), the refill will teleport to a new world
            - New refills spawn every 20 seconds, per team
        - Random Items:
            - Items from randomly chosen kits
            - Spawns only on a solid world
            - Can be destroyed by mining the world under it
            - Can be collected by any player, as long as their upgraded inventory space permits it
            - Can be thrown out of inventory to collect another item
            - One spawns every 20 seconds, independent of refills, per team
            - Spawn tiers:
                - First phase: Fully unmaxed
                - Second phase: Averagely maxed
                - Third phase: Maxed
    - Classes:
        - All classes are locked when player first joins
        - Classes are unlockable
        - Default:
            - Default class has nothing
            - No upgrades
        - Scout:
            - Unmaxed:
                - 16 seconds
                - Speed 1
            - Maxed:
                - 48 seconds
                - Speed 5
            - Upgrades:
                - +4 seconds per duration level
                - +1 to Speed per potency level
            - Potions is provided directly when a refill is picked up
        - Frog:
            - Unmaxed:
                - 16 seconds
                - Jump Boost 1
            - Maxed:
                - 48 seconds
                - Jump Boost 5
            - Upgrades:
                - +4 seconds per duration level
                - +1 to Jump Boost per potency level
                - Potion is provided directly when a refill is picked up
        - Engineer:
            - Unmaxed:
                - 2 traps
                - Explosive power 1
            - Maxed:
                - 10 traps
                - Explosive power 5
            - Upgrades:
                - +1 trap per quantity level
                - +1 power per potency level
            - Trap blocks:
                - Visible to opponent team for 1 second
                - Visible at all times to own team
                - Unbreakable by teammates
                - Disarmed by natural map degeneration
        - Miner:
            - Unmaxed:
                - +7% instamine time increase
                - 3 3x3 mining pick uses
            - Maxed:
                - +70% instamine time increase
                - 7 3x3 mining pick uses
            - Upgrades:
                - +9% time increase per duration level
                - +1 3x3 mining pick use per potency level
        - Perksmith:
            - Unmaxed:
                - 1 deployable refill
                - Cooldown of 30 seconds
            - Maxed:
                - 9 deployable refills
                - Cooldown of 16 seconds
            - Upgrades:
                - +1 refill per quantity level
                - -4 seconds on cooldown per potency level
        - Thief: (Not implemented because wither skulls are buggy)
            - Unmaxed:
                - 1 perk tappers
                - Each perk tapper steals 1 item
            - Maxed:
                - 9 perk tappers
                - Each perk tapper steals 5 items
            - Upgrades:
                - +1 perk tapper per quantity level
                - +1 item stolen per potency level
            - Items are stolen at random
            - Items stolen by the thief are stripped of all upgrades
        - Brawler:
            - Unmaxed:
                - 8 seconds on enemy side
                - Knockback 1 punch
            - Maxed:
                - 24 seconds on enemy side
                - Knockback 5 punch
            - Upgrades:
                - +2 seconds per duration level
                - +1 Knockback per potency level
            - Gets clickable item when ability is available
            - Cannot use any items while on opponent's side
            - Cannot pick up any items while on opponent's side
        - Random:
            - Game chooses a random class for the player
    */

