package dev.tmm.mirrorspleef.game;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.world.BlockSelection;
import dev.tmm.mirrorspleef.base.world.CoordBoundary;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.data.*;
import dev.tmm.mirrorspleef.game.inventory.EmptyInventory;
import dev.tmm.mirrorspleef.game.inventory.LobbyInventory;
import dev.tmm.mirrorspleef.game.inventory.PreGameInventory;
import dev.tmm.mirrorspleef.game.phases.InstamineSpawnPhase;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import dev.tmm.mirrorspleef.game.phases.base.PhaseGroup;
import dev.tmm.mirrorspleef.game.phases.base.PhaseSequence;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.List;

public class Game implements Listener {
    private final Plugin plugin;
    private final GameState state;
    private final World gameWorld;

    public Game(Plugin plugin, World world) {
        this.plugin = plugin;
        this.state = new GameState(plugin, world);
        this.gameWorld = world;

        this.state.setStage(GameStage.LOBBY);

        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void start() {
        this.setup();

        BukkitRunnable gameTick = new BukkitRunnable() {
            @Override
            public void run() {
                if (Game.this.state.isFinished()) {
                    this.cancel();
                    return;
                }

                Game.this.state.tick();
            }
        };

        gameTick.runTaskTimer(this.plugin, 0, 1);
    }

    public void setup() {
        for (Entity e : this.gameWorld.getEntities()) {
            if (!(e instanceof Player)) {
                e.remove();
            }
        }

        this.state.world.setOriginal(new Location(this.gameWorld, -21, 18, 179), new Location(this.gameWorld, 21, 82, 221));

        this.state.world.setStructureSrc(new Location(this.gameWorld, 379, 18, -21));
        this.state.world.setStructureRotation(StructureRotation.X_AXIS);
        this.state.world.setStructureLink(new Location(this.gameWorld, -379, 82, -21));

        this.state.world.reloadArenas();

        BlockSelection srcSelection = new BlockSelection(this.gameWorld, 379, 50, -21, 421, 50, 21);
        BlockSelection linkSelection = new BlockSelection(this.gameWorld, -421, 50, -21, -379, 50, 21);

        srcSelection.exclude(Material.AIR);
        linkSelection.exclude(Material.AIR);

        srcSelection.exclude(Material.NETHER_BRICK);
        linkSelection.exclude(Material.NETHER_BRICK);

        srcSelection.exclude(Material.COBBLE_WALL);
        linkSelection.exclude(Material.COBBLE_WALL);

        this.state.world.setSrcSelection(srcSelection);
        this.state.world.setLinkSelection(linkSelection);

        this.state.world.setSrcSpecSpawn(new Location(this.gameWorld, 400.5, 65, 0.5));
        this.state.world.setLinkSpecSpawn(new Location(this.gameWorld, -399.5, 65, 0.5));
        this.state.world.setMapBounds(new CoordBoundary(this.gameWorld, -421, 25, -21, 421, 82, 21));

        this.state.ui.setTitleTimes(0, 30, 0);

        final PhaseGroup mainSequence = new PhaseSequence();

        this.state.setStage(GameStage.PREGAME);

        for (GamePlayer gp : this.state.getPlayers()) {
            gp.setInventory(new PreGameInventory(this.state, gp, this.plugin));
        }

        GamePhase lobby = new GamePhase(this.state, GameConstants.LOBBY_LENGTH) {
            @Override
            public void tick() {
                if (this.getTick() % 20 == 0) {
                    Collection<? extends Player> players = Bukkit.getOnlinePlayers();

                    for (Player p : players) {
                        this.state.ui.displayJukeboxMsg(
                                ChatColor.YELLOW.toString() + ChatColor.BOLD + "You will be teleported in " + this.ticksLeft() / 20 + " seconds!",
                                p
                        );
                    }

                    switch (this.getTick()) {
                        case 0:
                        case 5 * GameConstants.TICKS_PER_SEC:
                        case 10 * GameConstants.TICKS_PER_SEC:
                        case 15 * GameConstants.TICKS_PER_SEC:
                        case 16 * GameConstants.TICKS_PER_SEC:
                        case 17 * GameConstants.TICKS_PER_SEC:
                        case 18 * GameConstants.TICKS_PER_SEC:
                        case 19 * GameConstants.TICKS_PER_SEC:
                            for (Player p : players) {
                                this.state.ui.playSound(Sound.CLICK, 1, 1, p);
                            }
                    }
                }
            }

            @Override
            public void onFinish() {
                this.state.initPlayers();

                for (GamePlayer gp : this.state.getPlayers()) {
                    gp.setInventory(new EmptyInventory(gp));
                }
            }
        };

        GamePhase preGame = new GamePhase(this.state, GameConstants.PREGAME_LENGTH) {
            @Override
            public void tick() {
                if (this.getTick() % 20 == 0) {
                    List<GamePlayer> players = this.state.getPlayers().getCurrentPlayersInGame();

                    this.state.ui.setTitleTimes(0, 30, 0);

                    for (GamePlayer gp : players) {
                        this.state.ui.displayTitle(
                                ChatColor.YELLOW + "Mirror Spleef",
                                ChatColor.RED + "Game starts in " + this.ticksLeft() / GameConstants.TICKS_PER_SEC + " seconds!",
                                gp.getBukkitPlayer()
                        );
                    }

                    switch (this.getTick()) {
                        case 5 * GameConstants.TICKS_PER_SEC:
                            for (GamePlayer gp : this.state.getPlayers()) {
                                this.state.showFlippedTeam(gp, gp.getTeam());
                                this.state.showFlippedTeam(gp, gp.getTeam().next());
                            }
                        case 0:
                        case 10 * GameConstants.TICKS_PER_SEC:
                        case 11 * GameConstants.TICKS_PER_SEC:
                        case 12 * GameConstants.TICKS_PER_SEC:
                        case 13 * GameConstants.TICKS_PER_SEC:
                        case 14 * GameConstants.TICKS_PER_SEC:
                            for (GamePlayer gp : players) {
                                this.state.ui.playSound(Sound.CLICK, 1, 1, gp.getBukkitPlayer());
                            }
                    }
                }
            }

            @Override
            public void onFinish() {
                List<GamePlayer> players = this.state.getPlayers().getCurrentPlayersInGame();

                this.state.ui.setTitleTimes(0, 20, 0);

                for (GamePlayer gp : players) {
                    this.state.ui.displayTitle(ChatColor.GOLD.toString() + ChatColor.BOLD + "Go!", null, gp.getBukkitPlayer());

                    GameClass cl = gp.getActiveClass();

                    if (cl == GameClass.RANDOM) {
                        cl = GameClass.getRandomClass();
                        gp.setActiveClass(cl);
                    }

                    if (this.state.hasDoubleJumps(gp)) {
                        gp.getBukkitPlayer().setAllowFlight(true);
                        gp.setCanDoubleJump(true);
                    }

                    this.state.resetClassInventory(gp, false);
                }

                this.state.setStage(GameStage.GRACE);

                this.state.startGame();
            }
        };

        final GamePhase endGame = new GamePhase(this.state, GameConstants.ENDGAME_LENGTH) {
            @Override
            public void tick() {
            }

            @Override
            public void onFinish() {
                this.state.endGame();
            }
        };

        GamePhase gracePeriod = new GamePhase(this.state, GameConstants.GRACE_PERIOD_LENGTH) {
            @Override
            public void onStart() {
                this.state.addEventPhase(new InstamineSpawnPhase(
                        this.state, GameConstants.INSTAMINE_SPAWN_TIME
                ));
            }

            @Override
            public void tick() {
                if (this.state.getStage() == GameStage.ENDGAME) {
                    this.skip();
                }
            }

            @Override
            public void onFinish() {
                this.state.setStage(GameStage.FIREBALL_4S);
            }
        };

        GamePhase fireball1 = new GamePhase(this.state, GameConstants.FIREBALL_4S_LENGTH) {
            @Override
            public void tick() {
                if (this.state.getStage() == GameStage.ENDGAME) {
                    this.skip();
                    return;
                }

                if (this.ticksLeft() % (8 * GameConstants.TICKS_PER_SEC) == 0) {
                    this.state.world.spawnSrcFireball(
                            this.state.world.getRandomSrcBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomSrcBlock().getLocation(), 0.1
                    );
                } else if (this.ticksLeft() % (8 * GameConstants.TICKS_PER_SEC) == 4 * GameConstants.TICKS_PER_SEC) {
                    this.state.world.spawnLinkFireball(
                            this.state.world.getRandomLinkBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomLinkBlock().getLocation(), 0.1
                    );
                }
            }

            @Override
            public void onFinish() {
                this.state.setStage(GameStage.FIREBALL_2S);
            }
        };

        GamePhase fireball2 = new GamePhase(this.state, GameConstants.FIREBALL_2S_LENGTH) {
            @Override
            public void tick() {
                if (this.state.getStage() == GameStage.ENDGAME) {
                    this.skip();
                    return;
                }

                if (this.ticksLeft() % (4 * GameConstants.TICKS_PER_SEC) == 0) {
                    this.state.world.spawnSrcFireball(
                            this.state.world.getRandomSrcBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomSrcBlock().getLocation(), 0.1
                    );
                } else if (this.ticksLeft() % (4 * GameConstants.TICKS_PER_SEC) == 2 * GameConstants.TICKS_PER_SEC) {
                    this.state.world.spawnLinkFireball(
                            this.state.world.getRandomLinkBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomLinkBlock().getLocation(), 0.1
                    );
                }
            }

            @Override
            public void onFinish() {
                this.state.setStage(GameStage.FIREBALL_1S);
            }
        };

        GamePhase fireball3 = new GamePhase(this.state, GameConstants.FIREBALL_1S_LENGTH) {
            @Override
            public void tick() {
                if (this.state.getStage() == GameStage.ENDGAME) {
                    this.skip();
                    return;
                }

                if (this.ticksLeft() % (2 * GameConstants.TICKS_PER_SEC) == 0) {
                    this.state.world.spawnSrcFireball(
                            this.state.world.getRandomSrcBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomSrcBlock().getLocation(), 0.1
                    );
                } else if (this.ticksLeft() % (2 * GameConstants.TICKS_PER_SEC) == GameConstants.TICKS_PER_SEC) {
                    this.state.world.spawnLinkFireball(
                            this.state.world.getRandomLinkBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomLinkBlock().getLocation(), 0.1
                    );
                }
            }

            @Override
            public void onFinish() {
                for (GamePlayer gp : this.state.getPlayers()) {
                    this.state.setLives(gp, 1);
                }
            }
        };

        GamePhase suddenDeath = new GamePhase(this.state, GameConstants.SUDDEN_DEATH_LENGTH) {
            @Override
            public void tick() {
                if (this.state.getStage() == GameStage.ENDGAME) {
                    this.skip();
                    return;
                }

                if (this.ticksLeft() % GameConstants.TICKS_PER_SEC == 0) {
                    this.state.world.spawnSrcFireball(
                            this.state.world.getRandomSrcBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomSrcBlock().getLocation(), 0.1
                    );
                } else if (this.ticksLeft() % GameConstants.TICKS_PER_SEC == GameConstants.TICKS_PER_SEC / 2) {
                    this.state.world.spawnLinkFireball(
                            this.state.world.getRandomLinkBlock().getLocation().add(0, 25, 0),
                            this.state.world.getRandomLinkBlock().getLocation(), 0.1
                    );
                }
            }
        };

        mainSequence.addPhase(lobby);
        mainSequence.addPhase(preGame);
        mainSequence.addPhase(gracePeriod);

        mainSequence.addPhase(fireball1);
        mainSequence.addPhase(fireball2);
        mainSequence.addPhase(fireball3);

        mainSequence.addPhase(suddenDeath);
        mainSequence.addPhase(endGame);

        this.state.addPhaseGroup(mainSequence, false);
    }

    public void end() {
        this.state.endGame();
    }

    public GamePlayer addPlayer(Player p) {
        return this.state.addPlayer(p);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        p.setHealth(20);
        p.setFoodLevel(20);
        p.setSaturation(100);

        GamePlayer gp = this.addPlayer(p);

        if (this.state.getStage() == GameStage.PREGAME) {
            gp.setInventory(new PreGameInventory(this.state, gp, this.plugin));
        } else {
            gp.setInventory(new LobbyInventory(this.state, gp, this.plugin));
        }

        for (PotionEffect pe : p.getActivePotionEffects()) {
            p.removePotionEffect(pe.getType());
        }

        p.teleport(e.getPlayer().getWorld().getSpawnLocation().add(0.5, 0, 0.5));
        p.setGameMode(GameMode.ADVENTURE);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        this.state.removePlayer(e.getPlayer());
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        GamePlayer gp = this.state.getPlayers().getPlayer(e.getPlayer());

        if (!gp.isInGame()) return;

        if (!this.state.world.withinPlayerBounds(e.getTo())) {
            if(gp.isAlive()) this.state.killPlayer(gp);
            else gp.getBukkitPlayer().teleport(this.state.world.getRespawnLoc(gp.getTeam()));
        }

        if (!gp.isAlive()) return;

        GameTeam trapTeam = gp.getTeam().next();
        Block b = this.state.world.getActivatedTrap(e.getPlayer(), trapTeam);

        if (b != null) {
            this.state.explodeTrap(b, trapTeam);
        }

        this.state.flippedPlayerMove(e.getPlayer(), e.getTo());
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        if (!this.state.getPlayers().getPlayer(e.getPlayer()).isInGame()) return;
        this.state.flippedPlayerMove(e.getPlayer(), e.getTo());
    }

    @EventHandler
    public void onPlayerSwing(PlayerAnimationEvent e) {
        if (!this.state.getPlayers().getPlayer(e.getPlayer()).isInGame()) return;
        this.state.flippedPlayerSwingArm(e.getPlayer());
    }

    @EventHandler
    public void onPlayerCrouch(PlayerToggleSneakEvent e) {
        if (!this.state.getPlayers().getPlayer(e.getPlayer()).isInGame()) return;
        this.state.flippedPlayerCrouch(e.getPlayer(), e.isSneaking());
    }

    @EventHandler
    public void onPlayerFly(PlayerToggleFlightEvent e) {
        Player p = e.getPlayer();

        if (!this.state.getPlayers().getPlayer(e.getPlayer()).isInGame()) return;
        if (!this.state.getPlayers().getPlayer(e.getPlayer()).isAlive()) return;

        p.setFlying(false);
        e.setCancelled(true);

        this.state.useDoubleJump(this.state.getPlayers().getPlayer(e.getPlayer()));
    }

    @EventHandler
    public void onPlayerInventory(final InventoryClickEvent e) {
        if (!this.state.getPlayers().getPlayer((Player) e.getWhoClicked()).isInGame()) return;

        this.state.runNextTick(new BukkitRunnable() {
            @Override
            public void run() {
                Player p = (Player) e.getWhoClicked();
                Game.this.state.flippedPlayerSetItemInHand(p, p.getItemInHand());
            }
        });
    }

    @EventHandler
    public void onPlayerSlotChange(final PlayerItemHeldEvent e) {
        if (!this.state.getPlayers().getPlayer(e.getPlayer()).isInGame()) return;

        this.state.runNextTick(new BukkitRunnable() {
            @Override
            public void run() {
                Player p = e.getPlayer();
                Game.this.state.flippedPlayerSetItemInHand(p, p.getItemInHand());
            }
        });
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (e.getCause() == EntityDamageEvent.DamageCause.SUICIDE) return;

        e.setDamage(0);

        if (!(e.getEntity() instanceof Player)) return;
        if (!this.state.getPlayers().getPlayer((Player) e.getEntity()).isInGame()) {
            e.setCancelled(true);
            return;
        }

        if (e.getCause() != EntityDamageEvent.DamageCause.FALL) return;

        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerHit(EntityDamageByEntityEvent e) {
        if(!(e.getEntity() instanceof Player)) return;
        if(!(e.getDamager() instanceof Player)) return;

        GamePlayer damagedPlayer = this.state.getPlayers().getPlayer((Player) e.getEntity());

        if(!damagedPlayer.isInGame()) return;
        if(!this.state.getPlayers().getPlayer((Player) e.getEntity()).isAlive()) return;

        GameTeam damager = this.state.getPlayers().getPlayer((Player) e.getDamager()).getTeam();
        GameTeam damaged = damagedPlayer.getTeam();

        if(damager == damaged) e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerHunger(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPhysics(EntityChangeBlockEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onProjectileHit(EntityExplodeEvent e) {
        if (e.getEntity() instanceof LargeFireball) {
            e.setCancelled(true);

            ((LargeFireball) e.getEntity()).setIsIncendiary(false);
            ((LargeFireball) e.getEntity()).setYield(0);

            if (!this.state.world.isOriginalFireball(e.getEntity())) return;

            Location loc = e.getLocation();
            GameTeam team = GameTeam.TEAM_SRC;

            if (this.state.world.isInLink(loc)) {
                team = GameTeam.TEAM_LINK;
            }

            this.state.explode(this.state.world.getFireballTarget((Fireball) e.getEntity()).getBlock(), team, 2, 0.4);
            this.state.world.removeFireball((Fireball) e.getEntity());
        }
    }
}
