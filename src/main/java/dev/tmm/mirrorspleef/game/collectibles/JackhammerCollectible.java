package dev.tmm.mirrorspleef.game.collectibles;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import dev.tmm.mirrorspleef.game.inventory.items.Jackhammer;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class JackhammerCollectible extends Collectible {
    public JackhammerCollectible(Block b, GameState state, GameTeam team, StructureRotation rotation, Plugin plugin) {
        super(
                b, GameClass.MINER.getItemStack(),
                state, "Jackhammer", team, rotation, plugin
        );
    }

    @Override
    public boolean collect(Player p) {
        GamePlayer gp = this.state.getPlayers().getPlayer(p);

        if (!gp.isAlive()) return false;

        if (!gp.getCurrentGameData().hasItemSpace()) {
            this.state.ui.setTitleTimes(0, 20, 0);
            this.state.ui.displayTitle(null, ChatColor.RED + "You don't have enough item space!", p);
            return false;
        }

        gp.getCurrentGameData().decreaseItemSpace();

        Jackhammer item = new Jackhammer(1, this.state, gp);
        Bukkit.getPluginManager().registerEvents(item, this.plugin);
        p.getInventory().addItem(item.getBukkitStack());

        return true;
    }
}
