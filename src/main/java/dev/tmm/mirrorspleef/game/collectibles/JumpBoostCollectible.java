package dev.tmm.mirrorspleef.game.collectibles;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.*;
import dev.tmm.mirrorspleef.game.inventory.items.JumpPotion;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class JumpBoostCollectible extends Collectible {
    private PotionEffect effect;

    public JumpBoostCollectible(Block loc, GameState state, GameTeam team, StructureRotation rotation, Plugin plugin) {
        super(
                loc, GameClass.FROG.getItemStack(),
                state, "Jump Boost", team, rotation, plugin
        );

        this.effect = new PotionEffect(
                PotionEffectType.JUMP,
                UpgradeConstants.computeClassQuantityUpgrade(GameClass.FROG, 0),
                UpgradeConstants.computeClassPotencyUpgrade(
                        GameClass.FROG, GameConstants.getCollectiblePotencyLevel(this.state.getStage())
                ), false, false
        );
    }

    @Override
    public boolean collect(Player p) {
        GamePlayer gp = this.state.getPlayers().getPlayer(p);

        if (!gp.isAlive()) return false;

        if (!gp.getCurrentGameData().hasItemSpace()) {
            this.state.ui.setTitleTimes(0, 20, 0);
            this.state.ui.displayTitle(null, ChatColor.RED + "You don't have enough item space!", p);
            return false;
        }

        JumpPotion item = new JumpPotion(
                UpgradeConstants.computeClassQuantityUpgrade(GameClass.FROG, 0),
                UpgradeConstants.computeClassPotencyUpgrade(
                        GameClass.FROG,
                        GameConstants.getCollectiblePotencyLevel(this.state.getStage())
                ), this.state, gp
        );

        Bukkit.getPluginManager().registerEvents(item, this.plugin);
        p.getInventory().addItem(item.getBukkitStack());

        gp.getCurrentGameData().decreaseItemSpace();

        return true;
    }
}
