package dev.tmm.mirrorspleef.game.collectibles;

import dev.tmm.mirrorspleef.base.entity.FloatingItem;
import dev.tmm.mirrorspleef.base.entity.MovableNameTag;
import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public abstract class Collectible extends FloatingItem {
    private final MovableNameTag name;
    private final MovableNameTag expiry;

    protected final GameState state;
    private int expiryTimer;

    private GameTeam team;

    protected final Plugin plugin;
    private ChatColor expiryColor;

    private Block srcBlock;

    public Collectible(Block b, ItemStack item, GameState state, String name, GameTeam team, StructureRotation rotation, Plugin plugin) {
        super(b.getLocation().add(calculateOffset(rotation)), item);

        this.state = state;
        this.plugin = plugin;

        this.expiryTimer = GameConstants.ITEM_LIFESPAN;
        this.expiryColor = ChatColor.GREEN;
        this.name = new MovableNameTag(this.location, ChatColor.YELLOW.toString() + ChatColor.BOLD + name);
        this.expiry = new MovableNameTag(this.location, this.expiryColor.toString() + this.expiryTimer / GameConstants.TICKS_PER_SEC + " seconds!");

        this.team = team;

        this.name.setOffset(0, rotation == StructureRotation.NONE ? 1.3 : -0.8, 0);
        this.expiry.setOffset(0, rotation == StructureRotation.NONE ? 1 : -0.5, 0);

        this.srcBlock = b;

        this.attachEntity(this.name, true);
        this.attachEntity(this.expiry, true);
    }

    public final void tick() {
        this.expiryTimer--;
        if (this.expiryTimer % (GameConstants.TICKS_PER_SEC / 2) == 0) {
            this.toggleColor();
            if (this.expiryTimer % GameConstants.TICKS_PER_SEC == 0) {
                this.expiry.setName(this.expiryColor.toString() + this.expiryTimer / GameConstants.TICKS_PER_SEC + " seconds!");
            }
        }
    }

    public final void toggleColor() {
        if (this.expiryColor == ChatColor.GREEN) this.expiryColor = ChatColor.GOLD;
        else this.expiryColor = ChatColor.GREEN;

        this.expiry.setName(this.expiryColor.toString() + (this.expiryTimer / GameConstants.TICKS_PER_SEC) + " seconds!");
    }

    public Block getSrcBlock() {
        return this.srcBlock;
    }

    public GameTeam getTeam() {
        return this.team;
    }

    public abstract boolean collect(Player p);

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (this.isTouching(e.getPlayer())) {
            if (this.collect(e.getPlayer())) this.state.removeItem(this);
        }
    }

    private static Vector calculateOffset(StructureRotation rotation) {
        Vector out = new Vector(0.5, 2, 0.5);

        if(rotation != StructureRotation.NONE) out.setY(-2);

        return out;
    }

    /*
    TODO Teleport specs after void
     */
}
