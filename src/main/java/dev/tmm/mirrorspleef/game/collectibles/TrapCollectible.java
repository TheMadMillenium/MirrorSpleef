package dev.tmm.mirrorspleef.game.collectibles;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.*;
import dev.tmm.mirrorspleef.game.inventory.items.Trap;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class TrapCollectible extends Collectible {
    public TrapCollectible(Block b, GameState state, GameTeam team, StructureRotation rotation, Plugin plugin) {
        super(
                b, GameClass.ENGINEER.getItemStack(),
                state, "Explosive Traps", team, rotation, plugin
        );
    }

    @Override
    public boolean collect(Player p) {
        GamePlayer gp = this.state.getPlayers().getPlayer(p);
        if (!gp.isAlive()) return false;

        if (!gp.getCurrentGameData().hasItemSpace()) {
            this.state.ui.setTitleTimes(0, 20, 0);
            this.state.ui.displayTitle(null, ChatColor.RED + "You don't have enough item space!", p);
            return false;
        }

        Trap item = new Trap(
                UpgradeConstants.computeClassQuantityUpgrade(GameClass.ENGINEER, 0),
                UpgradeConstants.computeClassPotencyUpgrade(
                        GameClass.ENGINEER,
                        GameConstants.getCollectiblePotencyLevel(this.state.getStage())
                ), this.state, gp
        );

        Bukkit.getPluginManager().registerEvents(item, this.plugin);
        p.getInventory().addItem(item.getBukkitStack());

        gp.getCurrentGameData().decreaseItemSpace();

        return true;
    }
}
