package dev.tmm.mirrorspleef.game.collectibles;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.classes.*;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffectType;

public class RefillCollectible extends Collectible {
    private final Player p;

    public RefillCollectible(Block b, Player deployer, GameState state, GameTeam team, StructureRotation rotation, Plugin plugin) {
        super(
                b, new ItemStack(Material.EXP_BOTTLE), state,
                deployer == null ? "Refill" : deployer.getName() + "'s Refill",
                team, rotation, plugin);

        this.p = deployer;

    }

    @Override
    public boolean collect(Player p) {
        GamePlayer gp = this.state.getPlayers().getPlayer(p);

        if (!gp.isAlive()) return false;
        if (p.equals(this.p)) return false;

        this.state.ui.setTitleTimes(0, 20, 0);

        if (gp.getActiveClass() == GameClass.PERKSMITH && this.p != null) {
            this.state.ui.displayTitle(null, ChatColor.RED + "You can only collect naturally spawning refills!", p);
            return false;
        }

        GameInventory inventory;

        switch (gp.getActiveClass()) {
            case SCOUT:
                inventory = new ScoutInventory(this.state, gp, false, this.plugin);
                if (gp.getBukkitPlayer().hasPotionEffect(PotionEffectType.SPEED)) {
                    this.state.ui.displayTitle(null, ChatColor.RED + "You still have an active speed boost!", p);
                    return false;
                }
                break;
            case FROG:
                inventory = new FrogInventory(this.state, gp, false, this.plugin);
                if (gp.getBukkitPlayer().hasPotionEffect(PotionEffectType.JUMP)) {
                    this.state.ui.displayTitle(null, ChatColor.RED + "You still have an active jump boost!", p);
                    return false;
                }
                break;
            case MINER:
                inventory = new MinerInventory(this.state, gp, false, this.plugin);
                break;
            case ENGINEER:
                inventory = new EngineerInventory(this.state, gp, false, this.plugin);
                break;
            case PERKSMITH:
                inventory = new PerksmithInventory(this.state, gp, false, this.plugin);
                break;
            case BRAWLER:
                inventory = new BrawlerInventory(this.state, gp, false, this.plugin);
                break;
            /*case THIEF:
                inventory = new ThiefInventory(this.state, gp, false, this.plugin);
                break;*/
            default:
                p.sendMessage("You should not see this message!");
                return false;
        }

        if (inventory.containsItems()) {
            this.state.ui.displayTitle(null, ChatColor.RED + "You still have remaining class items!", p);
            return false;
        }

        gp.overlayInventory(inventory);

        return true;
    }
}
