package dev.tmm.mirrorspleef.game;

import dev.tmm.mirrorspleef.base.entity.FlippedPlayerNPC;
import dev.tmm.mirrorspleef.base.entity.PacketScoreboardTeam;
import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.ui.UserInterface;
import dev.tmm.mirrorspleef.base.world.Explosion;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.collectibles.*;
import dev.tmm.mirrorspleef.game.data.*;
import dev.tmm.mirrorspleef.game.inventory.EmptyInventory;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.LobbyInventory;
import dev.tmm.mirrorspleef.game.inventory.classes.*;
import dev.tmm.mirrorspleef.game.phases.*;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import dev.tmm.mirrorspleef.game.phases.base.ParallelPhaseGroup;
import dev.tmm.mirrorspleef.game.phases.base.PhaseGroup;
import dev.tmm.mirrorspleef.util.RandomUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class GameState {
    private static final int SCOUT = 0;
    private static final int FROG = 1;
    private static final int MINER = 2;
    private static final int ENGINEER = 3;
    private static final int PERKSMITH = 4;
    private static final int BRAWLER = 5;
    private static final int REFILL = 6;
    private static final int THIEF = 7;

    public final UserInterface ui;
    public final GameWorld world;
    private final Plugin plugin;

    private GameStage stage;

    private Map<PhaseGroup, Boolean> phaseGroups;
    private PhaseGroup eventProcessor;

    private GamePlayerList players;
    private Map<Player, FlippedPlayerNPC> mappedReflections;

    public GameState(Plugin plugin, World world) {
        this.ui = new UserInterface();
        this.world = new GameWorld(world);

        this.plugin = plugin;

        this.phaseGroups = new ConcurrentHashMap<>();
        this.eventProcessor = new ParallelPhaseGroup();

        this.players = new GamePlayerList();
        this.mappedReflections = new HashMap<>();
    }

    public void tick() {
        for (PhaseGroup group : this.phaseGroups.keySet()) {
            group.tick();
        }
    }

    public boolean isFinished() {
        for (PhaseGroup sequence : this.phaseGroups.keySet()) {
            if (this.phaseGroups.get(sequence)) continue;
            if (!sequence.isFinished()) return false;
        }

        return true;
    }

    public void addPhaseGroup(PhaseGroup sequence, boolean canEndEarly) {
        this.phaseGroups.put(sequence, canEndEarly);
    }

    public void addEventPhase(GamePhase phase) {
        this.eventProcessor.addPhase(phase);
    }

    public GamePlayer addPlayer(Player p) {
        GamePlayer out = new GamePlayer(p, this.plugin);
        this.players.addPlayer(out);

        return out;
    }

    public void removePlayer(Player p) {
        this.players.removePlayer(p);
    }

    public void startGame() {
        this.addPhaseGroup(this.eventProcessor, true);

        this.eventProcessor.addPhase(new SpawnItemsPhase(this));
    }

    public void endGame() {
        for (GamePlayer gp : this.players) {
            Player p = gp.getBukkitPlayer();

            p.setAllowFlight(false);
            p.setGameMode(GameMode.ADVENTURE);
            p.teleport(p.getWorld().getSpawnLocation().add(0.5, 0, 0.5));

            gp.removeAllPotions();

            gp.setInventory(new LobbyInventory(this, gp, this.plugin));
            gp.clearVisibleTeams();

            gp.setInGame(false);
        }

        Iterator<Player> reflections = this.mappedReflections.keySet().iterator();

        while (reflections.hasNext()) {
            this.mappedReflections.get(reflections.next()).hideFromAll();
            reflections.remove();
        }

        this.world.clearTraps();

        this.eventProcessor.clear();
        this.phaseGroups.clear();

        this.setStage(GameStage.LOBBY);
    }

    public GameStage getStage() {
        return this.stage;
    }

    public void setStage(GameStage stage) {
        this.stage = stage;
    }

    public boolean inProgress() {
        return this.stage.inProgress();
    }

    public void initPlayers() {
        ArrayList<GamePlayer> playerList = new ArrayList<>(this.getPlayers().getCurrentPlayers());

        GameTeam team = GameTeam.TEAM_SRC;

        PacketScoreboardTeam npcs = new PacketScoreboardTeam("NPCs");
        npcs.setNameVisibility(NameTagVisibility.NEVER);
        npcs.addNames("Grumm", "NotGrumm");

        PacketScoreboardTeam src_allies = new PacketScoreboardTeam("src_allies");
        PacketScoreboardTeam src_enemies = new PacketScoreboardTeam("src_enemies");

        PacketScoreboardTeam link_allies = new PacketScoreboardTeam("link_allies");
        PacketScoreboardTeam link_enemies = new PacketScoreboardTeam("link_enemies");

        src_allies.setPrefix(ChatColor.GREEN.toString() + ChatColor.BOLD);
        link_allies.setPrefix(ChatColor.GREEN.toString() + ChatColor.BOLD);

        src_enemies.setPrefix(ChatColor.RED.toString() + ChatColor.BOLD);
        link_enemies.setPrefix(ChatColor.RED.toString() + ChatColor.BOLD);

        while (!playerList.isEmpty()) {
            GamePlayer gp = playerList.remove(RandomUtils.getInt(playerList.size()));

            gp.setInGame(true);
            gp.setTeam(team);
            gp.setAlive(true);

            switch (team) {
                case TEAM_SRC:
                    src_allies.addNames(gp.getBukkitPlayer().getName());
                    link_enemies.addNames(gp.getBukkitPlayer().getName());
                    break;
                case TEAM_LINK:
                    link_allies.addNames(gp.getBukkitPlayer().getName());
                    src_enemies.addNames(gp.getBukkitPlayer().getName());
                    break;
            }

            team = team.next();
            gp.computeData();

            this.spawnPlayer(gp, false);
        }

        for (GamePlayer p : this.players) {
            p.addVisibleTeam(npcs);

            switch (p.getTeam()) {
                case TEAM_SRC:
                    p.addVisibleTeam(src_allies);
                    p.addVisibleTeam(src_enemies);
                    break;
                case TEAM_LINK:
                    p.addVisibleTeam(link_allies);
                    p.addVisibleTeam(link_enemies);
                    break;
            }
        }
    }

    public GamePlayerList getPlayers() {
        return this.players;
    }

    public GamePlayer getPlayer(Player p) {
        return this.players.getPlayer(p);
    }

    public void showFlippedTeam(GamePlayer gp, GameTeam team) {
        for (Player p : this.mappedReflections.keySet()) {
            if (p == gp.getBukkitPlayer()) continue;

            GamePlayer npcSrc = this.getPlayer(p);
            if (npcSrc.getTeam() != team) continue;

            ChatColor prefix = gp.getTeam() == npcSrc.getTeam() ? ChatColor.GREEN : ChatColor.RED;

            this.mappedReflections.get(p).setName(prefix.toString() + ChatColor.BOLD + p.getName());
            this.mappedReflections.get(p).show(gp.getBukkitPlayer());
        }
    }

    public void hideFlippedTeam(GamePlayer gp, GameTeam team) {
        for (Player p : this.mappedReflections.keySet()) {
            GamePlayer npcSrc = this.getPlayer(p);

            if (npcSrc.getTeam() != team) continue;

            this.mappedReflections.get(p).hide(gp.getBukkitPlayer());
        }
    }

    public void spawnFlippedPlayer(GamePlayer gp) {
        Location spawnLoc = null;

        GameTeam team = gp.getTeam();
        Player p = gp.getBukkitPlayer();

        switch (team) {
            case TEAM_SRC:
                spawnLoc = this.world.srcToLink(p.getLocation());
                break;
            case TEAM_LINK:
                spawnLoc = this.world.linkToSrc(p.getLocation());
                break;
        }

        final FlippedPlayerNPC reflection = new FlippedPlayerNPC(spawnLoc, p.getUniqueId());
        Bukkit.getPluginManager().registerEvents(reflection, this.plugin);

        this.mappedReflections.put(p, reflection);

        new BukkitRunnable() {
            @Override
            public void run() {
                reflection.removeFromTab();
            }
        }.runTaskLater(this.plugin, 5 * GameConstants.TICKS_PER_SEC);
    }

    public void showFlippedPlayer(GamePlayer gp) {
        for (GamePlayer player : this.players.getCurrentPlayers()) {
            this.mappedReflections.get(gp.getBukkitPlayer()).show(player.getBukkitPlayer());
        }
    }

    public void hideFlippedPlayer(GamePlayer gp) {
        this.mappedReflections.get(gp.getBukkitPlayer()).hideFromAll();
    }

    public boolean isParticipant(GamePlayer gp) {
        return this.inProgress() && gp.isInGame();
    }

    public boolean isAlive(GamePlayer gp) {
        return gp.isAlive();
    }

    public boolean hasDoubleJumps(GamePlayer gp) {
        return gp.getCurrentGameData().hasDoubleJumps();
    }

    public boolean hasMoreLives(GamePlayer gp) {
        return gp.getCurrentGameData().hasLives();
    }

    public boolean hasItemSpace(GamePlayer gp) {
        return gp.getCurrentGameData().hasItemSpace();
    }

    public void decrementDoubleJumps(GamePlayer gp) {
        gp.getCurrentGameData().decreaseDoubleJumps();
    }

    public void decrementLives(GamePlayer gp) {
        gp.getCurrentGameData().decreaseLives();
    }

    public void incrementItemSlots(GamePlayer gp) {
        gp.getCurrentGameData().increaseItemSpace();
    }

    public void decrementItemSlots(GamePlayer gp) {
        gp.getCurrentGameData().decreaseItemSpace();
    }

    public void setLives(GamePlayer gp, int lives) {
        gp.getCurrentGameData().setLives(lives);
    }

    public void killPlayer(GamePlayer gp) {
        Player p = gp.getBukkitPlayer();

        gp.setAlive(false);
        gp.setInventory(new EmptyInventory(gp));

        p.setGameMode(GameMode.SPECTATOR);

        p.setAllowFlight(true);
        p.setFlying(true);

        p.teleport(this.world.getRespawnLoc(gp.getTeam()));

        this.decrementLives(gp);

        String msg;

        this.ui.setTitleTimes(10, 2 * GameConstants.TICKS_PER_SEC, 10);

        if (this.hasMoreLives(gp)) {
            int lives = gp.getCurrentGameData().getLives();
            msg = ChatColor.YELLOW + "You have " + lives + " li" + (lives > 1 ? "ves" : "fe") + " left!";
        } else {
            msg = ChatColor.YELLOW + "You are now a spectator!";
        }

        this.ui.setTitleTimes(5, 30, 5);
        this.ui.displayTitle(ChatColor.RED + "You died!", msg, p);

        ChatColor playerTeamColor = null;

        switch (gp.getTeam()) {
            case TEAM_SRC:
                playerTeamColor = GameConstants.SRC_TEAM_CHAT_COLOR;
                break;
            case TEAM_LINK:
                playerTeamColor = GameConstants.LINK_TEAM_CHAT_COLOR;
                break;
        }

        boolean srcHasLives = false;
        boolean linkHasLives = false;

        for (GamePlayer player : this.getPlayers()) {
            if (!player.isInGame()) continue;

            if (player.getTeam() == GameTeam.TEAM_SRC) srcHasLives |= player.getCurrentGameData().hasLives();
            else linkHasLives |= player.getCurrentGameData().hasLives();

            player.getBukkitPlayer().sendMessage(playerTeamColor + p.getName() + ChatColor.YELLOW + " fell in the void!");
        }

        if (!srcHasLives || !linkHasLives) {
            GameTeam winner = srcHasLives ? GameTeam.TEAM_SRC : GameTeam.TEAM_LINK;
            String winnerStr = srcHasLives ? GameConstants.SRC_TEAM_COLOR_STRING : GameConstants.LINK_TEAM_COLOR_STRING;

            this.ui.setTitleTimes(10, GameConstants.ENDGAME_LENGTH / 4, 10);

            for (GamePlayer player : this.getPlayers()) {
                if (!player.isInGame()) continue;
                if (player.getTeam() == winner) {
                    this.ui.displayTitle(
                            ChatColor.GREEN + "Victory!",
                            ChatColor.GOLD + winnerStr + " team won the game!",
                            player.getBukkitPlayer()
                    );
                } else {
                    this.ui.displayTitle(
                            ChatColor.RED + "Game over!",
                            ChatColor.GOLD + "Better luck next time!",
                            player.getBukkitPlayer()
                    );
                }

                this.world.clearItems();
            }

            this.setStage(GameStage.ENDGAME);
        }

        this.hideFlippedPlayer(gp);

        this.eventProcessor.addPhase(new RespawnPhase(this, gp));
    }

    //TODO spectator is visible when dying as brawler

    public void spawnPlayer(GamePlayer gp, boolean respawning) {
        Player p = gp.getBukkitPlayer();

        p.setGameMode(GameMode.ADVENTURE);
        p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1, false, false));
        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2 * GameConstants.TICKS_PER_SEC, 255, false, false));
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2 * GameConstants.TICKS_PER_SEC, 128, false, false));
        p.setFlying(false);

        if (!this.hasDoubleJumps(gp)) p.setAllowFlight(false);

        Block block;

        switch (gp.getTeam()) {
            case TEAM_SRC:
                block = this.world.getRandomSrcBlock();
                if (block == null) return;

                break;
            case TEAM_LINK:
                block = this.world.getRandomLinkBlock();
                if (block == null) return;

                break;
            default:
                return;
        }

        Location loc = block.getLocation();

        if (respawning) {
            this.showFlippedPlayer(gp);
            this.resetClassInventory(gp, true);
            this.addEventPhase(new SpawnProtectionPhase(this, loc));
        } else {
            this.spawnFlippedPlayer(gp);
        }

        p.teleport(loc.add(0.5, 1.1, 0.5));

        gp.setAlive(true);
    }

    public void spawnRandomItem(GameTeam team) {
        Block randomBlock = team == GameTeam.TEAM_SRC ? this.world.getRandomSrcBlock() : this.world.getRandomLinkBlock();
        Location blockLoc = randomBlock.getLocation().add(0.5, 0.5, 0.5);
        Location linkBlockLoc = team == GameTeam.TEAM_SRC ? this.world.srcToLink(blockLoc) : this.world.linkToSrc(blockLoc);
        Block linkBlock = linkBlockLoc.getWorld().getBlockAt(linkBlockLoc);

        StructureRotation rotation = this.world.getLinkRotation();

        Collectible random;
        Collectible flippedRandom;

        switch (RandomUtils.getInt(0, 8)) {
            case SCOUT:
                random = new SpeedBoostCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new SpeedBoostCollectible(linkBlock, this, team, rotation, this.plugin);
                break;
            case FROG:
                random = new JumpBoostCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new JumpBoostCollectible(linkBlock, this, team, rotation, this.plugin);
                break;
            case MINER:
                random = new JackhammerCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new JackhammerCollectible(linkBlock, this, team, rotation, this.plugin);
                break;
            case ENGINEER:
                random = new TrapCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new TrapCollectible(linkBlock, this, team, rotation, this.plugin);
                break;
            case PERKSMITH:
                random = new RefillSpawnerCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new RefillSpawnerCollectible(linkBlock, this, team, rotation, this.plugin);
                break;
            case BRAWLER:
                random = new EnderSapCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new EnderSapCollectible(linkBlock, this, team, rotation, this.plugin);
                break;
            case REFILL:
                random = new RefillCollectible(randomBlock, null, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new RefillCollectible(linkBlock, null, this, team, rotation, this.plugin);
                break;
            /*case THIEF:
                random = new SwagSwiperCollectible(randomBlock, this, team, StructureRotation.NONE, this.plugin);
                flippedRandom = new SwagSwiperCollectible(linkBlock, this, team, rotation, this.plugin);
                break;*/
            default:
                return;
        }

        Bukkit.getPluginManager().registerEvents(random, this.plugin);
        Bukkit.getPluginManager().registerEvents(flippedRandom, this.plugin);

        for (GamePlayer gp : this.players) {
            if (gp.getTeam() == team) {
                random.show(gp.getBukkitPlayer());
            } else {
                flippedRandom.show(gp.getBukkitPlayer());
            }
        }

        this.world.addCollectible(random, flippedRandom);

        this.eventProcessor.addPhase(new DespawnItemPhase(random, this));
        this.eventProcessor.addPhase(new DespawnItemPhase(flippedRandom, this));
    }

    public void spawnRefill(Block b, GamePlayer gp) {
        Location blockLoc = b.getLocation().add(0.5, 0.5, 0.5);
        Location linkBlockLoc = gp.getTeam() == GameTeam.TEAM_SRC ? this.world.srcToLink(blockLoc) : this.world.linkToSrc(blockLoc);
        Block linkBlock = linkBlockLoc.getWorld().getBlockAt(linkBlockLoc);

        StructureRotation rotation = this.world.getLinkRotation();

        Collectible item = new RefillCollectible(b, gp.getBukkitPlayer(), this, gp.getTeam(), StructureRotation.NONE, this.plugin);
        Collectible flippedItem = new RefillCollectible(linkBlock, gp.getBukkitPlayer(), this, gp.getTeam(), rotation, this.plugin);

        Bukkit.getPluginManager().registerEvents(item, this.plugin);
        Bukkit.getPluginManager().registerEvents(flippedItem, this.plugin);

        for (GamePlayer gpIt : this.players) {
            if (gpIt.getTeam() == gp.getTeam()) {
                item.show(gpIt.getBukkitPlayer());
            } else {
                flippedItem.show(gpIt.getBukkitPlayer());
            }
        }

        this.world.addCollectible(item, flippedItem);

        this.eventProcessor.addPhase(new DespawnItemPhase(item, this));
        this.eventProcessor.addPhase(new DespawnItemPhase(flippedItem, this));
    }

    public boolean itemExists(Collectible c) {
        return this.world.containsItem(c);
    }

    //TODO give thief a small chance of receiving a stolen perk

    public void removeItem(Collectible c) {
        this.world.removeItem(c);
    }

    public void explodeTrap(Block b, GameTeam trapTeam) {
        this.explode(b, trapTeam, this.world.getTrapRange(b, trapTeam), this.world.getTrapRange(b, trapTeam) * 0.1);
        this.world.removeTrap(b);
    }

    public void explode(Block b, GameTeam team, double radius, double blastPower) {
        Explosion srcExplosion = new Explosion(b.getLocation().add(0.5, 0.5, 0.5), radius, 0.7);
        srcExplosion.addAffectedBlock(Material.STAINED_GLASS, 3);
        srcExplosion.addAffectedBlock(Material.STAINED_CLAY, GameConstants.SRC_TEAM_COLOR);
        srcExplosion.addAffectedBlock(Material.STAINED_CLAY, GameConstants.LINK_TEAM_COLOR);

        Location linkExpLoc = team == GameTeam.TEAM_SRC ?
                this.world.srcToLink(b.getLocation().add(0.5, 0.5, 0.5)) :
                this.world.linkToSrc(b.getLocation().add(0.5, 0.5, 0.5));

        Explosion linkExplosion = new Explosion(linkExpLoc, radius, 0.7);

        List<Block> destroyedBlocks = srcExplosion.explode(b.getWorld());
        linkExplosion.explode(b.getWorld());

        for (Block destroyed : destroyedBlocks) {
            this.world.onBlockBreak(destroyed);
        }

        for (GamePlayer gPlayer : this.getPlayers()) {
            if (!gPlayer.isInGame()) continue;

            Vector launch;

            if (gPlayer.getTeam() == team) {
                launch = srcExplosion.getBlastVectorDefinite(gPlayer.getBukkitPlayer().getLocation());
            } else {
                launch = linkExplosion.getBlastVectorDefinite(gPlayer.getBukkitPlayer().getLocation());
            }

            if (launch == null) continue;

            gPlayer.getBukkitPlayer().setVelocity(
                    launch.add(
                            new Vector(0, blastPower, 0)
                    )
            );
        }
    }

    public boolean containsInstaminePerk(Block b) {
        return this.world.containsInstamine(b);
    }

    public void useDoubleJump(GamePlayer gp) {
        if (!gp.canDoubleJump()) return;
        if (!gp.getCurrentGameData().hasDoubleJumps()) return;

        Player p = gp.getBukkitPlayer();

        if (this.hasDoubleJumps(gp)) {
            p.setVelocity(p.getVelocity().setY(GameConstants.DOUBLE_JUMP_VELOCITY));
            this.decrementDoubleJumps(gp);
        }

        if (!this.hasDoubleJumps(gp)) p.setAllowFlight(false);

        this.setCanDoubleJump(gp, false);
        this.eventProcessor.addPhase(new DoubleJumpCooldownPhase(gp, this));
    }

    public void setCanDoubleJump(GamePlayer gp, boolean canDoubleJump) {
        gp.setCanDoubleJump(canDoubleJump);
    }

    public void resetClassInventory(GamePlayer gp, boolean respawning) {
        gp.setInventory(new CommonInventory(this, gp, this.plugin));

        GameInventory inventory;

        switch (gp.getActiveClass()) {
            case SCOUT:
                inventory = new ScoutInventory(this, gp, respawning, this.plugin);
                break;
            case FROG:
                inventory = new FrogInventory(this, gp, respawning, this.plugin);
                break;
            case MINER:
                inventory = new MinerInventory(this, gp, respawning, this.plugin);
                break;
            case ENGINEER:
                inventory = new EngineerInventory(this, gp, respawning, this.plugin);
                break;
            case PERKSMITH:
                inventory = new PerksmithInventory(this, gp, respawning, this.plugin);
                break;
            case BRAWLER:
                inventory = new BrawlerInventory(this, gp, respawning, this.plugin);
                break;
            /*case THIEF:
                inventory = new ThiefInventory(this, gp, respawning, this.plugin);
                break;*/
            default:
                return;
        }

        gp.overlayInventory(inventory);
    }

    public void runNextTick(BukkitRunnable runnable) {
        runnable.runTaskLater(this.plugin, 1);
    }

    public void flippedPlayerMove(Player p, Location playerLoc) {
        if (!this.mappedReflections.containsKey(p)) return;

        Location npcLoc = null;

        GamePlayer gp = this.players.getPlayer(p);
        GameTeam team = gp.getTeam();

        if (gp.getActiveClass() == GameClass.BRAWLER && gp.isPerkActive()) {
            team = team.next();
        }

        switch (team) {
            case TEAM_SRC:
                npcLoc = this.world.srcToLink(playerLoc);

                break;
            case TEAM_LINK:
                npcLoc = this.world.linkToSrc(playerLoc);
                break;
        }

        this.mappedReflections.get(p).move(npcLoc);
    }

    public void flippedPlayerCrouch(Player p, boolean crouched) {
        if (!this.mappedReflections.containsKey(p)) return;
        this.mappedReflections.get(p).setCrouched(crouched);
    }

    public void flippedPlayerSwingArm(Player p) {
        if (!this.mappedReflections.containsKey(p)) return;
        this.mappedReflections.get(p).swingArm();
    }

    public void flippedPlayerSetItemInHand(Player p, ItemStack item) {
        if (!this.mappedReflections.containsKey(p)) return;
        this.mappedReflections.get(p).setBlockInHand(item);
    }

    public void updateInstamine(Block block) {
        if (this.world.containsInstamine(block)) {
            this.ui.setTitleTimes(5, 30, 5);

            for (GamePlayer gp : this.players) {
                this.ui.displayTitle(
                        ChatColor.GOLD + "Instamine Enabled!",
                        ChatColor.YELLOW + "Blocks now break instantly!",
                        gp.getBukkitPlayer()
                );

                if (gp.getActiveClass() == GameClass.MINER) {
                    this.addEventPhase(
                            new InstaminePhase(
                                    this, gp.getCurrentGameData().getClassQuantity(GameClass.MINER), gp
                            )
                    );
                } else {
                    this.addEventPhase(new InstaminePhase(this, GameConstants.INSTAMINE_TIME, gp));
                }
            }

            this.world.clearInstamines();
        }
    }
}
