package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.SpeedPotion;
import org.bukkit.plugin.Plugin;

public class ScoutInventory extends GameInventory {
    public ScoutInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(
                new SpeedPotion(
                        respawning ?
                                UpgradeConstants.computeClassQuantityUpgrade(GameClass.SCOUT, 0) :
                                gp.getCurrentGameData().getClassQuantity(GameClass.SCOUT),
                        respawning ?
                                UpgradeConstants.computeClassPotencyUpgrade(GameClass.SCOUT, 0) :
                                gp.getCurrentGameData().getClassPotency(GameClass.SCOUT),
                        state, gp
                )
        );
    }
}
