package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.EnderSap;
import org.bukkit.plugin.Plugin;

public class BrawlerInventory extends GameInventory {
    public BrawlerInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(
                new EnderSap(
                        respawning ?
                                UpgradeConstants.computeClassQuantityUpgrade(GameClass.BRAWLER, 0) :
                                gp.getCurrentGameData().getClassQuantity(GameClass.BRAWLER),
                        respawning ?
                                UpgradeConstants.computeClassPotencyUpgrade(GameClass.BRAWLER, 0) :
                                gp.getCurrentGameData().getClassPotency(GameClass.BRAWLER),
                        state, gp, this.plugin
                )
        );
    }
}
