package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class GameItem implements Listener {
    protected final NBTItemStack stack;
    protected final GameState state;
    protected final GamePlayer owner;
    protected GameInventory parentInventory;

    protected GameItem(NBTItemStack stack, GameState state, GamePlayer owner, long itemID) {
        this.stack = stack.set(GameConstants.ITEM_ID_STRING, itemID);
        this.state = state;
        this.owner = owner;
    }

    public ItemStack getBukkitStack() {
        return this.stack.getBukkitStack();
    }

    public boolean equalTo(NBTItemStack other) {
        if (other == null) return this.stack == null;

        if (this.stack.getBukkitStack().getType() != other.getBukkitStack().getType()) return false;
        return this.stack.isEqual(other, GameConstants.ITEM_ID_STRING);
    }

    public boolean equalTo(ItemStack other) {
        if (other == null) return this.stack == null;

        if (this.stack.getBukkitStack().getType() != other.getType()) return false;
        return this.stack.isEqual(new NBTItemStack(other), GameConstants.ITEM_ID_STRING);
    }

    public void setParent(GameInventory inv) {
        this.parentInventory = inv;
    }

    public void removeFromParent() {
        if(this.parentInventory == null) return;
        this.parentInventory.remove(this);
    }
}
