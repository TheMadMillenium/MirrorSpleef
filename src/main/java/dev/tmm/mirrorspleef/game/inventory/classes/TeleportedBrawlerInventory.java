package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.BrawlerSword;
import org.bukkit.plugin.Plugin;

public class TeleportedBrawlerInventory extends GameInventory {
    public TeleportedBrawlerInventory(GameState state, GamePlayer gp, int knockback, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(
                new BrawlerSword(this.state, gp, knockback)
        );
    }
}
