package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.phases.BrawlerPhase;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class EnderSap extends GameItem {
    private final int duration;
    private final int knockback;
    private Plugin plugin;

    public EnderSap(int duration, int knockback, GameState state, GamePlayer owner, Plugin plugin) {
        super(
                new NBTItemStack(Material.POTION, 1, (short) 8204)
                        .setName(ChatColor.GREEN + "Ender Sap")
                        .addItemFlags(ItemFlag.HIDE_POTION_EFFECTS),
                state, owner, knockback | ((long) duration << 8)
        );

        this.duration = duration;
        this.knockback = knockback;
        this.plugin = plugin;
    }

    @EventHandler
    public void onDrink(final PlayerItemConsumeEvent e) {
        if (!this.equalTo(e.getItem())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        this.state.runNextTick(new BukkitRunnable() {
            @Override
            public void run() {
                e.getPlayer().getInventory().remove(Material.GLASS_BOTTLE);
            }
        });
        this.owner.getCurrentGameData().increaseItemSpace();

        this.state.addEventPhase(
                new BrawlerPhase(
                        this.state,
                        this.duration,
                        this.owner,
                        this.knockback,
                        this.plugin
                )
        );

        this.removeFromParent();
    }
}
