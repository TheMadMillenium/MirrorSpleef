package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.Blocks;
import dev.tmm.mirrorspleef.game.inventory.items.Pickaxe;
import org.bukkit.plugin.Plugin;

public class CommonInventory extends GameInventory {
    public CommonInventory(GameState state, GamePlayer gp, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(new Pickaxe(state, gp));
        this.addItem(new Blocks(gp.getCurrentGameData().getBlocks(), state, gp));
    }
}
