package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class SpeedPotion extends GameItem {
    public SpeedPotion(int duration, int amplifier, GameState state, GamePlayer owner) {
        super(
                new NBTItemStack(Material.POTION, 1, (short) 8194)
                        .setName(ChatColor.GREEN + "Speed Boost")
                        .addItemFlags(ItemFlag.HIDE_POTION_EFFECTS)
                        .setPotionEffect(PotionEffectType.SPEED, duration, amplifier),
                state, owner, amplifier | ((long) duration << 8)
        );
    }

    @EventHandler
    public void onDrink(final PlayerItemConsumeEvent e) {
        if (!this.equalTo(e.getItem())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        this.state.runNextTick(new BukkitRunnable() {
            @Override
            public void run() {
                e.getPlayer().getInventory().remove(Material.GLASS_BOTTLE);
            }
        });

        this.owner.getCurrentGameData().increaseItemSpace();
        this.removeFromParent();
    }
}
