package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

public class Blocks extends GameItem {
    public Blocks(int quantity, GameState state, GamePlayer owner) {
        super(
                new NBTItemStack(
                        Material.STAINED_GLASS,
                        quantity,
                        owner.getTeam() == GameTeam.TEAM_SRC ? GameConstants.SRC_TEAM_COLOR : GameConstants.LINK_TEAM_COLOR
                ).setCanPlaceOn(Material.STAINED_GLASS),
                state, owner, quantity
        );
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!this.equalTo(e.getItemInHand())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        if (!this.state.world.isInSelectionBounds(e.getBlock())) {
            e.setCancelled(true);
            return;
        }

        this.state.world.onBlockPlace(e.getBlock());

        this.owner.getCurrentGameData().decreaseBlocks();
        this.stack.setAmount(this.getBukkitStack().getAmount() - 1);

        if (!this.owner.getCurrentGameData().hasBlocks()) {
            this.removeFromParent();
        }
    }
}
