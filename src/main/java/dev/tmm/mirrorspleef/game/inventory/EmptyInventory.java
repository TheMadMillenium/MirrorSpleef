package dev.tmm.mirrorspleef.game.inventory;

import dev.tmm.mirrorspleef.game.data.GamePlayer;

public class EmptyInventory extends GameInventory {
    public EmptyInventory(GamePlayer gp) {
        super(null, gp, null);
    }
}
