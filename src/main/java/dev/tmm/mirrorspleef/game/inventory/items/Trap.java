package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import dev.tmm.mirrorspleef.game.phases.TrapPlacePhase;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

public class Trap extends GameItem {
    private final int range;

    public Trap(int quantity, int range, GameState state, GamePlayer owner) {
        super(
                new NBTItemStack(Material.TNT, quantity)
                        .setName(ChatColor.GREEN + "Explosive Traps")
                        .setCanPlaceOn(Material.STAINED_GLASS),
                state, owner, range | ((long) quantity << 8)
        );

        this.range = range;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!this.equalTo(e.getItemInHand())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        GamePlayer gp = this.state.getPlayers().getPlayer(e.getPlayer());

        Block against = e.getBlockAgainst();

        if (this.state.world.isInSelectionBounds(against)) {
            this.state.world.addTrap(against, this.range, gp.getTeam());

            against.setType(Material.STAINED_CLAY);
            against.setData((byte) 14);

            if(gp.getTeam() == GameTeam.TEAM_SRC) {
                this.state.addEventPhase(
                        new TrapPlacePhase(this.state.world.srcToLink(against.getLocation().add(0.5, 0.5, 0.5)).getBlock(), this.state)
                );
            } else {
                this.state.addEventPhase(
                        new TrapPlacePhase(this.state.world.linkToSrc(against.getLocation().add(0.5, 0.5, 0.5)).getBlock(), this.state)
                );
            }

            e.getBlockPlaced().setType(Material.AIR);

            this.stack.setAmount(this.getBukkitStack().getAmount() - 1);

            if(e.getItemInHand().getAmount() == 0) {
                gp.getCurrentGameData().increaseItemSpace();
                this.removeFromParent();
            }
        }
    }
}
