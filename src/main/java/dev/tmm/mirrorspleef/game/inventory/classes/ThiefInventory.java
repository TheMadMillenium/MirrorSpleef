package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.SwagSwiper;
import org.bukkit.plugin.Plugin;

public class ThiefInventory extends GameInventory {
    public ThiefInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(
                new SwagSwiper(
                        respawning ?
                                UpgradeConstants.computeClassQuantityUpgrade(GameClass.THIEF, 0) :
                                gp.getCurrentGameData().getClassQuantity(GameClass.THIEF),
                        respawning ?
                                UpgradeConstants.computeClassPotencyUpgrade(GameClass.THIEF, 0) :
                                gp.getCurrentGameData().getClassPotency(GameClass.THIEF),
                        state, gp
                )
        );
    }
}
