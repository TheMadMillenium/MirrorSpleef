package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.inventory.ItemFlag;

public class Pickaxe extends GameItem {
    public Pickaxe(GameState state, GamePlayer owner) {
        super(new NBTItemStack(Material.DIAMOND_PICKAXE)
                        .setCanDestroy(Material.STAINED_GLASS)
                        .addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE)
                        .setUnbreakable(true),
                state, owner, 0
        );
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent e) {
        if (this.owner.isPerkActive()) {
            e.setInstaBreak(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!this.equalTo(e.getPlayer().getItemInHand())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        if (!this.state.world.isInSelection(e.getBlock())) {
            e.setCancelled(true);
            return;
        }

        this.state.updateInstamine(e.getBlock());
        this.state.world.onBlockBreak(e.getBlock());
    }
}
