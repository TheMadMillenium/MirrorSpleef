package dev.tmm.mirrorspleef.game.inventory;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.items.Shop;
import org.bukkit.plugin.Plugin;

public class LobbyInventory extends ImmutableInventory {
    public LobbyInventory(GameState state, GamePlayer gp, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(new Shop(state, gp, plugin));
    }
}
