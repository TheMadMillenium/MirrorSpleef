package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.ui.ClassSelectGUI;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

public class SelectClass extends GameItem {
    private Plugin plugin;

    public SelectClass(GameState state, GamePlayer owner, Plugin plugin) {
        super(new NBTItemStack(Material.BOW).setName(ChatColor.GREEN + "Select Class"), state, owner, 0);

        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK && e.getAction() != Action.RIGHT_CLICK_AIR) return;
        if (!this.equalTo(e.getItem())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        new ClassSelectGUI(
                this.owner.getPlayerData(),
                this.plugin
        ).open(e.getPlayer());
    }
}
