package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.RefillSpawner;
import org.bukkit.plugin.Plugin;

public class PerksmithInventory extends GameInventory {
    public PerksmithInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(new RefillSpawner(
                respawning ?
                        UpgradeConstants.computeClassQuantityUpgrade(GameClass.PERKSMITH, 0) :
                        gp.getCurrentGameData().getClassQuantity(GameClass.PERKSMITH),
                respawning ?
                        UpgradeConstants.computeClassPotencyUpgrade(GameClass.PERKSMITH, 0) :
                        gp.getCurrentGameData().getClassPotency(GameClass.PERKSMITH),
                state, gp
        ));
    }
}
