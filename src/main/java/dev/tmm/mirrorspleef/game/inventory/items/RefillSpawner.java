package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.phases.PerkDeployCooldownPhase;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

public class RefillSpawner extends GameItem {
    private int cooldown;
    private MutableBoolean canDeploy;

    public RefillSpawner(int quantity, int cooldown, GameState state, GamePlayer owner) {
        super(
                new NBTItemStack(Material.ANVIL, quantity)
                        .setCanPlaceOn(Material.STAINED_GLASS)
                        .setName(ChatColor.GREEN + "Deploy Perk"),
                state, owner, quantity | ((long) cooldown << 8)
        );

        this.cooldown = cooldown;
        this.canDeploy = new MutableBoolean(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!this.equalTo(e.getItemInHand())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        GamePlayer gp = this.state.getPlayers().getPlayer(e.getPlayer());

        Block against = e.getBlockAgainst();

        if (this.state.world.isInSelectionBounds(against)) {
            this.state.spawnRefill(
                    e.getBlockAgainst(),
                    gp
            );

            this.state.addEventPhase(new PerkDeployCooldownPhase(this.state, this.cooldown, this.canDeploy));

            e.getBlockPlaced().setType(Material.AIR);
        }

        this.stack.setAmount(this.getBukkitStack().getAmount() - 1);

        if(e.getItemInHand().getAmount() <= 1) {
            this.removeFromParent();
        }
    }
}
