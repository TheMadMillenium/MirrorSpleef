package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SwagSwiper extends GameItem {
    private int greed;

    public SwagSwiper(int quantity, int greed, GameState state, GamePlayer owner) {
        super(
                new NBTItemStack(Material.SKULL_ITEM, quantity, (short) 1)
                        .setName(ChatColor.GREEN + "Swag Swiper"),
                state, owner, greed | ((long) quantity << 8)
        );

        this.greed = greed;
    }

    @EventHandler
    public void onUse(PlayerInteractEvent e) {
        if (!this.equalTo(e.getItem())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        if (e.getAction() != Action.RIGHT_CLICK_BLOCK && e.getAction() != Action.RIGHT_CLICK_AIR) return;

        Location eyeLoc = owner.getBukkitPlayer().getEyeLocation();
    }
}
