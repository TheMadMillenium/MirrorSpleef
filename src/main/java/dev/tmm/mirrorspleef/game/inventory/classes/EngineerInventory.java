package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.Trap;
import org.bukkit.plugin.Plugin;

public class EngineerInventory extends GameInventory {
    public EngineerInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(
                new Trap(
                        respawning ?
                                UpgradeConstants.computeClassQuantityUpgrade(GameClass.ENGINEER, 0) :
                                gp.getCurrentGameData().getClassQuantity(GameClass.ENGINEER),
                        respawning ?
                                UpgradeConstants.computeClassPotencyUpgrade(GameClass.ENGINEER, 0) :
                                gp.getCurrentGameData().getClassPotency(GameClass.ENGINEER),
                        state, gp
                )
        );
    }
}
