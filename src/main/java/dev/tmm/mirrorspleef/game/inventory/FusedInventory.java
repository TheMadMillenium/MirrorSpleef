package dev.tmm.mirrorspleef.game.inventory;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.items.GameItem;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class FusedInventory extends GameInventory {
    public FusedInventory(GameState state, GamePlayer gp, Plugin plugin) {
        super(state, gp, plugin);
    }

    public void addInventory(GameInventory inventory) {
        List<GameItem> items = inventory.items;

        for(GameItem item : items) {
            this.addItem(item);
        }
    }
}
