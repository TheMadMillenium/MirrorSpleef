package dev.tmm.mirrorspleef.game.inventory;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.items.GameItem;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameInventory implements Listener {
    List<GameItem> items;
    protected GameState state;
    protected GamePlayer gp;
    protected Plugin plugin;

    protected GameInventory(GameState state, GamePlayer gp, Plugin plugin) {
        this.items = new ArrayList<>();
        this.state = state;
        this.plugin = plugin;
        this.gp = gp;
    }

    protected final void addItem(GameItem item) {
        this.items.add(item);
        item.setParent(this);
    }

    public final boolean containsItems() {
        Inventory inv = gp.getBukkitPlayer().getInventory();

        for (ItemStack invItem : inv.getContents()) {
            for (GameItem item : this.items) {
                if (item.equalTo(invItem)) {
                    return true;
                }
            }
        }

        return false;
    }

    public final void setInventory() {
        gp.getBukkitPlayer().closeInventory();

        Inventory inv = gp.getBukkitPlayer().getInventory();
        inv.setMaxStackSize(127);

        inv.clear();

        for (GameItem stack : this.items) {
            Bukkit.getPluginManager().registerEvents(stack, this.plugin);
            inv.addItem(stack.getBukkitStack());
        }
    }

    public final void overlayInventory() {
        gp.getBukkitPlayer().closeInventory();

        Inventory inv = gp.getBukkitPlayer().getInventory();
        inv.setMaxStackSize(127);

        for (GameItem stack : this.items) {
            Bukkit.getPluginManager().registerEvents(stack, this.plugin);
            inv.addItem(stack.getBukkitStack());
        }
    }

    public void unregisterItems() {
        Iterator<GameItem> itemIt = this.items.iterator();

        while (itemIt.hasNext()) {
            HandlerList.unregisterAll(itemIt.next());
            itemIt.remove();
        }
    }

    public void remove(GameItem item) {
        if(!this.items.remove(item)) return;
        HandlerList.unregisterAll(item);
        item.setParent(null);
    }

    public GamePlayer getOwner() {
        return this.gp;
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }
}