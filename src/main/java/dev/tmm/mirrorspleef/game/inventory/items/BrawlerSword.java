package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

public class BrawlerSword extends GameItem {
    public BrawlerSword(GameState state, GamePlayer owner, int knockback) {
        super(new NBTItemStack(Material.WOOD_SWORD)
                        .setName(ChatColor.GREEN + "Brawler's Sword")
                        .addEnchant(Enchantment.KNOCKBACK, (short) knockback)
                        .setUnbreakable(true)
                        .addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ATTRIBUTES),
                state, owner, knockback);
    }
}
