package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.JumpPotion;
import org.bukkit.plugin.Plugin;

public class FrogInventory extends GameInventory {
    public FrogInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(
                new JumpPotion(
                        respawning ?
                                UpgradeConstants.computeClassQuantityUpgrade(GameClass.FROG, 0) :
                                gp.getCurrentGameData().getClassQuantity(GameClass.FROG),
                        respawning ?
                                UpgradeConstants.computeClassPotencyUpgrade(GameClass.FROG, 0) :
                                gp.getCurrentGameData().getClassPotency(GameClass.FROG),
                        state, gp
                )
        );
    }
}
