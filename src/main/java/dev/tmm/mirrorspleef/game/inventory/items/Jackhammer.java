package dev.tmm.mirrorspleef.game.inventory.items;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.scheduler.BukkitRunnable;

public class Jackhammer extends GameItem {
    private MutableBoolean instamineEnabled;

    public Jackhammer(int durability, GameState state, GamePlayer owner) {
        super(
                new NBTItemStack(Material.IRON_PICKAXE, 1, (short) (Material.IRON_PICKAXE.getMaxDurability() - durability + 1))
                        .setName(ChatColor.GREEN + "Jackhammer")
                        .setCanDestroy(Material.STAINED_GLASS)
                        .addItemFlags(ItemFlag.HIDE_ATTRIBUTES),
                state, owner, durability
        );

        this.instamineEnabled = new MutableBoolean(false);
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent e) {
        if (this.owner.isPerkActive()) {
            e.setInstaBreak(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!this.equalTo(e.getPlayer().getItemInHand())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        if (!this.state.world.isInSelection(e.getBlock())) {
            e.setCancelled(true);
            return;
        }

        final World w = e.getPlayer().getWorld();
        final Location blockPos = e.getBlock().getLocation();

        this.stack.setDamage((short) (this.getBukkitStack().getDurability() - 1));

        this.state.runNextTick(new BukkitRunnable() {
            @Override
            public void run() {
                Block currentBlock;

                GameState state = Jackhammer.this.state;

                for (int x = -1; x <= 1; x++) {
                    for (int z = -1; z <= 1; z++) {
                        if (state.world.isInSelection(
                                currentBlock = w.getBlockAt(
                                        blockPos.getBlockX() + x,
                                        blockPos.getBlockY(),
                                        blockPos.getBlockZ() + z
                                )
                        )) {
                            state.updateInstamine(currentBlock);
                            state.world.onBlockBreak(currentBlock);
                        }
                    }
                }
            }
        });
    }

    @EventHandler
    public void onToolBreak(PlayerItemBreakEvent e) {
        if (!this.equalTo(e.getBrokenItem())) return;
        if (!this.owner.getBukkitPlayer().equals(e.getPlayer())) return;

        this.owner.getCurrentGameData().increaseItemSpace();
        this.removeFromParent();
    }
}
