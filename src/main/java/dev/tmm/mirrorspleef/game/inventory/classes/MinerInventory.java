package dev.tmm.mirrorspleef.game.inventory.classes;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.items.Jackhammer;
import org.bukkit.plugin.Plugin;

public class MinerInventory extends GameInventory {
    public MinerInventory(GameState state, GamePlayer gp, boolean respawning, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(new Jackhammer(respawning ?
                UpgradeConstants.computeClassPotencyUpgrade(GameClass.MINER, 0) :
                gp.getCurrentGameData().getClassPotency(GameClass.MINER),
                state, gp
        ));
    }
}
