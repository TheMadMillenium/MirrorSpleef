package dev.tmm.mirrorspleef.game.inventory;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.inventory.items.SelectClass;
import org.bukkit.plugin.Plugin;

public class PreGameInventory extends ImmutableInventory {
    public PreGameInventory(GameState state, GamePlayer gp, Plugin plugin) {
        super(state, gp, plugin);

        this.addItem(new SelectClass(state, gp, plugin));
    }
}
