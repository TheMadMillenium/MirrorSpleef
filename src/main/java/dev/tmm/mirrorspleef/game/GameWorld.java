package dev.tmm.mirrorspleef.game;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import dev.tmm.mirrorspleef.base.world.BlockSelection;
import dev.tmm.mirrorspleef.base.world.CoordBoundary;
import dev.tmm.mirrorspleef.base.world.StructureLink;
import dev.tmm.mirrorspleef.base.world.StructureRotation;
import dev.tmm.mirrorspleef.game.collectibles.Collectible;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import net.minecraft.server.v1_8_R3.EntityFireball;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftFireball;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;

import java.util.*;

public class GameWorld {
    private World world;
    private StructureLink link;
    private Location srcSpecSpawn;
    private Location linkSpecSpawn;
    private CoordBoundary mapBounds;

    private StructureLink originalStructure;

    private BlockSelection srcSelection;
    private BlockSelection linkSelection;

    private BiMap<Collectible, Collectible> collectibles;
    private BiMap<Collectible, Collectible> invCollectibles;

    private Map<Fireball, Fireball> fireballs;
    private Map<Fireball, Location> fireballTargets;

    private Map<Block, Integer> srcTraps;
    private Map<Block, Integer> linkTraps;

    private Set<Block> instamines;

    public GameWorld(World world) {
        this.world = world;

        this.collectibles = HashBiMap.create();
        this.invCollectibles = this.collectibles.inverse();

        this.fireballs = new HashMap<>();
        this.fireballTargets = new HashMap<>();

        this.srcTraps = new HashMap<>();
        this.linkTraps = new HashMap<>();

        this.instamines = new HashSet<>();
    }

    public void setOriginal(Location a, Location b) {
        this.originalStructure = new StructureLink(a, b);
    }

    public void reloadArenas() {
        this.originalStructure.syncStructures();
        this.link.syncStructures();
    }

    public void setStructureSrc(Location src) {
        Location b = src.clone().add(this.originalStructure.getB()).subtract(this.originalStructure.getA());

        this.link = new StructureLink(src, b);
        this.originalStructure.setLink(this.world, src);
    }

    public void setStructureRotation(StructureRotation rotation) {
        this.link.setFlipAxis(rotation);
    }

    public void setStructureLink(Location link) {
        this.throwIfNull(this.link, "Structure source has not been set");

        this.link.setLink(this.world, link);
    }

    public void setSrcSelection(BlockSelection selection) {
        this.srcSelection = selection;
    }

    public void setLinkSelection(BlockSelection selection) {
        this.linkSelection = selection;
    }

    public void setMapBounds(CoordBoundary bounds) {
        this.mapBounds = bounds;
    }

    public boolean withinPlayerBounds(Location loc) {
        return this.mapBounds.withinBounds(loc);
    }

    public Location linkToSrc(Location loc) {
        return this.link.coordsLinkToSrc(loc);
    }

    public Location srcToLink(Location loc) {
        return this.link.coordsSrcToLink(loc);
    }

    public StructureRotation getLinkRotation() {
        return this.link.getRotation();
    }

    public void setSrcSpecSpawn(Location respawnLoc) {
        this.srcSpecSpawn = respawnLoc;
    }

    public void setLinkSpecSpawn(Location respawnLoc) {
        this.linkSpecSpawn = respawnLoc;
    }

    public Location getRespawnLoc(GameTeam gameTeam) {
        switch (gameTeam) {
            case TEAM_SRC:
                return this.srcSpecSpawn;
            case TEAM_LINK:
                return this.linkSpecSpawn;
        }

        return null;
    }

    public Location getSrcSpecSpawn() {
        return this.srcSpecSpawn;
    }

    public boolean linkExists() {
        return this.link != null;
    }

    public Block getRandomSrcBlock() {
        return this.srcSelection.getRandomBlock();
    }

    public Block getRandomLinkBlock() {
        return this.linkSelection.getRandomBlock();
    }

    public Block[] getRandomSrcBlocks(int amt) {
        this.throwIfNull(this.srcSelection, "Source selection has not been set");
        if (amt > this.srcSelection.getNumBlocks())
            throw new IllegalArgumentException("Amount cannot exceed the total number of blocks");
        if (amt < 0) throw new IllegalArgumentException("Amount cannot be negative");

        Block[] out = new Block[amt];

        for (int i = 0; i < amt; i++) {
            out[i] = this.srcSelection.getRandomBlock();
            this.srcSelection.exclude(out[i].getLocation());
        }

        for (Block b : out) {
            this.srcSelection.include(b.getLocation());
        }

        return out;
    }

    public Block[] getRandomLinkBlocks(int amt) {
        this.throwIfNull(this.linkSelection, "Link selection has not been set");
        if (amt > this.linkSelection.getNumBlocks())
            throw new IllegalArgumentException("Amount cannot exceed the total number of blocks");
        if (amt < 0) throw new IllegalArgumentException("Amount can't be negative");

        Block[] out = new Block[amt];

        for (int i = 0; i < amt; i++) {
            out[i] = this.linkSelection.getRandomBlock();
            this.linkSelection.exclude(out[i].getLocation());
        }

        for (Block b : out) {
            this.linkSelection.include(b.getLocation());
        }

        return out;
    }

    public void setBlocks(BlockSelection bounds, org.bukkit.Material type, byte data) {
        Location a = bounds.getA();
        Location b = bounds.getB();

        for (int x = a.getBlockX(); x <= b.getBlockX(); x++) {
            for (int y = a.getBlockY(); y <= b.getBlockY(); y++) {
                if (y < 0) continue;
                if (y > 255) continue;

                for (int z = a.getBlockZ(); z <= b.getBlockZ(); z++) {
                    if (!bounds.inSelection(new Location(null, x, y, z))) continue;

                    Block block = this.world.getBlockAt(x, y, z);
                    block.setType(type);
                    block.setData(data);

                    onBlockPlace(block);
                }
            }
        }
    }

    public void onBlockPlace(Block b) {
        this.throwIfNull(this.link, "Structure source has not been set");

        this.link.onBlockPlace(b);
        this.srcSelection.include(b.getLocation());
        this.linkSelection.include(b.getLocation());
    }

    public void onBlockBreak(Block b) {
        this.throwIfNull(this.link, "Structure source has not been set");

        Collectible remove = null;

        for (Collectible c : this.collectibles.keySet()) {
            if (c.getSrcBlock().equals(b)) {
                remove = c;
            }
        }

        if (remove != null) this.removeItem(remove);
        else {
            for (Collectible c : this.invCollectibles.keySet()) {
                if (c.getSrcBlock().equals(b)) {
                    remove = c;
                }
            }

            if (remove != null) this.removeItem(remove);
        }

        this.link.onBlockBreak(b);
        this.srcSelection.exclude(b.getLocation());
        this.linkSelection.exclude(b.getLocation());
    }

    public boolean isInSelection(Block b) {
        return this.srcSelection.inSelection(b.getLocation()) || this.linkSelection.inSelection(b.getLocation());
    }

    public boolean isInSelectionBounds(Block b) {
        return this.srcSelection.withinBounds(b.getLocation()) || this.linkSelection.withinBounds(b.getLocation());
    }

    public boolean isInLink(Location loc) {
        return this.link.isInLink(loc);
    }

    public void addCollectible(Collectible src, Collectible flipped) {
        this.collectibles.put(src, flipped);
    }

    public boolean containsItem(Collectible c) {
        return this.collectibles.containsKey(c) || this.invCollectibles.containsKey(c);
    }

    public void removeItem(Collectible col) {
        Collectible mirrored;

        if (this.collectibles.containsKey(col)) {
            mirrored = this.collectibles.get(col);
        } else {
            mirrored = this.invCollectibles.get(col);
        }

        if (mirrored == null) return;

        col.hideFromAll();
        mirrored.hideFromAll();

        HandlerList.unregisterAll(col);
        HandlerList.unregisterAll(mirrored);

        this.collectibles.remove(col);
    }

    public void clearItems() {
        for (Collectible col : this.collectibles.keySet()) {
            Collectible mirrored = this.collectibles.get(col);

            col.hideFromAll();
            mirrored.hideFromAll();

            HandlerList.unregisterAll(col);
            HandlerList.unregisterAll(mirrored);
        }

        this.collectibles.clear();
    }

    public boolean addTrap(Block b, int range, GameTeam team) {
        Map<Block, Integer> traps = team == GameTeam.TEAM_SRC ? this.srcTraps : this.linkTraps;

        if (traps.containsKey(b)) return false;
        traps.put(b, range);

        return true;
    }

    public int getTrapRange(Block b, GameTeam team) {
        if (team == GameTeam.TEAM_SRC) return this.srcTraps.get(b);
        else return this.linkTraps.get(b);
    }

    public void removeTrap(Block b) {
        this.srcTraps.remove(b);
        this.linkTraps.remove(b);
    }

    public void clearTraps() {
        this.srcTraps.clear();
        this.linkTraps.clear();
    }

    public Block getActivatedTrap(Player p, GameTeam team) {
        Map<Block, Integer> traps = team == GameTeam.TEAM_SRC ? this.srcTraps : this.linkTraps;

        for (Block b : traps.keySet()) {
            Block playerOn = team == GameTeam.TEAM_SRC ?
                    this.srcToLink(b.getLocation().add(0.5, 0.9, 0.5)).getBlock() :
                    this.linkToSrc(b.getLocation().add(0.5, 0.9, 0.5)).getBlock();

            if (p.getLocation().subtract(0, 1, 0).getBlock().equals(playerOn)) return b;
        }

        return null;
    }

    public void spawnSrcFireball(Location spawnLoc, Location headingTo, double speed) {
        headingTo.add(0.5, 0.5, 0.5);
        Fireball fb = this.world.spawn(spawnLoc, LargeFireball.class);
        EntityFireball cfb = ((CraftFireball) fb).getHandle();

        Vector direction = headingTo.toVector().subtract(spawnLoc.toVector()).normalize().multiply(speed);

        cfb.dirX = direction.getX();
        cfb.dirY = direction.getY();
        cfb.dirZ = direction.getZ();

        Fireball mirror_fb = this.world.spawn(this.srcToLink(spawnLoc), LargeFireball.class);
        EntityFireball mirror_cfb = ((CraftFireball) mirror_fb).getHandle();

        direction = this.srcToLink(headingTo).toVector()
                .subtract(this.srcToLink(spawnLoc).toVector())
                .normalize().multiply(speed);

        mirror_cfb.dirX = direction.getX();
        mirror_cfb.dirY = direction.getY();
        mirror_cfb.dirZ = direction.getZ();

        this.fireballs.put(fb, mirror_fb);
        this.fireballTargets.put(fb, headingTo);
    }

    public void spawnLinkFireball(Location spawnLoc, Location headingTo, double speed) {
        headingTo.add(0.5, 0.5, 0.5);
        Fireball fb = this.world.spawn(spawnLoc, LargeFireball.class);
        EntityFireball cfb = ((CraftFireball) fb).getHandle();

        Vector direction = headingTo.toVector().subtract(spawnLoc.toVector()).normalize().multiply(speed);

        cfb.dirX = direction.getX();
        cfb.dirY = direction.getY();
        cfb.dirZ = direction.getZ();

        Fireball mirror_fb = this.world.spawn(this.linkToSrc(spawnLoc), LargeFireball.class);
        EntityFireball mirror_cfb = ((CraftFireball) mirror_fb).getHandle();

        direction = this.linkToSrc(headingTo).toVector()
                .subtract(this.linkToSrc(spawnLoc).toVector())
                .normalize().multiply(speed);

        mirror_cfb.dirX = direction.getX();
        mirror_cfb.dirY = direction.getY();
        mirror_cfb.dirZ = direction.getZ();

        this.fireballs.put(fb, mirror_fb);
        this.fireballTargets.put(fb, headingTo);
    }

    public boolean isOriginalFireball(Entity fb) {
        return this.fireballs.containsKey(fb);
    }

    public Location getFireballTarget(Fireball proj) {
        return this.fireballTargets.get(proj);
    }

    public void removeFireball(Fireball fb) {
        if (!this.fireballs.containsKey(fb)) return;

        this.fireballs.get(fb).remove();
        fb.remove();
    }

    public int spawnInstamines(double probability) {
        this.clearInstamines();

        int genBlocks = (int) (this.srcSelection.getNumBlocks() * probability);

        Block[] blocks = this.getRandomSrcBlocks(genBlocks);
        Collections.addAll(this.instamines, blocks);

        return blocks.length;
    }

    public boolean containsInstamine(Block b) {
        if (this.isInLink(b.getLocation())) {
            return this.instamines.contains(this.linkToSrc(b.getLocation()).getBlock());
        }

        return this.instamines.contains(b);
    }

    public void clearInstamines() {
        this.instamines.clear();
    }

    private void throwIfNull(Object obj, String error) {
        if (obj == null) throw new IllegalStateException(error);
    }
}
