package dev.tmm.mirrorspleef.game.data;

import java.util.HashMap;
import java.util.Map;

public class PlayerData {
    private GamePlayer p;
    private Map<GameClass, Integer> quantityLevels;
    private Map<GameClass, Integer> potencyLevels;
    private Map<GeneralUpgrade, Integer> generalLevels;

    public PlayerData(GamePlayer p) {
        this.quantityLevels = new HashMap<>();
        this.potencyLevels = new HashMap<>();
        this.generalLevels = new HashMap<>();
        this.p = p;
    }

    public int getQuantityLevel(GameClass cl) {
        return this.getOrDefault(this.quantityLevels, cl, 0);
    }

    public int getPotencyLevel(GameClass cl) {
        return this.getOrDefault(this.potencyLevels, cl, 0);
    }

    public int getGeneralUpgradeLevel(GeneralUpgrade upgrade) {
        return this.getOrDefault(this.generalLevels, upgrade, 0);
    }

    public GamePlayer getPlayer() {
        return this.p;
    }

    public void setQuantityLevel(GameClass cl, int quantityLevel) {
        this.quantityLevels.put(cl, quantityLevel);
    }

    public void setPotencyLevel(GameClass cl, int potencyLevel) {
        this.potencyLevels.put(cl, potencyLevel);
    }

    public void setGeneralUpgradeLevel(GeneralUpgrade upgrade, int level) {
        this.generalLevels.put(upgrade, level);
    }

    private <K, V> V getOrDefault(Map<K, V> map, K key, V defaultValue) {
        V out = map.get(key);

        if (out == null) return defaultValue;
        return out;
    }
}
