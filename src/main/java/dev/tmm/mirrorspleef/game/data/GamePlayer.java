package dev.tmm.mirrorspleef.game.data;

import dev.tmm.mirrorspleef.base.entity.PacketScoreboardTeam;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.inventory.FusedInventory;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public class GamePlayer {
    private Plugin plugin;

    private Player p;
    private PlayerData data;

    private List<GameInventory> inventories;
    private List<PacketScoreboardTeam> visibleTeams;

    private boolean inGame;

    private GameTeam team;
    private ComputedPlayerData currentGameData;
    private GameClass activeClass;

    private boolean isAlive;
    private boolean canDoubleJump;
    private boolean isPerkActive;

    public GamePlayer(Player p, Plugin plugin) {
        this.plugin = plugin;

        this.p = p;
        this.data = new PlayerData(this);

        this.inventories = new ArrayList<>();
        this.visibleTeams = new ArrayList<>();

        this.activeClass = GameClass.RANDOM;
    }

    public Player getBukkitPlayer() {
        return this.p;
    }

    public void setInventory(GameInventory inventory) {
        this.clearInventories();

        Bukkit.getPluginManager().registerEvents(inventory, this.plugin);
        this.inventories.add(inventory);
        inventory.setInventory();
    }

    public void overlayInventory(GameInventory inventory) {
        Bukkit.getPluginManager().registerEvents(inventory, this.plugin);
        this.inventories.add(inventory);
        inventory.overlayInventory();
    }

    public void clearInventories() {
        for (GameInventory inv : this.inventories) {
            inv.unregisterItems();
            HandlerList.unregisterAll(inv);
        }

        this.inventories.clear();
    }

    public FusedInventory fuseInventories(GameState state) {
        FusedInventory fused = new FusedInventory(state, this, this.plugin);

        for (GameInventory inv : this.inventories) fused.addInventory(inv);

        return fused;
    }

    public void addVisibleTeam(PacketScoreboardTeam team) {
        this.visibleTeams.add(team);
        team.sendTeam(this.p);
    }

    public void clearVisibleTeams() {
        for(PacketScoreboardTeam team : this.visibleTeams) {
            team.removeTeam(this.p);
        }

        this.visibleTeams.clear();
    }

    public void setTeam(GameTeam team) {
        this.team = team;
    }

    public void setAlive(boolean alive) {
        this.isAlive = alive;
    }

    public void activatePerk() {
        this.isPerkActive = true;
    }

    public void deactivatePerk() {
        this.isPerkActive = false;
    }

    public PlayerData getPlayerData() {
        return this.data;
    }

    public void computeData() {
        this.currentGameData = new ComputedPlayerData(this.data);
    }

    public ComputedPlayerData getCurrentGameData() {
        return this.currentGameData;
    }

    public GameTeam getTeam() {
        return this.team;
    }

    public GameClass getActiveClass() {
        return this.activeClass;
    }

    public boolean isPerkActive() {
        return this.isPerkActive;
    }

    public boolean canDoubleJump() {
        return this.canDoubleJump;
    }

    public boolean isInGame() {
        return this.inGame;
    }

    public boolean isAlive() {
        return this.isAlive;
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }

    public void setCanDoubleJump(boolean canDoubleJump) {
        this.canDoubleJump = canDoubleJump;
    }

    public void setActiveClass(GameClass cl) {
        this.activeClass = cl;
    }

    public void removeAllPotions() {
        for (PotionEffect pe : this.p.getActivePotionEffects()) {
            this.p.removePotionEffect(pe.getType());
        }
    }
}