package dev.tmm.mirrorspleef.game.data;

public enum GameStage {
    LOBBY,
    PREGAME,
    GRACE,
    FIREBALL_4S,
    FIREBALL_2S,
    FIREBALL_1S,
    SUDDEN_DEATH,
    ENDGAME;

    public boolean inProgress() {
        switch (this) {
            case LOBBY:
                return false;
        }

        return true;
    }
}
