package dev.tmm.mirrorspleef.game.data;

public enum GameTeam {
    TEAM_SRC,
    TEAM_LINK;

    public GameTeam next() {
        return values()[(this.ordinal() + 1) % values().length];
    }
}
