package dev.tmm.mirrorspleef.game.data;

import dev.tmm.mirrorspleef.base.ui.ChatColor;

import java.util.HashMap;
import java.util.Map;

public class UpgradeConstants {
    private static final Map<GeneralUpgrade, Integer> baseGeneralQuantities;
    private static final Map<GeneralUpgrade, Integer> maxGeneralLevels;
    private static final Map<GeneralUpgrade, Integer> generalChangePerLevel;

    public static final int MAX_QUANTITY_UPGRADES = 8;
    public static final int MAX_POTENCY_UPGRADES = 4;

    private static final Map<GameClass, Integer> baseClassQuantities;
    private static final Map<GameClass, Integer> baseClassPotencies;
    private static final Map<GameClass, Integer> quantityChangePerLevel;
    private static final Map<GameClass, Integer> potencyChangePerLevel;

    private static final Map<Integer, String> potencyPrefix;

    public static int getMaxGeneralUpgrade(GeneralUpgrade upgrade) {
        return maxGeneralLevels.get(upgrade);
    }

    public static int computeGeneralUpgrade(GeneralUpgrade upgrade, int level) {
        return baseGeneralQuantities.get(upgrade) + generalChangePerLevel.get(upgrade) * level;
    }

    public static int computeClassQuantityUpgrade(GameClass cl, int level) {
        return baseClassQuantities.get(cl) + quantityChangePerLevel.get(cl) * level;
    }

    public static int computeClassPotencyUpgrade(GameClass cl, int level) {
        return baseClassPotencies.get(cl) + potencyChangePerLevel.get(cl) * level;
    }

    public static String getPotencyPrefix(int level) {
        return potencyPrefix.get(level);
    }

    static {
        baseGeneralQuantities = new HashMap<>();
        baseGeneralQuantities.put(GeneralUpgrade.BLOCKS, 20);
        baseGeneralQuantities.put(GeneralUpgrade.DOUBLE_JUMPS, 0);
        baseGeneralQuantities.put(GeneralUpgrade.ITEM_SPACE, 3);
        baseGeneralQuantities.put(GeneralUpgrade.LIVES, 3);

        maxGeneralLevels = new HashMap<>();
        maxGeneralLevels.put(GeneralUpgrade.BLOCKS, 8);
        maxGeneralLevels.put(GeneralUpgrade.DOUBLE_JUMPS, 7);
        maxGeneralLevels.put(GeneralUpgrade.LIVES, 5);
        maxGeneralLevels.put(GeneralUpgrade.ITEM_SPACE, 5);

        generalChangePerLevel = new HashMap<>();
        generalChangePerLevel.put(GeneralUpgrade.BLOCKS, 10);
        generalChangePerLevel.put(GeneralUpgrade.DOUBLE_JUMPS, 1);
        generalChangePerLevel.put(GeneralUpgrade.LIVES, 1);
        generalChangePerLevel.put(GeneralUpgrade.ITEM_SPACE, 1);

        baseClassQuantities = new HashMap<>();
        baseClassQuantities.put(GameClass.SCOUT, 16 * GameConstants.TICKS_PER_SEC);
        baseClassQuantities.put(GameClass.FROG, 16 * GameConstants.TICKS_PER_SEC);
        baseClassQuantities.put(GameClass.MINER, GameConstants.INSTAMINE_TIME + 1 * GameConstants.TICKS_PER_SEC);
        baseClassQuantities.put(GameClass.ENGINEER, 2);
        baseClassQuantities.put(GameClass.PERKSMITH, 1);
        baseClassQuantities.put(GameClass.BRAWLER, 8 * GameConstants.TICKS_PER_SEC);
        baseClassQuantities.put(GameClass.THIEF, 1);

        baseClassPotencies = new HashMap<>();
        baseClassPotencies.put(GameClass.SCOUT, 0);
        baseClassPotencies.put(GameClass.FROG, 0);
        baseClassPotencies.put(GameClass.MINER, 3);
        baseClassPotencies.put(GameClass.ENGINEER, 2);
        baseClassPotencies.put(GameClass.PERKSMITH, 30 * GameConstants.TICKS_PER_SEC);
        baseClassPotencies.put(GameClass.BRAWLER, 1);
        baseClassPotencies.put(GameClass.THIEF, 1);

        quantityChangePerLevel = new HashMap<>();
        quantityChangePerLevel.put(GameClass.SCOUT, 4 * GameConstants.TICKS_PER_SEC);
        quantityChangePerLevel.put(GameClass.FROG, 4 * GameConstants.TICKS_PER_SEC);
        quantityChangePerLevel.put(GameClass.MINER, 1 * GameConstants.TICKS_PER_SEC);
        quantityChangePerLevel.put(GameClass.ENGINEER, 1);
        quantityChangePerLevel.put(GameClass.PERKSMITH, 1);
        quantityChangePerLevel.put(GameClass.BRAWLER, 2 * GameConstants.TICKS_PER_SEC);
        quantityChangePerLevel.put(GameClass.THIEF, 1);

        potencyChangePerLevel = new HashMap<>();
        potencyChangePerLevel.put(GameClass.SCOUT, 1);
        potencyChangePerLevel.put(GameClass.FROG, 1);
        potencyChangePerLevel.put(GameClass.MINER, 1);
        potencyChangePerLevel.put(GameClass.ENGINEER, 1);
        potencyChangePerLevel.put(GameClass.PERKSMITH, -4 * GameConstants.TICKS_PER_SEC);
        potencyChangePerLevel.put(GameClass.BRAWLER, 1);
        potencyChangePerLevel.put(GameClass.THIEF, 1);

        potencyPrefix = new HashMap<>();
        potencyPrefix.put(0, ChatColor.GREEN.toString());
        potencyPrefix.put(1, ChatColor.GOLD.toString());
        potencyPrefix.put(2, ChatColor.YELLOW.toString());
        potencyPrefix.put(3, ChatColor.RED.toString() + ChatColor.BOLD);
        potencyPrefix.put(4, ChatColor.DARK_RED.toString() + ChatColor.BOLD);
    }
}
