package dev.tmm.mirrorspleef.game.data;

import org.bukkit.entity.Player;

import java.util.*;

public class GamePlayerList implements Iterable<GamePlayer> {
    private Map<Player, GamePlayer> players;

    public GamePlayerList() {
        this.players = new HashMap<>();
    }

    public GamePlayer getPlayer(Player p) {
        return this.players.get(p);
    }

    public void addPlayer(GamePlayer gp) {
        this.players.put(gp.getBukkitPlayer(), gp);
    }

    public void removePlayer(Player p) {
        this.players.remove(p);
    }

    public List<GamePlayer> getCurrentPlayers() {
        return new ArrayList<>(this.players.values());
    }

    public List<GamePlayer> getCurrentPlayersInGame() {
        ArrayList<GamePlayer> out = new ArrayList<>();

        for(GamePlayer gp : this) {
            if(gp.isInGame()) {
                out.add(gp);
            }
        }

        return out;
    }

    @Override
    public Iterator<GamePlayer> iterator() {
        return this.players.values().iterator();
    }
}
