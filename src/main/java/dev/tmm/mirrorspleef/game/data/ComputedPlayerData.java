package dev.tmm.mirrorspleef.game.data;

import java.util.HashMap;
import java.util.Map;

public class ComputedPlayerData {
    private Map<GameClass, Integer> quantityValues;
    private Map<GameClass, Integer> potencyValues;
    private Map<GeneralUpgrade, Integer> generalValues;

    public ComputedPlayerData(PlayerData data) {
        this.quantityValues = new HashMap<>();
        this.potencyValues = new HashMap<>();
        this.generalValues = new HashMap<>();

        this.computeQuantities(data);
        this.computePotencies(data);
        this.computeGeneral(data);
    }

    private void computeQuantities(PlayerData data) {
        for (GameClass cl : GameClass.values()) {
            if (cl == GameClass.RANDOM) continue;

            this.quantityValues.put(
                    cl,
                    UpgradeConstants.computeClassQuantityUpgrade(
                            cl,
                            data.getQuantityLevel(cl)
                    )
            );
        }
    }

    private void computePotencies(PlayerData data) {
        for (GameClass cl : GameClass.values()) {
            if (cl == GameClass.RANDOM) continue;

            this.potencyValues.put(
                    cl,
                    UpgradeConstants.computeClassPotencyUpgrade(
                            cl,
                            data.getPotencyLevel(cl)
                    )
            );
        }
    }

    private void computeGeneral(PlayerData data) {
        for (GeneralUpgrade upgrade : GeneralUpgrade.values()) {
            this.generalValues.put(
                    upgrade,
                    UpgradeConstants.computeGeneralUpgrade(
                            upgrade,
                            data.getGeneralUpgradeLevel(upgrade)
                    )
            );
        }
    }

    public int getClassQuantity(GameClass cl) {
        return this.quantityValues.get(cl);
    }

    public int getClassPotency(GameClass cl) {
        return this.potencyValues.get(cl);
    }

    public int getBlocks() {
        return this.generalValues.get(GeneralUpgrade.BLOCKS);
    }

    public int getLives() {
        return this.generalValues.get(GeneralUpgrade.LIVES);
    }

    public boolean hasDoubleJumps() {
        return this.generalValues.get(GeneralUpgrade.DOUBLE_JUMPS) > 0;
    }

    public boolean hasBlocks() {
        return this.generalValues.get(GeneralUpgrade.BLOCKS) > 0;
    }

    public boolean hasLives() {
        return this.generalValues.get(GeneralUpgrade.LIVES) > 0;
    }

    public boolean hasItemSpace() {
        return this.generalValues.get(GeneralUpgrade.ITEM_SPACE) > 0;
    }

    public void decreaseBlocks() {
        this.generalValues.put(
                GeneralUpgrade.BLOCKS,
                this.generalValues.get(GeneralUpgrade.BLOCKS) - 1
        );
    }

    public void decreaseDoubleJumps() {
        this.generalValues.put(
                GeneralUpgrade.DOUBLE_JUMPS,
                this.generalValues.get(GeneralUpgrade.DOUBLE_JUMPS) - 1
        );
    }

    public void decreaseLives() {
        this.generalValues.put(
                GeneralUpgrade.LIVES,
                this.generalValues.get(GeneralUpgrade.LIVES) - 1
        );
    }

    public void increaseItemSpace() {
        this.generalValues.put(
                GeneralUpgrade.ITEM_SPACE,
                this.generalValues.get(GeneralUpgrade.ITEM_SPACE) + 1
        );
    }

    public void decreaseItemSpace() {
        this.generalValues.put(
                GeneralUpgrade.ITEM_SPACE,
                this.generalValues.get(GeneralUpgrade.ITEM_SPACE) - 1
        );
    }

    public void setLives(int lives) {
        this.generalValues.put(GeneralUpgrade.LIVES, lives);
    }
}
