package dev.tmm.mirrorspleef.game.data;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.util.RandomUtils;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public enum GameClass {
    SCOUT(new NBTItemStack(Material.POTION, 1, (short) 8194)
            .setName(ChatColor.GREEN + "Scout Upgrades")
            .addItemFlags(ItemFlag.HIDE_POTION_EFFECTS)
            .getBukkitStack()),

    FROG(new NBTItemStack(Material.WATER_LILY)
            .setName(ChatColor.GREEN + "Frog Upgrades")
            .getBukkitStack()),

    MINER(new NBTItemStack(Material.IRON_PICKAXE)
            .setName(ChatColor.GREEN + "Miner Upgrades")
            .addItemFlags(ItemFlag.HIDE_ATTRIBUTES)
            .getBukkitStack()),

    ENGINEER(new NBTItemStack(Material.TNT)
            .setName(ChatColor.GREEN + "Engineer Upgrades")
            .getBukkitStack()),

    PERKSMITH(new NBTItemStack(Material.ANVIL)
            .setName(ChatColor.GREEN + "Perksmith Upgrades")
            .getBukkitStack()),

    BRAWLER(new NBTItemStack(Material.EYE_OF_ENDER)
            .setName(ChatColor.GREEN + "Brawler Upgrades")
            .getBukkitStack()),

    THIEF(new NBTItemStack(Material.GOLD_INGOT)
            .setName(ChatColor.GREEN + "Thief Upgrades")
            .getBukkitStack()),

    RANDOM(null);

    private ItemStack stack;

    GameClass(ItemStack stack) {
        this.stack = stack;
    }

    public ItemStack getItemStack() {
        return this.stack;
    }

    public static GameClass getRandomClass() {
        return GameClass.values()[RandomUtils.getInt(GameClass.values().length - 1)];
    }
}
