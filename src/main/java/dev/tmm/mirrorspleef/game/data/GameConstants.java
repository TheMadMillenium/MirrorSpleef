package dev.tmm.mirrorspleef.game.data;

import dev.tmm.mirrorspleef.base.ui.ChatColor;

import java.util.HashMap;
import java.util.Map;

public class GameConstants {
    public static final int TICKS_PER_SEC = 20;

    public static final int LOBBY_LENGTH = 20 * TICKS_PER_SEC;
    public static final int PREGAME_LENGTH = 15 * TICKS_PER_SEC;
    public static final int GRACE_PERIOD_LENGTH = 30 * TICKS_PER_SEC;
    public static final int FIREBALL_4S_LENGTH = 60 * TICKS_PER_SEC;
    public static final int FIREBALL_2S_LENGTH = 60 * TICKS_PER_SEC;
    public static final int FIREBALL_1S_LENGTH = 60 * TICKS_PER_SEC;
    public static final int SUDDEN_DEATH_LENGTH = Integer.MAX_VALUE; //The amount of blocks each player gets is finite, so it won't take long for everyone to fall.
    public static final int ENDGAME_LENGTH = 5 * TICKS_PER_SEC;

    public static final int SPAWN_PROT_LENGTH = 4 * TICKS_PER_SEC;

    public static final int DOUBLE_JUMP_COOLDOWN = TICKS_PER_SEC;
    public static final double DOUBLE_JUMP_VELOCITY = 1.5;

    public static final int MIN_ITEM_SPAWN_TIME = 15 * TICKS_PER_SEC;
    public static final int MAX_ITEM_SPAWN_TIME = 40 * TICKS_PER_SEC;
    public static final int ITEM_LIFESPAN = 15 * TICKS_PER_SEC;

    public static final int OPPONENT_TRAP_VISIBLE_TIME = 1 * TICKS_PER_SEC;

    public static final int INSTAMINE_TIME = 8 * TICKS_PER_SEC;
    public static final int INSTAMINE_SPAWN_TIME = 30 * TICKS_PER_SEC;
    public static final int INSTAMINE_SPAWN_TIME_INCREASE = 10 * TICKS_PER_SEC;
    public static final double INSTAMINE_PROBABILITY = 0.04;

    public static final short SRC_TEAM_COLOR = 14;
    public static final short LINK_TEAM_COLOR = 11;

    public static final ChatColor SRC_TEAM_CHAT_COLOR = ChatColor.RED;
    public static final ChatColor LINK_TEAM_CHAT_COLOR = ChatColor.BLUE;

    public static final String SRC_TEAM_COLOR_STRING = SRC_TEAM_CHAT_COLOR + "Red";
    public static final String LINK_TEAM_COLOR_STRING = LINK_TEAM_CHAT_COLOR + "Blue";

    public static final String ITEM_ID_STRING = "gameItemID";

    private static final Map<GameStage, Integer> collectiblePotencyLevel;

    public static int getCollectiblePotencyLevel(GameStage stage) {
        return collectiblePotencyLevel.get(stage);
    }

    static {
        collectiblePotencyLevel = new HashMap<>();
        collectiblePotencyLevel.put(GameStage.GRACE, 0);
        collectiblePotencyLevel.put(GameStage.FIREBALL_4S, 1);
        collectiblePotencyLevel.put(GameStage.FIREBALL_2S, 2);
        collectiblePotencyLevel.put(GameStage.FIREBALL_1S, 3);
        collectiblePotencyLevel.put(GameStage.SUDDEN_DEATH, UpgradeConstants.MAX_POTENCY_UPGRADES);
    }
}
