package dev.tmm.mirrorspleef.game.data;

public enum GeneralUpgrade {
    BLOCKS,
    DOUBLE_JUMPS,
    ITEM_SPACE,
    LIVES
}
