package dev.tmm.mirrorspleef.game.ui;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GeneralUpgrade;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.util.RomanNumeralFormat;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.inventory.ItemStack;

public class UpgradeNameTransformer {
    public static ItemStack classQuantitySetTexts(ItemStack in, GameClass cl, int level) {
        NBTItemStack builder = new NBTItemStack(in);

        switch (cl) {
            case SCOUT:
            case FROG:
            case MINER:
            case BRAWLER:
                builder.setName(ChatColor.GREEN + "Duration " + RomanNumeralFormat.format(level));
                break;
            case ENGINEER:
            case PERKSMITH:
            case THIEF:
                builder.setName(ChatColor.GREEN + "Quantity " + RomanNumeralFormat.format(level));
                break;
            default:
                return in;
        }

        int quantityVal = UpgradeConstants.computeClassQuantityUpgrade(cl, level);

        switch (cl) {
            case SCOUT:
                return builder.setLore(ChatColor.GRAY + "Speed lasts " + quantityVal/GameConstants.TICKS_PER_SEC + " seconds!").getBukkitStack();
            case FROG:
                return builder.setLore(ChatColor.GRAY + "Jump boost lasts " + quantityVal/GameConstants.TICKS_PER_SEC + " seconds!").getBukkitStack();
            case MINER:
                return builder.setLore(ChatColor.GRAY + "You get " + (quantityVal - GameConstants.INSTAMINE_TIME)/GameConstants.TICKS_PER_SEC + " more seconds of instamine!").getBukkitStack();
            case ENGINEER:
                return builder.setLore(ChatColor.GRAY + "Hold up to " + quantityVal + " traps at a time!").getBukkitStack();
            case PERKSMITH:
                return builder.setLore(ChatColor.GRAY + "Hold up to " + quantityVal + " refill" + (quantityVal == 1 ? "" : "s") + " at a time!").getBukkitStack();
            case BRAWLER:
                return builder.setLore(ChatColor.GRAY + "Stay on the enemy side for " + quantityVal/GameConstants.TICKS_PER_SEC + " seconds!").getBukkitStack();
            case THIEF:
                return builder.setLore(ChatColor.GRAY + "Hold up to " + quantityVal + " Swag Swipers at a time!").getBukkitStack();
            default:
                return in;
        }
    }

    public static ItemStack classPotencySetTexts(ItemStack in, GameClass cl, int level) {
        NBTItemStack builder = new NBTItemStack(in);

        switch (cl) {
            case SCOUT:
            case FROG:
            builder.setName(ChatColor.GREEN + "Amplitude " + RomanNumeralFormat.format(level));
                break;
            case MINER:
                builder.setName(ChatColor.GREEN + "Haste " + RomanNumeralFormat.format(level));
                break;
            case ENGINEER:
                builder.setName(ChatColor.GREEN + "Range " + RomanNumeralFormat.format(level));
                break;
            case PERKSMITH:
                builder.setName(ChatColor.GREEN + "Cooldown " + RomanNumeralFormat.format(level));
                break;
            case BRAWLER:
                builder.setName(ChatColor.GREEN + "Strength " + RomanNumeralFormat.format(level));
                break;
            case THIEF:
                builder.setName(ChatColor.GREEN + "Greed " + RomanNumeralFormat.format(level));
                break;
            default:
                return in;
        }

        int potencyVal = UpgradeConstants.computeClassPotencyUpgrade(cl, level);

        switch (cl) {
            case SCOUT:
                return builder.setLore(ChatColor.GRAY + "You move " + 20 * (potencyVal + 1) + "% faster!").getBukkitStack();
            case FROG:
                int percentIncrease = 0;

                switch (potencyVal + 1) {
                    case 2:
                        percentIncrease = 110;
                        break;
                    case 3:
                        percentIncrease = 180;
                        break;
                    case 4:
                        percentIncrease = 260;
                        break;
                    case 5:
                        percentIncrease = 350;
                        break;
                }
                return builder.setLore(ChatColor.GRAY + "You jump " + percentIncrease + "% higher!").getBukkitStack();
            case MINER:
                return builder.setLore(ChatColor.GRAY + "Your jackhammer has " + potencyVal + " uses!").getBukkitStack();
            case ENGINEER:
                return builder.setLore(ChatColor.GRAY + "Your traps affect blocks and players " + potencyVal + " blocks away!").getBukkitStack();
            case PERKSMITH:
                return builder.setLore(ChatColor.GRAY + "Deploy a refill every " + potencyVal/GameConstants.TICKS_PER_SEC + " seconds!").getBukkitStack();
            case BRAWLER:
                return builder.setLore(ChatColor.GRAY + "Your sword is enchanted with Knockback " + potencyVal + "!").getBukkitStack();
            case THIEF:
                return builder.setLore(ChatColor.GRAY + "Steal " + potencyVal + " random items/perks", ChatColor.GRAY + " when you hit an opponent!").getBukkitStack();
            default:
                return in;
        }
    }

    public static ItemStack generalSetTexts(ItemStack in, GeneralUpgrade upgrade, int level) {
        int upgradeValue = UpgradeConstants.computeGeneralUpgrade(upgrade, level);

        switch (upgrade) {
            case BLOCKS:
                return new NBTItemStack(in)
                        .setName(ChatColor.GREEN + "Blocks " + RomanNumeralFormat.format(level))
                        .setLore(ChatColor.GRAY + "Spawn with " + upgradeValue + " blocks!")
                        .getBukkitStack();
            case DOUBLE_JUMPS:
                return new NBTItemStack(in)
                        .setName(ChatColor.GREEN + "Double Jumps " + RomanNumeralFormat.format(level))
                        .setLore(ChatColor.GRAY + "Spawn with " + upgradeValue + " double jump" + (upgradeValue > 1 ? "s!" : "!"))
                        .getBukkitStack();
            case LIVES:
                return new NBTItemStack(in)
                        .setName(ChatColor.GREEN + "Lives " + RomanNumeralFormat.format(level))
                        .setLore(ChatColor.GRAY + "Spawn with " + upgradeValue + " lives!")
                        .getBukkitStack();
            case ITEM_SPACE:
                return new NBTItemStack(in)
                        .setName(ChatColor.GREEN + "Items " + RomanNumeralFormat.format(level))
                        .setLore(ChatColor.GRAY + "Hold up to " + upgradeValue + " item" + (upgradeValue > 1 ? "s" : "") + " at a time!")
                        .getBukkitStack();
            default:
                return in;
        }
    }
}
