package dev.tmm.mirrorspleef.game.ui;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIItem;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenu;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenuRedirect;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.PlayerData;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class MainShopGUI extends ChestGUIMenu {
    public MainShopGUI(PlayerData data, Plugin plugin) {
        super("Shop", 5, plugin);

        ItemStack generalStack = new NBTItemStack(Material.DIAMOND)
                .setName(ChatColor.GREEN + "General Upgrades")
                .getBukkitStack();

        ChestGUIItem generalMenu = new ChestGUIMenuRedirect(
                generalStack,
                new GeneralShopGUI(data, this, plugin)
        );

        ChestGUIItem scoutMenu = new ChestGUIMenuRedirect(
                GameClass.SCOUT.getItemStack(),
                new ClassShopGUI("Scout Upgrades", data, GameClass.SCOUT, this, plugin)
        );

        ChestGUIItem frogMenu = new ChestGUIMenuRedirect(
                GameClass.FROG.getItemStack(),
                new ClassShopGUI("Frog Upgrades", data, GameClass.FROG, this, plugin)
        );

        ChestGUIItem minerMenu = new ChestGUIMenuRedirect(
                GameClass.MINER.getItemStack(),
                new ClassShopGUI("Miner Upgrades", data, GameClass.MINER, this, plugin)
        );

        ChestGUIItem engineerMenu = new ChestGUIMenuRedirect(
                GameClass.ENGINEER.getItemStack(),
                new ClassShopGUI("Engineer Upgrades", data, GameClass.ENGINEER, this, plugin)
        );

        ChestGUIItem perksmithMenu = new ChestGUIMenuRedirect(
                GameClass.PERKSMITH.getItemStack(),
                new ClassShopGUI("Perksmith Upgrades", data, GameClass.PERKSMITH, this, plugin)
        );

        ChestGUIItem brawlerMenu = new ChestGUIMenuRedirect(
                GameClass.BRAWLER.getItemStack(),
                new ClassShopGUI("Brawler Upgrades", data, GameClass.BRAWLER, this, plugin)
        );

        /*ChestGUIItem thiefMenu = new ChestGUIMenuRedirect(
                GameClass.THIEF.getItemStack(),
                new ClassShopGUI("Thief Upgrades", data, GameClass.THIEF, this, plugin)
        );*/

        this.setItem(generalMenu, 1, 1);
        this.setItem(scoutMenu, 3, 1);
        this.setItem(frogMenu, 5, 1);
        this.setItem(minerMenu, 7, 1);
        this.setItem(engineerMenu, 2, 3);
        this.setItem(perksmithMenu, 4, 3);
        this.setItem(brawlerMenu, 6, 3);
        //this.setItem(thiefMenu, 7, 3);
    }
}
