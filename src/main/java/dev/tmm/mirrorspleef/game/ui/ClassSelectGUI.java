package dev.tmm.mirrorspleef.game.ui;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenu;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIRunnable;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.PlayerData;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.plugin.Plugin;

public class ClassSelectGUI extends ChestGUIMenu {
    private PlayerData data;

    public ClassSelectGUI(PlayerData data, Plugin plugin) {
        super("Class Selection", 5, plugin);

        this.data = data;

        this.setup();
    }

    private void setup() {
        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.POTION, 1, (short) 8194)
                        .setName(ChatColor.GREEN + "Scout")
                        .addItemFlags(ItemFlag.HIDE_POTION_EFFECTS)
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.SCOUT);
            }
        }, 2, 1);

        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.IRON_PICKAXE)
                        .setName(ChatColor.GREEN + "Miner")
                        .addItemFlags(ItemFlag.HIDE_ATTRIBUTES)
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.MINER);
            }
        }, 2, 3);

        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.WATER_LILY)
                        .setName(ChatColor.GREEN + "Frog")
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.FROG);
            }
        }, 6, 1);

        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.TNT)
                        .setName(ChatColor.GREEN + "Engineer")
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.ENGINEER);
            }
        }, 6, 3);

        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.ANVIL)
                        .setName(ChatColor.GREEN + "Perksmith")
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.PERKSMITH);
            }
        }, 4, 4);

        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.EYE_OF_ENDER)
                        .setName(ChatColor.GREEN + "Brawler")
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.BRAWLER);
            }
        }, 4, 0);

        /*this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.GOLD_INGOT)
                        .setName(ChatColor.GREEN + "Thief")
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.THIEF);
            }
        }, 5, 4);*/

        this.setItem(new ChestGUIRunnable(
                new NBTItemStack(Material.CHEST)
                        .setName(ChatColor.GREEN + "Random")
                        .getBukkitStack()
        ) {
            @Override
            public void onClick(Player p) {
                ClassSelectGUI.this.setActiveClass(GameClass.RANDOM);
            }
        }, 4, 2);
    }

    private void setActiveClass(GameClass cl) {
        GamePlayer gp = this.data.getPlayer();
        gp.getBukkitPlayer().sendMessage(ChatColor.GREEN + "You selected the " + ChatColor.GOLD + ChatColor.BOLD + cl.toString() + ChatColor.GREEN + " class!");
        gp.setActiveClass(cl);
        this.close(this.data.getPlayer().getBukkitPlayer());
    }
}
