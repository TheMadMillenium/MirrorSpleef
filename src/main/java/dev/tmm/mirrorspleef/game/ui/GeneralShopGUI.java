package dev.tmm.mirrorspleef.game.ui;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIDummyItem;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenu;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenuRedirect;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIRunnable;
import dev.tmm.mirrorspleef.game.data.GeneralUpgrade;
import dev.tmm.mirrorspleef.game.data.PlayerData;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class GeneralShopGUI extends ChestGUIMenu {
    private PlayerData data;

    public GeneralShopGUI(PlayerData data, ChestGUIMenu parent, Plugin plugin) {
        super("General Upgrades", 6, plugin);

        this.data = data;

        ItemStack backArrow = new NBTItemStack(Material.ARROW).setName(ChatColor.GREEN + "Go Back").getBukkitStack();
        this.setItem(new ChestGUIMenuRedirect(backArrow, parent), 0, 0);

        ItemStack headerStack = new NBTItemStack(Material.DIAMOND).setName(ChatColor.GREEN + "General Upgrades").getBukkitStack();
        this.setItem(new ChestGUIDummyItem(headerStack), 4, 0);

        ItemStack blocksStack = new NBTItemStack(Material.STAINED_GLASS, 1, (short) 14).setName(ChatColor.GREEN + "Blocks").getBukkitStack();
        ItemStack jumpsStack = new NBTItemStack(Material.DIAMOND_BOOTS).setName(ChatColor.GREEN + "Double Jumps").getBukkitStack();
        ItemStack itemsStack = new NBTItemStack(Material.CHEST).setName(ChatColor.GREEN + "Max Items in Hotbar").getBukkitStack();
        ItemStack livesStack = new NBTItemStack(Material.POTION, 1, (short) 8197).setName(ChatColor.GREEN + "Lives").addItemFlags(ItemFlag.HIDE_POTION_EFFECTS).getBukkitStack();

        this.setItem(new ChestGUIRunnable(blocksStack) {
            @Override
            public void onClick(Player p) {
                GeneralShopGUI.this.updateUpgrades(GeneralUpgrade.BLOCKS, 0);
            }
        }, 0, 2);

        this.setItem(new ChestGUIRunnable(jumpsStack) {
            @Override
            public void onClick(Player p) {
                GeneralShopGUI.this.updateUpgrades(GeneralUpgrade.DOUBLE_JUMPS, 0);
            }
        }, 0, 3);

        this.setItem(new ChestGUIRunnable(itemsStack) {
            @Override
            public void onClick(Player p) {
                GeneralShopGUI.this.updateUpgrades(GeneralUpgrade.ITEM_SPACE, 0);
            }
        }, 0, 4);

        this.setItem(new ChestGUIRunnable(livesStack) {
            @Override
            public void onClick(Player p) {
                GeneralShopGUI.this.updateUpgrades(GeneralUpgrade.LIVES, 0);
            }
        }, 0, 5);

        this.updateUpgrades(GeneralUpgrade.BLOCKS, this.data.getGeneralUpgradeLevel(GeneralUpgrade.BLOCKS));
        this.updateUpgrades(GeneralUpgrade.DOUBLE_JUMPS, this.data.getGeneralUpgradeLevel(GeneralUpgrade.DOUBLE_JUMPS));
        this.updateUpgrades(GeneralUpgrade.ITEM_SPACE, this.data.getGeneralUpgradeLevel(GeneralUpgrade.ITEM_SPACE));
        this.updateUpgrades(GeneralUpgrade.LIVES, this.data.getGeneralUpgradeLevel(GeneralUpgrade.LIVES));
    }

    private void updateUpgrades(final GeneralUpgrade upgrade, int newLevel) {
        GeneralShopGUI.this.data.setGeneralUpgradeLevel(upgrade, newLevel);

        final int y;

        switch (upgrade) {
            case BLOCKS:
                y = 2;
                break;
            case DOUBLE_JUMPS:
                y = 3;
                break;
            case ITEM_SPACE:
                y = 4;
                break;
            case LIVES:
                y = 5;
                break;
            default:
                y = 0;
                break;
        }

        final int maxLevel = UpgradeConstants.getMaxGeneralUpgrade(upgrade);

        for (int i = 0; i < 8; i++) {
            ItemStack clayItem;

            if (i < maxLevel) {
                clayItem = UpgradeNameTransformer.generalSetTexts(
                        new NBTItemStack(Material.STAINED_CLAY, 1, (short) (i < newLevel ? 5 : 14)).getBukkitStack(),
                        upgrade, i + 1
                );

                final int level = i + 1;

                this.setItem(new ChestGUIRunnable(clayItem) {
                    @Override
                    public void onClick(Player p) {
                        GeneralShopGUI.this.updateUpgrades(upgrade, level);
                    }
                }, i + 1, y);
            } else {
                clayItem = new NBTItemStack(Material.STAINED_CLAY, 1, (short) 15)
                        .setName(ChatColor.GRAY + "Unavailable")
                        .getBukkitStack();

                this.setItem(new ChestGUIRunnable(clayItem) {
                    @Override
                    public void onClick(Player p) {
                        p.sendMessage(ChatColor.RED + "That can't be upgraded any further!");
                    }
                }, i + 1, y);
            }
        }
    }
}
