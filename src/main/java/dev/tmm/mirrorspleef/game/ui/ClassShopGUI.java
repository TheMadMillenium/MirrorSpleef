package dev.tmm.mirrorspleef.game.ui;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIDummyItem;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenu;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIMenuRedirect;
import dev.tmm.mirrorspleef.base.ui.chest.ChestGUIRunnable;
import dev.tmm.mirrorspleef.game.data.GameClass;
import dev.tmm.mirrorspleef.game.data.PlayerData;
import dev.tmm.mirrorspleef.game.data.UpgradeConstants;
import dev.tmm.mirrorspleef.util.nbt.NBTItemStack;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class ClassShopGUI extends ChestGUIMenu {
    private static final ItemStack BACK_ARROW;

    private PlayerData data;
    private GameClass currentClass;

    public ClassShopGUI(String title, PlayerData data, GameClass cl, ChestGUIMenu parent, Plugin plugin) {
        super(title, 5, plugin);

        this.currentClass = cl;

        this.data = data;

        this.setItem(new ChestGUIMenuRedirect(BACK_ARROW, parent), 0, 0);
        this.setItem(new ChestGUIDummyItem(cl.getItemStack()), 4, 0);

        this.setItem(new ChestGUIRunnable(this.getQuantityIcon(cl)) {
            @Override
            public void onClick(Player p) {
                ClassShopGUI.this.updateQuantity(0);
            }
        }, 0, 2);

        this.setItem(new ChestGUIRunnable(this.getPotencyIcon(cl)) {
            @Override
            public void onClick(Player p) {
                ClassShopGUI.this.updatePotency(0);
            }
        }, 2, 4);

        this.updateQuantity(this.data.getQuantityLevel(cl));
        this.updatePotency(this.data.getPotencyLevel(cl));
    }

    private void updateQuantity(int quantity) {
        ClassShopGUI.this.data.setQuantityLevel(ClassShopGUI.this.currentClass, quantity);

        for (int i = 0; i < UpgradeConstants.MAX_QUANTITY_UPGRADES; i++) {
            final int level = i + 1;

            ItemStack clayColor = UpgradeNameTransformer.classQuantitySetTexts(
                    new NBTItemStack(Material.STAINED_CLAY, 1, (short) (i <  quantity ? 5 : 14)).getBukkitStack(),
                    ClassShopGUI.this.currentClass, level
            );

            this.setItem(new ChestGUIRunnable(clayColor) {
                @Override
                public void onClick(Player p) {
                    ClassShopGUI.this.updateQuantity(level);
                }
            }, i + 1, 2);
        }
    }

    private void updatePotency(int potency) {
        ClassShopGUI.this.data.setPotencyLevel(ClassShopGUI.this.currentClass, potency);

        for (int i = 0; i < UpgradeConstants.MAX_POTENCY_UPGRADES; i++) {
            final int level = i + 1;

            ItemStack clayColor = UpgradeNameTransformer.classPotencySetTexts(
                    new NBTItemStack(Material.STAINED_CLAY, 1, (short) (i < potency ? 5 : 14)).getBukkitStack(),
                    ClassShopGUI.this.currentClass, level
            );

            this.setItem(new ChestGUIRunnable(clayColor) {
                @Override
                public void onClick(Player p) {
                    ClassShopGUI.this.updatePotency(level);
                }
            }, i + 3, 4);
        }
    }

    private ItemStack getQuantityIcon(GameClass cl) {
        NBTItemStack quantityStack = new NBTItemStack(Material.WATCH);

        switch (cl) {
            case SCOUT:
                quantityStack.setName(ChatColor.GREEN + "Speed Boost Duration").setLore(ChatColor.GRAY + "Move faster for longer!");
                break;
            case FROG:
                quantityStack.setName(ChatColor.GREEN + "Jump Boost Duration").setLore(ChatColor.GRAY + "Jump higher for longer!");
                break;
            case MINER:
                quantityStack.setName(ChatColor.GREEN + "Instamine Duration").setLore(ChatColor.GRAY + "Outplay the competition with your extended instamine!");
                break;
            case ENGINEER:
                quantityStack.setName(ChatColor.GREEN + "Traps").setLore(ChatColor.GRAY + "Place traps on blocks to make them explosive!");
                break;
            case PERKSMITH:
                quantityStack.setName(ChatColor.GREEN + "Perk Packages").setLore(ChatColor.GRAY + "Deploy perk packages for your teammates to use!");
                break;
            case BRAWLER:
                quantityStack.setName(ChatColor.GREEN + "Endurance").setLore(ChatColor.GRAY + "Teleport to the opponent side and knock 'em around!");
                break;
            case THIEF:
                quantityStack.setName(ChatColor.GREEN + "Perk Tappers").setLore(ChatColor.GRAY + "Balance the playing field by stealing from the powerful!");
                break;
        }

        return quantityStack.getBukkitStack();
    }

    private ItemStack getPotencyIcon(GameClass cl) {
        NBTItemStack potencyStack = new NBTItemStack(Material.BLAZE_POWDER);

        switch (cl) {
            case SCOUT:
                potencyStack.setName(ChatColor.GREEN + "Speed Boost Level").setLore(ChatColor.GRAY + "Run even faster!");
                break;
            case FROG:
                potencyStack.setName(ChatColor.GREEN + "Jump Height").setLore(ChatColor.GRAY + "Jump even higher!");
                break;
            case MINER:
                potencyStack.setName(ChatColor.GREEN + "Jackhammers").setLore(ChatColor.GRAY + "Break a 3x3 area from under the opponent's", ChatColor.GRAY + " feet when they least expect it!");
                break;
            case ENGINEER:
                potencyStack.setName(ChatColor.GREEN + "Trap Range").setLore(ChatColor.GRAY + "Increase the range of your traps!");
                break;
            case PERKSMITH:
                potencyStack.setName(ChatColor.GREEN + "Perk Package Cooldown").setLore(ChatColor.GRAY + "Deploy perk packages faster!");
                break;
            case BRAWLER:
                potencyStack.setName(ChatColor.GREEN + "Strength").setLore(ChatColor.GRAY + "Knock them out of the park with your knockback sword!");
                break;
            case THIEF:
                potencyStack.setName(ChatColor.GREEN + "Greed").setLore(ChatColor.GRAY + "Steal more from your opponents!");
                break;
        }

        return potencyStack.getBukkitStack();
    }

    static {
        BACK_ARROW = new NBTItemStack(Material.ARROW).setName(ChatColor.GREEN + "Go Back").getBukkitStack();
    }
}
