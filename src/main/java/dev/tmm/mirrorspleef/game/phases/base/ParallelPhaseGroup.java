package dev.tmm.mirrorspleef.game.phases.base;

import io.netty.util.internal.ConcurrentSet;
import org.bukkit.event.HandlerList;

import java.util.Iterator;
import java.util.Set;

public class ParallelPhaseGroup implements PhaseGroup {
    private final Set<GamePhase> phases;

    public ParallelPhaseGroup() {
        this.phases = new ConcurrentSet<>();
    }

    @Override
    public void tick() {
        if (this.isFinished()) return;

        // Using an iterator instead of a for-each to allow safe removal.
        Iterator<GamePhase> phaseIterator = this.phases.iterator();

        while(phaseIterator.hasNext()) {
            GamePhase phase = phaseIterator.next();

            if (phase.isFinished()) {
                if(!phase.shouldSkip()) phase.onFinish();
                HandlerList.unregisterAll(phase);
                phaseIterator.remove();
                continue;
            }

            phase.tick();
            phase.incrementTick();
        }
    }

    @Override
    public void addPhase(GamePhase phase) {
        this.phases.add(phase);
        phase.setGroup(this);
        phase.onStart();
    }

    @Override
    public void clear() {
        this.phases.clear();
    }

    @Override
    public boolean isFinished() {
        return this.phases.isEmpty();
    }
}
