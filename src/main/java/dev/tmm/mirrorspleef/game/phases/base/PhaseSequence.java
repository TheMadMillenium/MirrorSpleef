package dev.tmm.mirrorspleef.game.phases.base;

import java.util.ArrayDeque;
import java.util.Queue;

public class PhaseSequence implements PhaseGroup {
    private final Queue<GamePhase> phases;
    private GamePhase currentPhase;

    public PhaseSequence() {
        this.phases = new ArrayDeque<>();
    }

    @Override
    public void tick() {
        if (this.isFinished()) return;

        boolean leaveLoop = false;

        while (!leaveLoop) {
            leaveLoop = true;

            if (this.currentPhase == null) {
                this.currentPhase = this.phases.poll();
            } else if (this.currentPhase.isFinished()) {
                if (this.currentPhase.shouldSkip()) {
                    leaveLoop = false;
                } else if (this.currentPhase != null) {
                    this.currentPhase.onFinish();
                }

                this.currentPhase = this.phases.poll();
                if (this.currentPhase != null) {
                    this.currentPhase.onStart();
                }
            }

            if (this.currentPhase != null) {
                this.currentPhase.tick();
                this.currentPhase.incrementTick();
            }
        }
    }

    @Override
    public void addPhase(GamePhase phase) {
        this.phases.add(phase);
        phase.setGroup(this);
    }

    @Override
    public void clear() {
        this.phases.clear();
        this.currentPhase = null;
    }

    @Override
    public boolean isFinished() {
        return this.phases.isEmpty() && this.currentPhase == null;
    }
}
