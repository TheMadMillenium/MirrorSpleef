package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;

public class InstaminePhase extends GamePhase {
    private GamePlayer gp;

    public InstaminePhase(GameState state, int duration, GamePlayer gp) {
        super(state, duration);

        this.gp = gp;
    }

    @Override
    public void onStart() {
        this.gp.activatePerk();
    }

    @Override
    public void onFinish() {
        this.gp.deactivatePerk();
    }
}
