package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.base.world.BlockSelection;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import org.bukkit.Location;
import org.bukkit.Material;

public class SpawnProtectionPhase extends GamePhase {
    private Location loc;

    public SpawnProtectionPhase(GameState state, Location loc) {
        super(state, GameConstants.SPAWN_PROT_LENGTH);

        this.loc = loc.clone();
    }

    @Override
    public void onStart() {
        BlockSelection selection = new BlockSelection(
                this.loc.clone().add(-1, 0, -1),
                this.loc.clone().add(1, 0, 1));

        selection.exclude(Material.NETHER_BRICK);
        selection.exclude(Material.COBBLE_WALL);

        this.state.world.setBlocks(selection, Material.OBSIDIAN, (byte) 0);
    }

    @Override
    public void onFinish() {
        BlockSelection selection = new BlockSelection(
                this.loc.clone().add(-1, 0, -1),
                this.loc.clone().add(1, 0, 1));

        selection.exclude(Material.NETHER_BRICK);
        selection.exclude(Material.COBBLE_WALL);

        this.state.world.setBlocks(selection, Material.STAINED_GLASS, (byte) 3);
    }
}
