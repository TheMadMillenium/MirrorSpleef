package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;

public class RespawnPhase extends GamePhase {
    private static final int RESPAWN_LENGTH = 5 * GameConstants.TICKS_PER_SEC;

    private final GamePlayer gp;

    public RespawnPhase(GameState state, GamePlayer gp) {
        super(state, RESPAWN_LENGTH);

        this.gp = gp;
    }

    @Override
    public void onFinish() {
        if (this.state.hasMoreLives(this.gp)) {
            this.state.spawnPlayer(this.gp, true);
        }
    }
}
