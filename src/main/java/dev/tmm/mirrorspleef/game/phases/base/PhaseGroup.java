package dev.tmm.mirrorspleef.game.phases.base;

public interface PhaseGroup {
    void tick();
    void addPhase(GamePhase phase);
    void clear();
    boolean isFinished();
}
