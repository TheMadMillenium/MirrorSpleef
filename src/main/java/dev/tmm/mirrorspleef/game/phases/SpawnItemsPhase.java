package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameStage;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import dev.tmm.mirrorspleef.util.RandomUtils;

public class SpawnItemsPhase extends GamePhase {
    public SpawnItemsPhase(GameState state) {
        super(state, RandomUtils.getInt(GameConstants.MIN_ITEM_SPAWN_TIME, GameConstants.MAX_ITEM_SPAWN_TIME));
    }

    @Override
    public void tick() {
        if (this.state.getStage() == GameStage.ENDGAME) this.skip();
    }

    @Override
    public void onFinish() {
        this.state.spawnRandomItem(GameTeam.TEAM_SRC);
        this.state.spawnRandomItem(GameTeam.TEAM_LINK);
        this.group.addPhase(new SpawnItemsPhase(this.state));
    }
}