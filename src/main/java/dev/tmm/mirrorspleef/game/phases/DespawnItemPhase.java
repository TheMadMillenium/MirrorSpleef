package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.collectibles.Collectible;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GameStage;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;

public class DespawnItemPhase extends GamePhase {
    private Collectible c;

    public DespawnItemPhase(Collectible c, GameState state) {
        super(state, GameConstants.ITEM_LIFESPAN);

        this.c = c;
    }

    @Override
    public void tick() {
        if (this.state.getStage() == GameStage.ENDGAME) {
            this.endPhase();
            return;
        }

        if (!this.state.itemExists(c)) {
            this.endPhase();
            return;
        }

        c.tick();
    }

    @Override
    public void onFinish() {
        this.state.removeItem(this.c);
    }
}
