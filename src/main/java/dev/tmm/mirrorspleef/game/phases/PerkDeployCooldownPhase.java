package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import org.apache.commons.lang.mutable.MutableBoolean;

public class PerkDeployCooldownPhase extends GamePhase {
    private MutableBoolean canDeploy;

    public PerkDeployCooldownPhase(GameState state, int durationInTicks, MutableBoolean canDeploy) {
        super(state, durationInTicks);

        this.canDeploy = canDeploy;
    }

    @Override
    public void onStart() {
        this.canDeploy.setValue(false);
    }

    @Override
    public void onFinish() {
        this.canDeploy.setValue(true);
    }
}
