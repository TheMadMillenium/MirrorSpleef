package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.data.GameTeam;
import dev.tmm.mirrorspleef.game.inventory.GameInventory;
import dev.tmm.mirrorspleef.game.inventory.classes.TeleportedBrawlerInventory;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class BrawlerPhase extends GamePhase {
    private Location spawnLoc;
    private GamePlayer gp;
    private Plugin plugin;
    private int knockback;

    private GameInventory storedInventory;

    public BrawlerPhase(GameState state, int durationInTicks, GamePlayer gp, int knockback, Plugin plugin) {
        super(state, durationInTicks);

        switch (gp.getTeam()) {
            case TEAM_SRC:
                this.spawnLoc = this.state.world.getRandomLinkBlock().getLocation();
                break;
            case TEAM_LINK:
                spawnLoc = this.state.world.getRandomSrcBlock().getLocation();
                break;
            default:
                return;
        }

        this.state.addEventPhase(new SpawnProtectionPhase(this.state, spawnLoc));

        this.spawnLoc.add(0.5, 1.5, 0.5);

        this.gp = gp;
        this.plugin = plugin;
        this.knockback = knockback;
    }

    @Override
    public void onStart() {
        Player p = this.gp.getBukkitPlayer();

        p.teleport(this.spawnLoc);

        gp.removeAllPotions();
        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, this.getLength(), 0, false, false));
        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2 * GameConstants.TICKS_PER_SEC, 255, false, false));
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2 * GameConstants.TICKS_PER_SEC, 128, false, false));

        p.setAllowFlight(false);

        this.storedInventory = this.gp.fuseInventories(this.state);
        this.gp.setInventory(new TeleportedBrawlerInventory(this.state, this.gp, this.knockback, this.plugin));

        this.gp.activatePerk();

        this.state.showFlippedTeam(this.gp, this.gp.getTeam());
    }

    @Override
    public void tick() {
        if (!this.gp.isAlive()) this.skip();
    }

    @Override
    public void onFinish() {
        GameTeam team = this.gp.getTeam();

        Location returnLoc = null;

        switch (team) {
            case TEAM_SRC:
                returnLoc = this.state.world.getRandomSrcBlock().getLocation();
                break;
            case TEAM_LINK:
                returnLoc = this.state.world.getRandomLinkBlock().getLocation();
                break;
        }

        Player p = this.gp.getBukkitPlayer();

        p.teleport(returnLoc.add(0.5, 1.5, 0.5));

        if (this.state.hasDoubleJumps(gp)) {
            gp.getBukkitPlayer().setAllowFlight(true);
            gp.setCanDoubleJump(true);
        }

        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 2 * GameConstants.TICKS_PER_SEC, 255, false, false));
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 2 * GameConstants.TICKS_PER_SEC, 128, false, false));

        this.gp.setInventory(this.storedInventory);
        this.gp.deactivatePerk();

        this.state.showFlippedTeam(this.gp, this.gp.getTeam().next());
    }
}
