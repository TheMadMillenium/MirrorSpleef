package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.base.ui.ChatColor;
import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;

public class InstamineSpawnPhase extends GamePhase {
    public InstamineSpawnPhase(GameState state, int durationInTicks) {
        super(state, durationInTicks);
    }

    @Override
    public void onFinish() {
        int numBlocks = this.state.world.spawnInstamines(GameConstants.INSTAMINE_PROBABILITY);

        this.state.ui.setTitleTimes(5, 30, 5);

        for (GamePlayer gp : this.state.getPlayers()) {
            this.state.ui.displayTitle(
                    null, ChatColor.YELLOW + "An insta-mine perk was hidden in " + numBlocks + " blocks!",
                    gp.getBukkitPlayer()
            );
        }

        this.state.addEventPhase(new InstamineSpawnPhase(
                this.state, this.getLength() + GameConstants.INSTAMINE_SPAWN_TIME_INCREASE
        ));
    }
}
