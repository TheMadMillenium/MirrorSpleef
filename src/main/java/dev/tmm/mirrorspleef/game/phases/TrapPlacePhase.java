package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class TrapPlacePhase extends GamePhase {
    private Block b;

    private Material type;
    private byte data;

    public TrapPlacePhase(Block b, GameState state) {
        super(state, GameConstants.OPPONENT_TRAP_VISIBLE_TIME);

        this.b = b;
        this.type = this.b.getType();
        this.data = this.b.getData();
    }

    @Override
    public void onStart() {
        b.setType(Material.STAINED_CLAY);
        b.setData((byte) 14);
    }

    @Override
    public void onFinish() {
        b.setType(this.type);
        b.setData(this.data);
    }
}
