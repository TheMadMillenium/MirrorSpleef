package dev.tmm.mirrorspleef.game.phases.base;

import dev.tmm.mirrorspleef.game.GameState;
import org.bukkit.event.Listener;

public class GamePhase implements Listener {
    private final int durationInTicks;
    private int currentTick;
    private boolean skip;

    protected final GameState state;
    protected PhaseGroup group;

    public GamePhase(GameState state, int durationInTicks) {
        this.state = state;

        this.durationInTicks = durationInTicks;
        this.currentTick = 0;
    }

    public final int getLength() {
        return this.durationInTicks;
    }

    public final boolean isFinished() {
        return this.currentTick == this.durationInTicks || this.shouldSkip();
    }

    public final int getTick() {
        return this.currentTick;
    }

    public final int ticksLeft() {
        return this.durationInTicks - this.currentTick;
    }

    public final void resetPhase() {
        this.currentTick = 0;
    }

    public final void endPhase() {
        this.currentTick = this.durationInTicks;
    }

    public final void incrementTick() {
        this.currentTick++;
    }

    public final void skip() {
        this.skip = true;
    }

    final void setGroup(PhaseGroup group) {
        this.group = group;
    }

    final boolean shouldSkip() {
        return this.skip;
    }

    public void onStart() {}
    public void tick() {}
    public void onFinish() {}
}
