package dev.tmm.mirrorspleef.game.phases;

import dev.tmm.mirrorspleef.game.GameState;
import dev.tmm.mirrorspleef.game.data.GameConstants;
import dev.tmm.mirrorspleef.game.data.GamePlayer;
import dev.tmm.mirrorspleef.game.phases.base.GamePhase;

public class DoubleJumpCooldownPhase extends GamePhase {
    private final GamePlayer gp;

    public DoubleJumpCooldownPhase(GamePlayer gp, GameState state) {
        super(state, GameConstants.DOUBLE_JUMP_COOLDOWN);

        this.gp = gp;
    }

    @Override
    public void onStart() {
        this.gp.getBukkitPlayer().setAllowFlight(false);
    }

    @Override
    public void onFinish() {
        this.state.setCanDoubleJump(this.gp, true);
        if (gp.getCurrentGameData().hasDoubleJumps()) {
            this.gp.getBukkitPlayer().setAllowFlight(true);
        }
    }
}
