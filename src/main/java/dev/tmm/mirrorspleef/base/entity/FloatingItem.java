package dev.tmm.mirrorspleef.base.entity;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class FloatingItem extends PacketEntity {
    private EntityItem itemEntity;
    private DataWatcher itemMeta;

    public FloatingItem(Location loc, ItemStack item) {
        super(new EntityArmorStand(((CraftWorld) loc.getWorld()).getHandle()), loc);

        this.itemEntity = new EntityItem(((CraftWorld) loc.getWorld()).getHandle());
        this.itemMeta = this.itemEntity.getDataWatcher();

        if (item == null) throw new IllegalArgumentException("The \"item\" parameter cannot be null");

        this.itemEntity.setItemStack(CraftItemStack.asNMSCopy(item));

        this.itemEntity.mount(this.entity);

        this.entity.setInvisible(true);
        ((EntityArmorStand) this.entity).setBasePlate(false);

        this.passengerOffset.setY(this.entity.an());

        this.move(loc);
    }

    @Override
    protected void move(Location loc, boolean adjust) {
        Location adjustedLoc = loc.clone();

        if (adjust) adjustedLoc.subtract(0, this.entity.an(), 0);

        super.move(adjustedLoc, adjust);
    }

    @Override
    protected void sendMove() {
        PacketPlayOutEntityTeleport teleportStand = new PacketPlayOutEntityTeleport(this.entity);
        this.sendPacketsAll(teleportStand);
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void sendSpawn(Player p) {
        Packet spawnStand = new PacketPlayOutSpawnEntityLiving((EntityLiving) this.entity);
        Packet standMeta = new PacketPlayOutEntityMetadata(this.entity.getId(), this.meta, true);
        Packet spawnItem = new PacketPlayOutSpawnEntity(this.itemEntity, 2);
        Packet itemMeta = new PacketPlayOutEntityMetadata(this.itemEntity.getId(), this.itemMeta, true);
        Packet passengerA = new PacketPlayOutAttachEntity(0, this.itemEntity, this.entity);

        this.sendPacketsAll(spawnStand, standMeta, spawnItem, itemMeta, passengerA);
    }

    @Override
    protected void sendDespawn(Player p) {
        Packet despawn = new PacketPlayOutEntityDestroy(this.entity.getId(), this.itemEntity.getId());

        this.sendPacketsAll(despawn);
    }
}