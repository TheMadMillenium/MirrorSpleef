package dev.tmm.mirrorspleef.base.entity;

import net.minecraft.server.v1_8_R3.DataWatcher;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class PacketEntity implements Listener {
    protected Entity entity;
    public Location location;
    protected Location offset;
    protected Location passengerOffset;

    protected DataWatcher meta;

    private double renderDistanceSquared;

    private Set<Player> canSee;
    private Set<Player> withinRange;

    private Map<PacketEntity, Boolean> attachedEntities;

    public PacketEntity(Entity entity, Location loc) {
        this(entity, loc, 60);
    }

    public PacketEntity(Entity entity, Location loc, double renderDistance) {
        this.entity = entity;
        this.location = loc.clone();
        this.offset = new Location(loc.getWorld(), 0, 0, 0);
        this.passengerOffset = new Location(loc.getWorld(), 0, 0, 0);
        this.meta = entity.getDataWatcher();

        this.renderDistanceSquared = renderDistance * renderDistance;

        this.canSee = new HashSet<>();
        this.withinRange = new HashSet<>();

        this.attachedEntities = new HashMap<>();
    }

    public void attachEntity(PacketEntity e, boolean adjust) {
        if (e == this) throw new IllegalArgumentException("Cannot attach entity to self");

        this.attachedEntities.put(e, adjust);

        e.move(this.location.clone().add(this.passengerOffset), adjust);

        for (Player p : this.withinRange) {
            e.show(p);
        }
    }

    public final void detachEntity(PacketEntity e) {
        this.attachedEntities.remove(e);
    }

    public final void show(Player p) {
        this.canSee.add(p);
        this.updateVisibility(p);

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.show(p);
        }
    }

    public final void hide(Player p) {
        this.canSee.remove(p);
        this.updateVisibility(p);

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.hide(p);
        }
    }

    public final void hideFromAll() {
        this.canSee.clear();

        for (Player p : new HashSet<>(this.withinRange)) {
            this.updateVisibility(p);
        }

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.hideFromAll();
        }
    }

    public final void move(Location loc) {
        this.move(loc, true);
    }

    public final void setOffset(double x, double y, double z) {
        this.setOffset(x, y, z, 0, 0);
    }

    public final void setOffset(double x, double y, double z, float yaw, float pitch) {
        this.offset.setX(x);
        this.offset.setY(y);
        this.offset.setZ(z);
        this.offset.setYaw(yaw);
        this.offset.setPitch(pitch);

        this.move(this.location);
    }

    public final void setRenderDistance(double renderDistance) {
        this.renderDistanceSquared = renderDistance * renderDistance;
    }

    public boolean isTouching(Entity other) {
        return this.entity.getBoundingBox().b(other.getBoundingBox());
    }

    public boolean isTouching(org.bukkit.entity.Entity other) {
        return this.entity.getBoundingBox().b(((CraftEntity) other).getHandle().getBoundingBox());
    }

    protected void move(Location loc, boolean adjust) {
        this.location = loc.clone();

        this.entity.setLocation(
                this.location.getX() + this.offset.getX(),
                this.location.getY() + this.offset.getY(),
                this.location.getZ() + this.offset.getZ(),
                this.location.getYaw() + this.offset.getYaw(),
                this.location.getPitch() + this.offset.getPitch()
        );

        this.sendMove();

        for (Player p : this.withinRange) {
            this.updateVisibility(p);
        }

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.move(loc.clone().add(this.passengerOffset), this.attachedEntities.get(pe));
            pe.sendMove();
        }
    }

    protected final void sendPackets(Player p, Packet... packets) {
        PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;

        for (Packet packet : packets) {
            connection.sendPacket(packet);
        }
    }

    protected final void sendPacketsAll(Packet... packets) {
        for (Player p : this.withinRange) {
            PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;

            for (Packet packet : packets) {
                connection.sendPacket(packet);
            }
        }
    }

    protected abstract void sendSpawn(Player p);

    protected abstract void sendDespawn(Player p);

    protected abstract void sendMove();

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        this.updateVisibility(e.getPlayer());

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.updateVisibility(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        this.updateVisibility(e.getPlayer());

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.updateVisibility(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        this.hide(e.getPlayer());

        for (PacketEntity pe : this.attachedEntities.keySet()) {
            pe.hide(e.getPlayer());
        }
    }

    private void updateVisibility(Player p) {
        if (this.canSee.contains(p)) {
            if (p.getLocation().getWorld() == this.location.getWorld()) {
                if (p.getLocation().distanceSquared(this.location) < this.renderDistanceSquared) {
                    if (!this.withinRange.contains(p)) {
                        this.withinRange.add(p);
                        this.sendSpawn(p);
                    }
                }
            }
        } else {
            if (this.withinRange.contains(p)) {
                this.sendDespawn(p);
                this.withinRange.remove(p);
            }
        }
    }
}
