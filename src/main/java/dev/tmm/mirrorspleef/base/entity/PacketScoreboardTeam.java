package dev.tmm.mirrorspleef.base.entity;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;

import java.util.*;

public class PacketScoreboardTeam {
    private static final Scoreboard dummy = new Scoreboard();

    private ScoreboardTeam team;
    private Set<String> names;

    public PacketScoreboardTeam(String name) {
        this.team = new ScoreboardTeam(dummy, name);

        this.names = new HashSet<>();
    }

    public void setPrefix(String prefix) {
        this.team.setPrefix(prefix);
    }

    public void setSuffix(String suffix) {
        this.team.setSuffix(suffix);
    }

    public void setFriendlyFire(boolean allow) {
        this.team.setAllowFriendlyFire(allow);
    }

    public void setFriendlyInvisibles(boolean allow) {
        this.team.setCanSeeFriendlyInvisibles(allow);
    }

    public void setNameVisibility(NameTagVisibility visibility) {
        this.team.setNameTagVisibility(
                ScoreboardTeamBase.EnumNameTagVisibility.valueOf(visibility.name())
        );
    }

    public boolean contains(String name) {
        return this.names.contains(name);
    }

    public void sendTeam(Player receiver) {
        this.removeTeam(receiver);

        PacketPlayOutScoreboardTeam newTeam = new PacketPlayOutScoreboardTeam(this.team, 0);
        this.sendPackets(receiver, newTeam);

        if(!this.names.isEmpty()) {
            PacketPlayOutScoreboardTeam joinPlayers = new PacketPlayOutScoreboardTeam(this.team, this.names, 3);
            this.sendPackets(receiver, joinPlayers);
        }
    }

    public void removeTeam(Player receiver) {
        if(!this.names.isEmpty()) {
            PacketPlayOutScoreboardTeam leavePlayers = new PacketPlayOutScoreboardTeam(this.team, this.names, 4);
            this.sendPackets(receiver, leavePlayers);
        }

        PacketPlayOutScoreboardTeam destroyTeam = new PacketPlayOutScoreboardTeam(this.team, 2);
        this.sendPackets(receiver, destroyTeam);
    }

    public void addNames(String... names) {
        this.names.addAll(Arrays.asList(names));
    }

    public void removeNames(String... names) {
        this.names.removeAll(Arrays.asList(names));
    }

    private void sendPackets(Player p, Packet... packets) {
        PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;

        for (Packet packet : packets) {
            connection.sendPacket(packet);
        }
    }
}
