package dev.tmm.mirrorspleef.base.entity;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import dev.tmm.mirrorspleef.util.MathUtils;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

/**
 * A wrapper for the EntityPlayer class used to display a specified player upside down.
 */

public class FlippedPlayerNPC extends PacketEntity {
    private double bodyRotation;
    private boolean facingRight;

    private MovableNameTag tag;

    /**
     * Creates an upside down NPC.<br>
     * I am unsure whether creating an NPC with a custom name while retaining the flipped property is possible.<br>
     * Therefore, I'll simply use Grumm's name. For the purposes of this mini-game, nametags won't be a problem.
     *
     * @param loc        The NPC's spawn location.
     * @param playerUUID The unique ID of the player to be displayed upside down.
     */

    public FlippedPlayerNPC(Location loc, UUID playerUUID) {
        super(createNPC(loc.getWorld(), playerUUID), loc);

        this.meta.watch(10, (byte) 0b11111111); // Flags to display skin overlays (hat, sleeves, etc.)

        this.move(loc, true);

        this.tag = new MovableNameTag(loc, Bukkit.getPlayer(playerUUID).getName());
        tag.setOffset(0, -0.5, 0);
        this.attachEntity(tag, true);
    }

    /**
     * Displays the arm swing animation to all players within range.
     */

    public void swingArm() {
        double headRot = -this.location.getYaw();

        this.bodyRotation = MathUtils.wrapAngle(this.bodyRotation - headRot) * 0.3 + headRot;

        Packet swing = new PacketPlayOutAnimation(this.entity, 0);
        PacketPlayOutEntity.PacketPlayOutEntityLook look = new PacketPlayOutEntity.PacketPlayOutEntityLook(
                this.entity.getId(),
                (byte) (headRot * 0.711111111111),
                (byte) (this.location.getPitch() * 0.711111111111),
                false
        );
        PacketPlayOutEntityHeadRotation headLook = new PacketPlayOutEntityHeadRotation(
                this.entity,
                (byte) ((headRot + MathUtils.wrapAngle(this.bodyRotation - headRot) * 2) * 0.711111111)
        );

        this.sendPacketsAll(swing, look, headLook);
    }

    /**
     * Changes the item held to all players within range.
     *
     * @param item The ItemStack which this NPC should hold.
     */

    public void setBlockInHand(ItemStack item) {
        Packet changeItem = new PacketPlayOutEntityEquipment(this.entity.getId(), 0, CraftItemStack.asNMSCopy(item));

        this.sendPacketsAll(changeItem);
    }

    /**
     * Sets this NPC's crouching state.
     *
     * @param crouched Whether this NPC should crouch or not.
     */

    public void setCrouched(boolean crouched) {
        this.meta.watch(0, (byte) (crouched ? 0x2 : 0));
        Packet crouch = new PacketPlayOutEntityMetadata(this.entity.getId(), this.meta, false);

        this.sendPacketsAll(crouch);
    }

    public void removeFromTab() {
        this.sendPacketsAll(
                new PacketPlayOutPlayerInfo(
                        PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER,
                        (EntityPlayer) this.entity
                )
        );
    }

    public void setName(String formattedName) {
        this.tag.setName(formattedName);
    }

    @Override
    protected void sendSpawn(Player p) {
        Packet spawn = new PacketPlayOutNamedEntitySpawn((EntityHuman) this.entity);
        Packet data = new PacketPlayOutEntityMetadata(this.entity.getId(), this.meta, false);

        this.sendPackets(
                p,
                new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, (EntityPlayer) this.entity),
                spawn,
                data
        );
    }

    @Override
    protected void sendDespawn(Player p) {
        Packet playerInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, (EntityPlayer) this.entity);
        Packet despawn = new PacketPlayOutEntityDestroy(this.entity.getId());
        this.sendPackets(p, despawn, playerInfo);
    }

    @Override
    protected void move(Location loc, boolean adjust) {
        double lastX = this.location.getX();
        double lastZ = this.location.getZ();

        super.move(loc.clone().subtract(0, this.entity.length, 0), adjust);

        float headRot = -this.location.getYaw();

        double velX = this.entity.locX - lastX;
        double velZ = this.entity.locZ - lastZ;

        double dist = velX * velX + velZ * velZ;

        if (dist > 0.0025) {
            double targetBodyRotation = Math.abs((Math.toDegrees(Math.atan2(velZ, velX)) - headRot + 450) % 360 - 180);
            if (!this.facingRight) targetBodyRotation = -targetBodyRotation;

            if (Math.abs(targetBodyRotation) < 100) {
                double perpdot = -Math.sin(Math.toRadians(headRot)) * velZ - Math.cos(Math.toRadians(headRot)) * velX;

                if (Math.abs(perpdot) > 0.001) {
                    this.facingRight = perpdot > 0;
                }
            }

            targetBodyRotation *= 0.5;
            targetBodyRotation += headRot;

            this.bodyRotation = (MathUtils.clampAngle(targetBodyRotation, headRot, 64) - this.bodyRotation) * 0.4 + this.bodyRotation;
        } else {
            this.bodyRotation = MathUtils.clampAngle(this.bodyRotation, headRot, 42);
        }
    }

    @Override
    protected void sendMove() {
        double headRot = -this.location.getYaw();

        PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(this.entity);
        PacketPlayOutEntity.PacketPlayOutEntityLook look = new PacketPlayOutEntity.PacketPlayOutEntityLook(
                this.entity.getId(),
                (byte) (headRot * 0.711111111111),
                (byte) (this.location.getPitch() * 0.711111111111),
                false
        );
        PacketPlayOutEntityHeadRotation headLook = new PacketPlayOutEntityHeadRotation(
                this.entity,
                (byte) ((headRot + MathUtils.wrapAngle(this.bodyRotation - headRot) * 2) * 0.711111111111));

        this.sendPacketsAll(teleport, look, headLook);
    }

    private static UUID dinnerboneUUID = UUID.fromString("61699b2e-d327-4a01-9f1e-0ea8c3f06bc6");
    private static UUID grummUUID = UUID.fromString("e6b5c088-0680-44df-9e1b-9bf11792291b");

    private static EntityPlayer createNPC(World world, UUID playerUUID) {
        MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
        WorldServer worldServ = ((CraftWorld) world).getHandle();

        // Spawning Dinnerbone and Grumm in normal orientation, just for giggles.
        String name = dinnerboneUUID.equals(playerUUID) || grummUUID.equals(playerUUID) ? "NotGrumm" : "Grumm";

        GameProfile profile = new GameProfile(UUID.randomUUID(), name);
        attachSkinProperty(profile, playerUUID);

        return new EntityPlayer(server, worldServ, profile, new PlayerInteractManager(worldServ));
    }

    /**
     * Attaches the "textures" property to the <code>profile</code> object.
     * Original author(s) <a href="https://bukkit.org/threads/273251/">here</a>.
     *
     * @param profile The GameProfile object which to attach the property to.
     * @param uuid    The unique ID of the player to get skin data from.
     */
    private static void attachSkinProperty(GameProfile profile, UUID uuid) {
        try {
            URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.toString().replace("-", "") + "?unsigned=false");
            URLConnection uc = url.openConnection();

            uc.setUseCaches(false);
            uc.setDefaultUseCaches(false);

            uc.addRequestProperty("User-Agent", "Mozilla/5.0");
            uc.addRequestProperty("Cache-Control", "min-fresh=60, must-revalidate"); // Since the request is limited to 1 request/min/uuid, may as well cache the result.
            //uc.addRequestProperty("Pragma", "no-cache"); // We still want cache to be kept for 1 minute.

            // Not critical, but BufferedReader is likely faster, since it does not tokenize the input.

            //Scanner scanner = new Scanner(uc.getInputStream(), "UTF-8").useDelimiter("\\A");
            //String json = scanner.next();

            BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String json = reader.readLine();

            JSONArray properties = (JSONArray) ((JSONObject) new JSONParser().parse(json)).get("properties");

            for (Object property1 : properties) {
                JSONObject property = (JSONObject) property1;
                String name = (String) property.get("name");
                String value = (String) property.get("value");
                String signature = property.containsKey("signature") ? (String) property.get("signature") : null;
                if (signature != null) {
                    profile.getProperties().put(name, new Property(name, value, signature));
                } else {
                    profile.getProperties().put(name, new Property(name, value));
                }
            }
        } catch (Exception ignored) {
        }
    }
}
