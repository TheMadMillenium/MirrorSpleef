package dev.tmm.mirrorspleef.base.entity;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;

public class MovableNameTag extends PacketEntity {
    public MovableNameTag(Location loc, String formattedNameTag) {
        super(new EntityArmorStand(((CraftWorld) loc.getWorld()).getHandle()), loc);

        this.entity.setCustomName(formattedNameTag);
        this.entity.setCustomNameVisible(true);

        this.entity.setInvisible(true);
    }

    public void setName(String formattedName) {
        this.entity.setCustomName(formattedName);
        this.entity.setCustomNameVisible(true);

        this.sendPacketsAll(new PacketPlayOutEntityMetadata(this.entity.getId(), this.meta, true));
    }

    public void setName(String formatterNameTag, Player receiver) {
        //String oldName = this.entity.getCustomName();

        this.entity.setCustomName(formatterNameTag);
        this.entity.setCustomNameVisible(true);

        //this.sendPackets(receiver, new PacketPlayOutEntityMetadata(this.entity.getId(), this.meta, true));
    }

    @Override
    public void move(Location loc, boolean adjust) {
        Location adjustedLoc = loc.clone().subtract(0, this.entity.length + 0.5, 0);

        super.move(adjustedLoc, adjust);
    }

    @Override
    protected void sendSpawn(Player p) {
        Packet spawnStand = new PacketPlayOutSpawnEntityLiving((EntityLiving) this.entity);
        Packet standMeta = new PacketPlayOutEntityMetadata(this.entity.getId(), this.meta, true);

        this.sendPacketsAll(spawnStand, standMeta);
    }

    @Override
    protected void sendDespawn(Player p) {
        Packet despawn = new PacketPlayOutEntityDestroy(this.entity.getId());

        this.sendPacketsAll(despawn);
    }

    @Override
    protected void sendMove() {
        PacketPlayOutEntityTeleport teleportStand = new PacketPlayOutEntityTeleport(this.entity);
        this.sendPacketsAll(teleportStand);
    }
}
