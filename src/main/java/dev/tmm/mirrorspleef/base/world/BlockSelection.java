package dev.tmm.mirrorspleef.base.world;

import dev.tmm.mirrorspleef.util.RandomUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

public class BlockSelection extends CoordBoundary {
    private static final int BLOCK_LIMIT = 0x1000000; // 256 ^ 3

    public List<Integer> regions;
    private int selectionVolume;

    public BlockSelection(Location a, Location b) {
        super(a, b);

        this.construct();
    }

    public BlockSelection(World world, int aX, int aY, int aZ, int bX, int bY, int bZ) {
        super(world, aX, aY, aZ, bX, bY, bZ);

        this.construct();
    }

    private void construct() {
        selectionVolume = (this.bX - this.aX + 1) * (this.bY - this.aY + 1) * (this.bZ - this.aZ + 1);

        if (selectionVolume > BLOCK_LIMIT) {
            throw new IllegalArgumentException("Volume of regions cannot exceed " + BLOCK_LIMIT);
        }

        if (this.aY > 255 || this.bY > 255) {
            throw new IllegalArgumentException("The selection cannot exist above y=255");
        }

        if (this.aY < 0 || this.bY < 0) {
            throw new IllegalArgumentException("The selection cannot exist below y=0");
        }

        this.regions = new ArrayList<>();
        this.regions.add(selectionVolume);
        this.regions.add(selectionVolume);
    }

    public void include(Location loc) {
        this.updateBlock(loc, true);
    }

    public void include(Material type) {
        this.include(type, -1);
    }

    public void include(Material type, int data) {
        this.updateBlocks(type, data, false);
    }

    public void exclude(Location loc) {
        this.updateBlock(loc, false);
    }

    public void exclude(Material type) {
        this.exclude(type, -1);
    }

    public void exclude(Material type, int data) {
        this.updateBlocks(type, data, true);
    }

    public void setBlocks(Material type) {
        this.setBlocks(type, 0);
    }

    public void setBlocks(Material type, int data) {
        this.optimizeRegions();

        int width = this.bX - this.aX + 1;
        int length = this.bZ - this.aZ + 1;

        for (int e = 0; e < this.regions.size(); e += 2) {
            int regionStart = 0;
            int regionEnd = this.regions.get(e);

            if (e > 0) regionStart = this.regions.get(e - 1);

            for (int i = regionStart; i < regionEnd; i++) {
                int x = i % width + this.aX;
                int y = i / width / length + this.aY;
                int z = i / width % length + this.aZ;

                Block b = this.world.getBlockAt(x, y, z);

                b.setType(type);
                b.setData((byte) data);
            }
        }
    }

    public Block getRandomBlock() {
        this.optimizeRegions();

        int width = this.bX - this.aX + 1;
        int length = this.bZ - this.aZ + 1;

        boolean offset = false;

        int regionCount = (this.regions.size() + 1) / 2;
        if (this.regions.get(0) == 0) {
            regionCount--;
            offset = true;
        }

        if (regionCount == 0) return null;

        int region = RandomUtils.getInt(regionCount) * 2 + (offset ? 2 : 0);
        int diff = this.regions.get(region);
        int startIndex = 0;
        if (region > 0) startIndex = this.regions.get(region - 1);

        diff -= startIndex;

        int index = (diff == 0 ? 0 : RandomUtils.getInt(diff)) + startIndex;

        int x = index % width + this.aX;
        int y = index / width / length + this.aY;
        int z = index / width % length + this.aZ;

        return this.world.getBlockAt(x, y, z);
    }

    public int getNumBlocks() {
        int sum = this.regions.get(0);

        for (int i = 2; i < this.regions.size(); i += 2) {
            sum += this.regions.get(i) - this.regions.get(i - 1);
        }

        return sum;
    }

    public boolean inSelection(Location loc) {
        this.optimizeRegions();

        int x = loc.getBlockX() - this.aX;
        int y = loc.getBlockY() - this.aY;
        int z = loc.getBlockZ() - this.aZ;

        if (x < 0 || x > this.bX - this.aX) return false;
        if (y < 0 || y > this.bY - this.aY) return false;
        if (z < 0 || z > this.bZ - this.aZ) return false;

        int width = this.bX - this.aX + 1;
        int length = this.bZ - this.aZ + 1;

        int i = x + z * width + y * width * length;
        int e = this.regions.size() - 1;

        while (true) {
            if (e < 0) break;
            if (i >= this.regions.get(e)) break;

            e--;
        }

        e++;

        return e % 2 == 0;
    }

    private void updateBlock(Location loc, boolean include) {
        this.optimizeRegions();

        int x = loc.getBlockX() - this.aX;
        int y = loc.getBlockY() - this.aY;
        int z = loc.getBlockZ() - this.aZ;

        if (x < 0 || x > this.bX - this.aX) return;
        if (y < 0 || y > this.bY - this.aY) return;
        if (z < 0 || z > this.bZ - this.aZ) return;

        int width = this.bX - this.aX + 1;
        int length = this.bZ - this.aZ + 1;

        int i = x + z * width + y * width * length;
        int e = this.regions.size() - 1;

        while (true) {
            if (e < 0) break;
            if (i >= this.regions.get(e)) break;

            e--;
        }

        e++;

        if (e % 2 == 0 == include) return;

        this.regions.add(e, i + 1);
        this.regions.add(e, i);
    }

    private void updateBlocks(Material type, int data, boolean exclude) {
        this.optimizeRegions();

        int width = this.bX - this.aX + 1;
        int length = this.bZ - this.aZ + 1;

        for (int e = exclude ? 0 : 1; e < this.regions.size(); e += 2) {
            int regionStart = 0;
            int regionEnd = this.regions.get(e);

            if (e > 0) regionStart = this.regions.get(e - 1);

            boolean addToPreviousRegion = true;
            for (int i = regionStart; i < regionEnd; i++) {
                int x = i % width + this.aX;
                int y = i / width / length + this.aY;
                int z = i / width % length + this.aZ;

                Block b = this.world.getBlockAt(x, y, z);

                if (b.getType() == type && (data < 0 || b.getData() == data)) {
                    if (addToPreviousRegion && e > 0) this.regions.set(e - 1, this.regions.get(e - 1) + 1);
                    else {
                        this.regions.add(e, i + 1);
                        this.regions.add(e, i);
                        e += 2;
                    }

                    addToPreviousRegion = true;
                } else addToPreviousRegion = false;
            }
        }
    }

    private void optimizeRegions() {
        for (int i = 0; i < this.regions.size() - 2; i++) {
            if (this.regions.get(i).intValue() == this.regions.get(i + 1).intValue()) {
                this.regions.remove(i);
                this.regions.remove(i);
                i = -1;
            }
        }
    }
}
