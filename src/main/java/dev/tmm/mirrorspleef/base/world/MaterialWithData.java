package dev.tmm.mirrorspleef.base.world;

import org.bukkit.Material;

public class MaterialWithData {
    private final Material type;
    private final int data;

    public MaterialWithData(Material type, int data) {
        this.type = type;
        this.data = data;
    }

    @Override
    public final boolean equals(Object other) {
        if(other == this) return true;
        if(!(other.getClass() == MaterialWithData.class)) return false;

        MaterialWithData material = (MaterialWithData) other;

        if(this.type != material.type) return false;
        if(this.data != material.data) return false;

        return true;
    }
}
