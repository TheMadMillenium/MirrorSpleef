package dev.tmm.mirrorspleef.base.world;

public enum StructureRotation {
    NONE,
    X_AXIS,
    Z_AXIS
}
