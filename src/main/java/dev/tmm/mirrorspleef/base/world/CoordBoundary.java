package dev.tmm.mirrorspleef.base.world;

import org.bukkit.Location;
import org.bukkit.World;

public class CoordBoundary {
    protected final World world;
    protected final int aX, aY, aZ;
    protected final int bX, bY, bZ;

    public CoordBoundary(Location a, Location b) {
        if (a.getWorld() != b.getWorld())
            throw new IllegalArgumentException("Both locations must be in the same world");

        this.world = a.getWorld();
        this.aX = Math.min(a.getBlockX(), b.getBlockX());
        this.aY = Math.min(a.getBlockY(), b.getBlockY());
        this.aZ = Math.min(a.getBlockZ(), b.getBlockZ());
        this.bX = Math.max(a.getBlockX(), b.getBlockX());
        this.bY = Math.max(a.getBlockY(), b.getBlockY());
        this.bZ = Math.max(a.getBlockZ(), b.getBlockZ());
    }

    public CoordBoundary(World world, int aX, int aY, int aZ, int bX, int bY, int bZ) {
        this.world = world;
        this.aX = Math.min(aX, bX);
        this.aY = Math.min(aY, bY);
        this.aZ = Math.min(aZ, bZ);
        this.bX = Math.max(aX, bX);
        this.bY = Math.max(aY, bY);
        this.bZ = Math.max(aZ, bZ);
    }

    public final Location getA() {
        return new Location(this.world, this.aX, this.aY, this.aZ);
    }

    public final Location getB() {
        return new Location(this.world, this.bX, this.bY, this.bZ);
    }

    public final World getWorld() {
        return this.world;
    }

    public boolean intersects(CoordBoundary other) {
        if (other.world != this.world) return false;

        if (this.aX > other.bX || this.aY > other.bY || this.aZ > other.bZ) return false;
        if (this.bX < other.aX || this.bY < other.aY || this.bZ < other.aZ) return false;

        return true;
    }

    public boolean withinBounds(Location loc) {
        if (loc.getWorld() != this.world) return false;

        if (this.aX > loc.getX() || this.aY > loc.getY() || this.aZ > loc.getZ()) return false;
        if (this.bX < loc.getX() || this.bY < loc.getY() || this.bZ < loc.getZ()) return false;

        return true;
    }
}
