package dev.tmm.mirrorspleef.base.world;

import dev.tmm.mirrorspleef.util.RandomUtils;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Explosion {
    private Location loc;
    private double radius;
    private double blastPower;

    private List<MaterialWithData> blocks;

    public Explosion(Location loc, double radius, double blastPower) {
        this.loc = loc.clone();
        this.radius = radius;
        this.blastPower = blastPower;

        this.blocks = new ArrayList<>();
    }

    public void addAffectedBlock(Material type, int data) {
        this.blocks.add(new MaterialWithData(type, data));
    }

    @SuppressWarnings("deprecation")
    public List<Block> explode(World w) {
        ArrayList<Block> destroyedBlocks = new ArrayList<>();

        Effect effect;

        if (this.radius < 1) effect = Effect.EXPLOSION;
        else if (this.radius < 3) effect = Effect.EXPLOSION_LARGE;
        else effect = Effect.EXPLOSION_HUGE;

        w.playEffect(this.loc, effect, 0);

        int blockRadius = (int) this.radius;

        for (int x = -blockRadius; x <= blockRadius; x++) {
            for (int y = -blockRadius; y <= blockRadius; y++) {
                for (int z = -blockRadius; z <= blockRadius; z++) {
                    int dist = x * x + y * y + z * z;
                    if (dist == 0) continue;
                    if (dist > blockRadius * blockRadius) continue;

                    Location blockLoc = new Location(w, x + 0.5, y, z + 0.5).add(this.loc);
                    Block b = w.getBlockAt(blockLoc);

                    Material blockMaterial = b.getType();
                    if (!blockMaterial.isSolid()) continue;

                    byte blockData = b.getData();

                    if (!this.blocks.contains(new MaterialWithData(blockMaterial, blockData))) continue;

                    Vector vel = this.getBlastVector(blockLoc);
                    if (vel == null) continue;

                    b.setType(Material.AIR);

                    FallingBlock explodedBlock = w.spawnFallingBlock(blockLoc, blockMaterial, blockData);
                    explodedBlock.setVelocity(vel.add(new Vector(0, this.radius * 0.1, 0)));

                    destroyedBlocks.add(b);
                }
            }
        }

        return destroyedBlocks;
    }

    public Vector getBlastVector(Location affectedLoc) {
        if (this.loc.distanceSquared(affectedLoc) > this.radius * this.radius) return null;
        if (2 / this.loc.distanceSquared(affectedLoc) < RandomUtils.getDouble(1)) return null;

        return affectedLoc.clone().subtract(this.loc).toVector().normalize().multiply(this.blastPower);
    }

    public Vector getBlastVectorDefinite(Location affectedLoc) {
        if (this.loc.distanceSquared(affectedLoc) > this.radius * this.radius) return null;

        return affectedLoc.clone().subtract(this.loc).toVector().normalize().multiply(this.blastPower);
    }
}
