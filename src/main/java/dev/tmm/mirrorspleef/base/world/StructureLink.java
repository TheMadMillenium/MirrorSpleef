package dev.tmm.mirrorspleef.base.world;

import dev.tmm.mirrorspleef.util.MathUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class StructureLink {
    private World sourceWorld, linkWorld;

    private Location a, b;
    private CoordBoundary bounds;

    private boolean linkExists;
    private Location link;

    private StructureRotation rotation;

    public StructureLink(Location a, Location b) {
        if (a.getY() < 0 || b.getY() < 0) throw new IllegalArgumentException("Structure cannot exist below y=0");
        if (a.getY() > 255 || b.getY() > 255) throw new IllegalArgumentException("Structure cannot exist above y=255");
        if (a.getWorld() != b.getWorld()) throw new IllegalArgumentException("Locations must exist in the same world");

        this.sourceWorld = a.getWorld();

        this.a = a.clone();
        this.b = b.clone();

        this.a.setX(Math.min(this.a.getX(), this.b.getX()));
        this.a.setY(Math.min(this.a.getY(), this.b.getY()));
        this.a.setZ(Math.min(this.a.getZ(), this.b.getZ()));
        this.b.setX(Math.max(this.a.getX(), this.b.getX()));
        this.b.setY(Math.max(this.a.getY(), this.b.getY()));
        this.b.setZ(Math.max(this.a.getZ(), this.b.getZ()));

        this.bounds = new CoordBoundary(a, b);

        this.rotation = StructureRotation.NONE;
    }

    public void setLink(World world, Location link) {
        this.linkWorld = world;

        this.link = link.clone();

        this.verifyLink();

        this.linkExists = true;
    }

    public void setFlipAxis(StructureRotation rotation) {
        this.rotation = rotation;

        this.verifyLink();
    }

    public void syncStructures() {
        this.updateLinkBlocks();
    }

    public Location getA() {
        return this.a;
    }

    public Location getB() {
        return this.b;
    }

    public StructureRotation getRotation() {
        return this.rotation;
    }

    public Location coordsSrcToLink(Location in) {
        if (this.sourceWorld == null) throw new IllegalStateException("Structure has not been linked yet");
        if (!this.linkExists) throw new IllegalStateException("Structure has not been linked yet");

        double x = in.getX() - this.a.getX();
        double xOffs = (this.rotation == StructureRotation.X_AXIS ? 1 - x : x);

        double y = in.getY() - this.a.getY();
        double yOffs = (this.rotation != StructureRotation.NONE ? 1 - y : y);

        double z = in.getZ() - this.a.getZ();
        double zOffs = (this.rotation == StructureRotation.Z_AXIS ? 1 - z : z);

        return new Location(
                this.linkWorld,
                this.link.getX() + xOffs,
                this.link.getY() + yOffs,
                this.link.getZ() + zOffs,
                this.rotation == StructureRotation.Z_AXIS ? 180 + in.getYaw() : in.getYaw(),
                in.getPitch()
        );
    }

    public Location coordsLinkToSrc(Location in) {
        if (this.sourceWorld == null) throw new IllegalStateException("Structure has not been linked yet");
        if (!this.linkExists) throw new IllegalStateException("Structure has not been linked yet");

        double x = in.getX() - this.link.getX();
        double xOffs = (this.rotation == StructureRotation.X_AXIS ? 1 - x : x);

        double y = in.getY() - this.link.getY();
        double yOffs = (this.rotation != StructureRotation.NONE ? 1 - y : y);

        double z = in.getZ() - this.link.getZ();
        double zOffs = (this.rotation == StructureRotation.Z_AXIS ? 1 - z : z);

        return new Location(
                this.linkWorld,
                this.a.getX() + xOffs,
                this.a.getY() + yOffs,
                this.a.getZ() + zOffs,
                this.rotation == StructureRotation.Z_AXIS ? 180 + in.getYaw() : in.getYaw(),
                in.getPitch()
        );
    }

    public Location getLinkLocation() {
        return this.link;
    }

    public void onBlockBreak(Block b) {
        if (this.isInSrc(b.getLocation())) {
            this.updateLinkBlock(b, true);
            return;
        }

        if (this.isInLink(b.getLocation())) {
            this.updateSourceBlock(b, true);
        }
    }

    public void onBlockPlace(Block b) {
        Material placedBlockType = b.getType();

        switch (placedBlockType) {
            case SAPLING:
            case WATER:
            case STATIONARY_WATER:
            case LAVA:
            case STATIONARY_LAVA:
            case SAND:
            case GRAVEL:
            case POWERED_RAIL:
            case DETECTOR_RAIL:
            case LONG_GRASS:
            case DEAD_BUSH:
            case YELLOW_FLOWER:
            case RED_ROSE:
            case BROWN_MUSHROOM:
            case RED_MUSHROOM:
            case TORCH:
            case FIRE:
            case REDSTONE_WIRE:
            case CROPS:
            case SIGN_POST:
            case WOODEN_DOOR:
            case LADDER:
            case RAILS:
            case WALL_SIGN:
            case LEVER:
            case STONE_PLATE:
            case IRON_DOOR_BLOCK:
            case WOOD_PLATE:
            case REDSTONE_TORCH_OFF:
            case REDSTONE_TORCH_ON:
            case STONE_BUTTON:
            case SNOW:
            case CACTUS:
            case SUGAR_CANE_BLOCK:
            case CAKE_BLOCK:
            case DIODE_BLOCK_OFF:
            case DIODE_BLOCK_ON:
            case TRAP_DOOR:
            case MELON_BLOCK:
            case PUMPKIN_STEM:
            case MELON_SEEDS:
            case VINE:
            case WATER_LILY:
            case NETHER_WARTS:
            case DRAGON_EGG:
            case COCOA:
            case TRIPWIRE_HOOK:
            case FLOWER_POT:
            case CARROT:
            case POTATO:
            case WOOD_BUTTON:
            case SKULL:
            case ANVIL:
            case GOLD_PLATE:
            case IRON_PLATE:
            case REDSTONE_COMPARATOR_OFF:
            case REDSTONE_COMPARATOR_ON:
            case ACTIVATOR_RAIL:
            case CARPET:
            case DOUBLE_PLANT:
            case STANDING_BANNER:
            case WALL_BANNER:
            case SPRUCE_DOOR:
            case BIRCH_DOOR:
            case JUNGLE_DOOR:
            case ACACIA_DOOR:
            case DARK_OAK_DOOR:
            case BED:
                return;
        }

        if (this.isInSrc(b.getLocation())) {
            this.updateLinkBlock(b, false);
            return;
        }

        if (this.isInLink(b.getLocation())) {
            this.updateSourceBlock(b, false);
        }
    }

    public boolean isInSrc(Location loc) {
        int srcXDiff = this.b.getBlockX() - this.a.getBlockX();
        int srcYDiff = this.b.getBlockY() - this.a.getBlockY();
        int srcZDiff = this.b.getBlockZ() - this.a.getBlockZ();

        double placeX = loc.getX();
        double placeY = b.getY();
        double placeZ = b.getZ();

        return MathUtils.inBounds(
                placeX, placeY, placeZ,
                this.a.getBlockX(), this.a.getBlockY(), this.a.getBlockZ(),
                this.a.getBlockX() + srcXDiff, this.a.getBlockY() + srcYDiff, this.a.getBlockZ() + srcZDiff
        );
    }

    public boolean isInLink(Location loc) {
        int srcXDiff = this.b.getBlockX() - this.a.getBlockX();
        int srcYDiff = this.b.getBlockY() - this.a.getBlockY();
        int srcZDiff = this.b.getBlockZ() - this.a.getBlockZ();

        double placeX = loc.getX();
        double placeY = loc.getY();
        double placeZ = loc.getZ();

        int compareX = this.link.getBlockX();
        int compareY = this.link.getBlockY();
        int compareZ = this.link.getBlockZ();

        switch (this.rotation) {
            case X_AXIS:
                placeX = -placeX;
                placeY = -placeY;

                compareX = -compareX;
                compareY = -compareY;

                break;
            case Z_AXIS:
                placeZ = -placeZ;
                placeY = -placeY;

                compareZ = -compareZ;
                compareY = -compareY;

                break;
        }

        return MathUtils.inBounds(
                placeX, placeY, placeZ,
                compareX, compareY, compareZ,
                compareX + srcXDiff, compareY + srcYDiff, compareZ + srcZDiff
        );
    }

    @SuppressWarnings("deprecation")
    private void updateLinkBlock(Block b, boolean removeBlock) {
        if (removeBlock) b.setType(Material.AIR);

        int x = b.getX() - this.a.getBlockX();
        int xOffs = (this.rotation == StructureRotation.X_AXIS ? -x : x);

        int y = b.getY() - this.a.getBlockY();
        int yOffs = (this.rotation != StructureRotation.NONE ? -y : y);

        int z = b.getZ() - this.a.getBlockZ();
        int zOffs = (this.rotation == StructureRotation.Z_AXIS ? -z : z);

        Block srcBlock = this.sourceWorld.getBlockAt(
                this.a.getBlockX() + x, this.a.getBlockY() + y, this.a.getBlockZ() + z
        );

        Block linkBlock = this.linkWorld.getBlockAt(
                this.link.getBlockX() + xOffs, this.link.getBlockY() + yOffs, this.link.getBlockZ() + zOffs
        );

        linkBlock.setType(srcBlock.getType());
        linkBlock.setData(this.getRotatedBlock(srcBlock.getType(), srcBlock.getData()));
    }

    @SuppressWarnings("deprecation")
    private void updateSourceBlock(Block b, boolean removeBlock) {
        if (removeBlock) b.setType(Material.AIR);

        int x = b.getX() - this.link.getBlockX();
        int xOffs = (this.rotation == StructureRotation.X_AXIS ? -x : x);

        int y = b.getY() - this.link.getBlockY();
        int yOffs = (this.rotation != StructureRotation.NONE ? -y : y);

        int z = b.getZ() - this.link.getBlockZ();
        int zOffs = (this.rotation == StructureRotation.Z_AXIS ? -z : z);

        Block srcBlock = this.linkWorld.getBlockAt(this.link.getBlockX() + x, this.link.getBlockY() + y, this.link.getBlockZ() + z);
        Block linkBlock = this.sourceWorld.getBlockAt(this.a.getBlockX() + xOffs, this.a.getBlockY() + yOffs, this.a.getBlockZ() + zOffs);

        linkBlock.setType(srcBlock.getType());
        linkBlock.setData(this.getRotatedBlock(srcBlock.getType(), srcBlock.getData()));
    }

    @SuppressWarnings("deprecation")
    private void updateLinkBlocks() {
        int srcXDiff = this.b.getBlockX() - this.a.getBlockX();
        int srcYDiff = this.b.getBlockY() - this.a.getBlockY();
        int srcZDiff = this.b.getBlockZ() - this.a.getBlockZ();

        for (int x = 0; x <= srcXDiff; x++) {
            int xOffs = (this.rotation == StructureRotation.X_AXIS ? -x : x);

            for (int y = 0; y <= srcYDiff; y++) {
                int yOffs = (this.rotation != StructureRotation.NONE ? -y : y);

                for (int z = 0; z <= srcZDiff; z++) {

                    int zOffs = (this.rotation == StructureRotation.Z_AXIS ? -z : z);

                    Block srcBlock = this.sourceWorld.getBlockAt(this.a.getBlockX() + x, this.a.getBlockY() + y, this.a.getBlockZ() + z);
                    Block linkBlock = this.linkWorld.getBlockAt(this.link.getBlockX() + xOffs, this.link.getBlockY() + yOffs, this.link.getBlockZ() + zOffs);

                    linkBlock.setType(srcBlock.getType());
                    linkBlock.setData(this.getRotatedBlock(srcBlock.getType(), srcBlock.getData()));
                }
            }
        }
    }

    private byte getRotatedBlock(Material type, byte data) {
        if (this.rotation == StructureRotation.NONE) return data;

        switch (type) {
            case STEP:
            case WOOD_STEP:
            case STONE_SLAB2:
                data ^= 0x8;
                break;
            case ACACIA_STAIRS:
            case SANDSTONE_STAIRS:
            case BIRCH_WOOD_STAIRS:
            case SMOOTH_STAIRS:
            case BRICK_STAIRS:
            case SPRUCE_WOOD_STAIRS:
            case COBBLESTONE_STAIRS:
            case DARK_OAK_STAIRS:
            case JUNGLE_WOOD_STAIRS:
            case NETHER_BRICK_STAIRS:
            case QUARTZ_STAIRS:
            case RED_SANDSTONE_STAIRS:
            case WOOD_STAIRS:
                data ^= 0x4;
                break;
            case PISTON_BASE:
            case PISTON_EXTENSION:
            case PISTON_MOVING_PIECE:
            case PISTON_STICKY_BASE:
            case DISPENSER:
            case DROPPER:
                if ((data & 0x7) == 0 || (data & 0x7) == 1) data ^= 1;
                break;
        }

        switch (type) {
            case JACK_O_LANTERN:
            case PUMPKIN:
                if (this.rotation == StructureRotation.X_AXIS) {
                    if ((data & 0x3) == 1 || (data & 0x3) == 3) data ^= 2;
                } else {
                    if ((data & 0x3) == 0 || (data & 0x3) == 2) data ^= 2;
                }
                break;
            case ACACIA_STAIRS:
            case SANDSTONE_STAIRS:
            case BIRCH_WOOD_STAIRS:
            case SMOOTH_STAIRS:
            case BRICK_STAIRS:
            case SPRUCE_WOOD_STAIRS:
            case COBBLESTONE_STAIRS:
            case DARK_OAK_STAIRS:
            case JUNGLE_WOOD_STAIRS:
            case NETHER_BRICK_STAIRS:
            case QUARTZ_STAIRS:
            case RED_SANDSTONE_STAIRS:
            case WOOD_STAIRS:
                if (this.rotation == StructureRotation.X_AXIS) {
                    if ((data & 0x3) == 0 || (data & 0x3) == 1) data ^= 1;
                } else {
                    if ((data & 0x3) == 2 || (data & 0x3) == 3) data ^= 1;
                }
                break;
            case CHEST:
            case ENDER_CHEST:
            case TRAPPED_CHEST:
            case PISTON_BASE:
            case PISTON_EXTENSION:
            case PISTON_MOVING_PIECE:
            case PISTON_STICKY_BASE:
            case DISPENSER:
            case DROPPER:
                if (this.rotation == StructureRotation.X_AXIS) {
                    if ((data & 0x7) == 4 || (data & 0x7) == 5) data ^= 1;
                } else {
                    if ((data & 0x7) == 2 || (data & 0x7) == 3) data ^= 1;
                }
                break;
        }

        return data;
    }

    private String verifyLink() {
        String error = null;

        if (this.link == null) return null;

        if (this.link.getY() < 0 || this.link.getY() + (
                this.rotation != StructureRotation.NONE ?
                        this.a.getY() - this.b.getY() : this.b.getY() - this.a.getY()
        ) < 0)
            error = "All blocks inside link must stay above y=0";
        if (this.link.getY() > 255 || this.link.getY() + (
                this.rotation != StructureRotation.NONE ?
                        this.a.getY() - this.b.getY() : this.b.getY() - this.a.getY()) > 255)
            error = "All blocks inside link must stay below y=255";

        int laX = this.link.getBlockX();
        int laY = this.link.getBlockY();
        int laZ = this.link.getBlockZ();
        int lbX = this.link.getBlockX() + (
                this.rotation == StructureRotation.X_AXIS ?
                        this.a.getBlockX() - this.b.getBlockX() : this.b.getBlockX() - this.a.getBlockX()
        );

        int lbY = this.link.getBlockY() + (
                this.rotation != StructureRotation.NONE ?
                        this.a.getBlockY() - this.b.getBlockY() : this.b.getBlockY() - this.a.getBlockY()
        );

        int lbZ = this.link.getBlockZ() + (
                this.rotation == StructureRotation.Z_AXIS ?
                        this.a.getBlockZ() - this.b.getBlockZ() : this.b.getBlockZ() - this.a.getBlockZ()
        );

        if (this.bounds.intersects(new CoordBoundary(this.bounds.getWorld(), laX, laY, laZ, lbX, lbY, lbZ)))
            error = "Link cannot intersect with the source";

        return error;
    }
}
