package dev.tmm.mirrorspleef.base.ui.chest;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class ChestGUIItem<T extends ChestGUIMenu> {
    private ItemStack stack;
    private T parent;

    ChestGUIItem(ItemStack stack) {
        this.stack = stack;
    }

    final void setParentMenu(T parent) {
        this.parent = parent;
    }

    public final ItemStack getItemStack() {
        return this.stack;
    }

    public final T getParent() {
        return this.parent;
    }

    public final void setItemStack(ItemStack stack) {
        this.stack = stack;
    }

    public abstract void onClick(Player p);
}
