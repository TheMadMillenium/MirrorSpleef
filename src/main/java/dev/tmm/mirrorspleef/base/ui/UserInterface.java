package dev.tmm.mirrorspleef.base.ui;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.Set;

public class UserInterface {
    private TitleDisplay titles;
    private JukeboxDisplay jbDisplay;

    public UserInterface() {
        this.titles = new TitleDisplay();
        this.jbDisplay = new JukeboxDisplay();
    }

    public void setTitleTimes(int fadeIn, int sustain, int fadeOut) {
        this.titles.setFadeIn(fadeIn);
        this.titles.setSustain(sustain);
        this.titles.setFadeOut(fadeOut);
    }

    public void displayTitle(String title, String subtitle, Player p) {
        this.titles.setTitle(title);
        this.titles.setSubtitle(subtitle);

        this.titles.sendPackets(p);
    }

    public void displayJukeboxMsg(String message, Player p) {
        this.jbDisplay.setMessage(message);

        this.jbDisplay.sendPackets(p);
    }

    public void playSound(Sound sound, float pitchFactor, float volumeFactor, Player p) {
        p.playSound(p.getLocation(), sound, pitchFactor, volumeFactor);
    }
}
