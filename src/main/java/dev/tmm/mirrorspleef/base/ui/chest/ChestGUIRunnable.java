package dev.tmm.mirrorspleef.base.ui.chest;

import org.bukkit.inventory.ItemStack;

public abstract class ChestGUIRunnable<T extends ChestGUIMenu> extends ChestGUIItem<T> {
    public ChestGUIRunnable(ItemStack stack) {
        super(stack);
    }
}
