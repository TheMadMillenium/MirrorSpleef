package dev.tmm.mirrorspleef.base.ui.chest;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

public class ChestGUIMenu implements Listener {
    private Map<Integer, ChestGUIItem> items;
    private Inventory inventory;

    public ChestGUIMenu(String title, int height, Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);

        this.items = new HashMap<>();
        this.inventory = Bukkit.createInventory(null, height * 9, title);
    }

    public final void open(Player p) {
        p.openInventory(this.inventory);
    }

    public final void close(Player p) {
        p.closeInventory();
    }

    public final void setItem(ChestGUIItem item, int x, int y) {
        if (x < 0 || x >= 9) {
            throw new IndexOutOfBoundsException("Cannot place item at x = " + x);
        }

        int index = x + y * 9;

        if (index < 0 || index > this.inventory.getSize()) {
            throw new IndexOutOfBoundsException("Cannot place item at y = " + y);
        }

        this.items.put(index, item);
        this.inventory.setItem(index, item.getItemStack());
    }

    public final void clearItems() {
        this.inventory.clear();
    }

    @EventHandler
    public void onPlayerInventoryInteract(InventoryClickEvent e) {
        if(!this.inventory.equals(e.getInventory())) return;

        e.setCancelled(true);

        switch (e.getAction()) {
            case PICKUP_ALL:
            case PICKUP_HALF:
                if (!this.inventory.equals(e.getClickedInventory())) return;

                ChestGUIItem item = this.items.get(e.getSlot());
                if (item == null) return;

                item.onClick((Player) e.getWhoClicked());
        }
    }
}
