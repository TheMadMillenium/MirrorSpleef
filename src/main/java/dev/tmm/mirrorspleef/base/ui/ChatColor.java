package dev.tmm.mirrorspleef.base.ui;

import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.ChatModifier;
import net.minecraft.server.v1_8_R3.EnumChatFormat;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;

import java.util.HashMap;
import java.util.Map;

public enum ChatColor {
    BLACK('0'),
    DARK_BLUE('1'),
    DARK_GREEN('2'),
    DARK_AQUA('3'),
    DARK_RED('4'),
    DARK_PURPLE('5'),
    GOLD('6'),
    GRAY('7'),
    DARK_GRAY('8'),
    BLUE('9'),
    GREEN('a'),
    AQUA('b'),
    RED('c'),
    LIGHT_PURPLE('d'),
    YELLOW('e'),
    WHITE('f'),
    OBFUSCATED('k'),
    BOLD('l'),
    STRIKETHROUGH('m'),
    UNDERLINE('n'),
    ITALIC('o'),
    RESET('r');

    private static final Map<Character, ChatColor> LOOKUP = new HashMap<>();

    static {
        for (ChatColor c : values()) {
            LOOKUP.put(c.formattingCode, c);
        }
    }

    private char formattingCode;

    ChatColor(char formattingCode) {
        this.formattingCode = formattingCode;
    }

    public String toString() {
        return "\u00a7" + this.formattingCode;
    }

    public static String stripRedundancies(String redundantString) {
        StringBuilder out = new StringBuilder(redundantString);

        char lastChar = '\u00a7';
        boolean affectedTextExists = false;

        boolean changeLastChar = true;

        for (int i = out.length() - 1; i >= 0; i--) {
            if (out.charAt(i) == '\u00a7') {
                switch (lastChar) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                    case 'r':
                        if (!affectedTextExists) {
                            out.replace(i, i + 2, "");
                            changeLastChar = false;
                        }

                        affectedTextExists = false;

                        break;
                }
            } else {
                if (lastChar != '\u00a7') affectedTextExists = true;
            }

            if (changeLastChar) lastChar = out.charAt(i);
            else lastChar = '\u00a7';

            changeLastChar = true;
        }

        return out.toString();
    }

    public static IChatBaseComponent getAdjustedStr(String formattedString) {
        formattedString = stripRedundancies(formattedString);

        formattedString += ChatColor.RESET;

        IChatBaseComponent out = null;

        ChatModifier style = new ChatModifier();

        char val;

        for (int i = 0; i < formattedString.length(); i++) {
            if (formattedString.charAt(i) == '\u00a7') {
                switch (val = formattedString.charAt(++i)) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                        style.setBold(false);
                        style.setItalic(false);
                        style.setRandom(false);
                        style.setStrikethrough(false);
                        style.setUnderline(false);
                        style.setColor(EnumChatFormat.valueOf(fromCode(val).name()));

                        break;
                    case 'k':
                        style.setRandom(true);
                        break;
                    case 'l':
                        style.setBold(true);
                        break;
                    case 'm':
                        style.setStrikethrough(true);
                        break;
                    case 'n':
                        style.setUnderline(true);
                        break;
                    case 'o':
                        style.setItalic(true);
                        break;
                    case 'r':
                        style = new ChatModifier();
                        break;
                }
            } else {
                if (out == null) {
                    out = new ChatComponentText(
                            formattedString.substring(i, i = formattedString.indexOf('\u00a7', i + 1))
                    ).setChatModifier(style);
                } else {
                    out.addSibling(new ChatComponentText(
                            formattedString.substring(i, i = formattedString.indexOf('\u00a7', i))
                    ).setChatModifier(style));
                }

                i--;
            }
        }

        return out;
    }

    public static ChatColor fromCode(char code) {
        return LOOKUP.get(code);
    }
}
