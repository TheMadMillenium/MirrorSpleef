package dev.tmm.mirrorspleef.base.ui.chest;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChestGUIMenuRedirect extends ChestGUIItem {
    private ChestGUIMenu linkedMenu;

    public ChestGUIMenuRedirect(ItemStack stack, ChestGUIMenu menu) {
        super(stack);
        this.linkedMenu = menu;
    }

    @Override
    public void onClick(Player p) {
        this.linkedMenu.open(p);
    }
}
