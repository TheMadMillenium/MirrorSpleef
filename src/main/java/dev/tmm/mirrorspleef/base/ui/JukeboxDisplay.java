package dev.tmm.mirrorspleef.base.ui;

import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JukeboxDisplay {
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public void sendPackets(Player p) {
        PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;

        connection.sendPacket(new PacketPlayOutChat(new ChatComponentText(this.message), (byte) 2));
    }
}
