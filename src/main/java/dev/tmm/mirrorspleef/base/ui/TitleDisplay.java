package dev.tmm.mirrorspleef.base.ui;

import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TitleDisplay {
    private String title;
    private String subtitle;

    private int fadeIn, sustain, fadeOut;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setFadeIn(int fadeIn) {
        if (this.fadeIn == fadeIn) return;
        this.fadeIn = fadeIn;
    }

    public void setSustain(int sustain) {
        if (this.sustain == sustain) return;
        this.sustain = sustain;
    }

    public void setFadeOut(int fadeOut) {
        if (this.fadeOut == fadeOut) return;
        this.fadeOut = fadeOut;
    }

    public void sendPackets(Player p) {
        PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;

        connection.sendPacket(new PacketPlayOutTitle(this.fadeIn, this.sustain, this.fadeOut));

        if (isEmpty(this.title) && isEmpty(this.subtitle)) return;

        if (!isEmpty(this.subtitle)) connection.sendPacket(
                new PacketPlayOutTitle(
                        PacketPlayOutTitle.EnumTitleAction.SUBTITLE,
                        ChatColor.getAdjustedStr(this.subtitle)
                )
        );


        connection.sendPacket(
                new PacketPlayOutTitle(
                        PacketPlayOutTitle.EnumTitleAction.TITLE,
                        ChatColor.getAdjustedStr(this.title == null ? "" : this.title)
                )
        );
    }

    private static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }
}
