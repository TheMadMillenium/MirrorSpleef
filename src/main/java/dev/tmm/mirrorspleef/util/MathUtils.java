package dev.tmm.mirrorspleef.util;

public class MathUtils {
    public static double clamp(double val, double min, double max) {
        if (val < min) return min;
        if (val > max) return max;
        return val;
    }

    public static double clampAngle(double angle, double midpoint, double diff) {
        double f = wrapAngle(midpoint - angle);

        if (f < -diff) f = -diff;
        if (f >= diff) f = diff;

        return midpoint - f;
    }

    public static double wrapAngle(double ang) {
        ang %= 360;
        if (ang >= 180) ang -= 360;
        if (ang < -180) ang += 360;

        return ang;
    }

    public static boolean inBounds(double x, double y, double z, int xMin, int yMin, int zMin, int xMax, int yMax, int zMax) {
        if (x < xMin) return false;
        if (y < yMin) return false;
        if (z < zMin) return false;
        if (x > xMax) return false;
        if (y > yMax) return false;
        if (z > zMax) return false;

        return true;
    }

    public static int floor(double value) {
        int i = (int) value;
        return value < i ? i - 1 : i;
    }
}
