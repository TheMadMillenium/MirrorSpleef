package dev.tmm.mirrorspleef.util;

public enum NBTTypes {
    END(0),
    BYTE(1),
    SHORT(2),
    INT(3),
    LONG(4),
    FLOAT(5),
    DOUBLE(6),
    BYTE_ARRAY(7),
    STRING(8),
    LIST(9),
    COMPOUND(10),
    INT_ARRAY(11);

    private final int type;

    NBTTypes(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
