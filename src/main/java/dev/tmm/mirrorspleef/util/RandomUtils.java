package dev.tmm.mirrorspleef.util;

import java.util.Random;

public class RandomUtils {
    private static final Random RANDOM = new Random();

    private RandomUtils() {}

    public static int getInt(int to) {
        return RANDOM.nextInt(to);
    }

    public static int getInt(int from, int to) {
        return RANDOM.nextInt(to - from) + from;
    }

    public static double getDouble(double to) {
        return RANDOM.nextDouble() * to;
    }

    public static double getDouble(double from, double to) {
        return RANDOM.nextDouble() * (to - from) + from;
    }
}
