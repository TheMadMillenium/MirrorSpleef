package dev.tmm.mirrorspleef.util;

import java.util.TreeMap;

//Source: http://stackoverflow.com/a/19759564
public class RomanNumeralFormat {
    private static final TreeMap<Integer, String> NUMERAL_MAP;

    private RomanNumeralFormat() {
    }

    public static String format(int number) {
        int n = NUMERAL_MAP.floorKey(number);

        if (number == n) return NUMERAL_MAP.get(number);

        return NUMERAL_MAP.get(n) + format(number - n);
    }

    static {
        NUMERAL_MAP = new TreeMap<>();
        NUMERAL_MAP.put(10, "X");
        NUMERAL_MAP.put(9, "IX");
        NUMERAL_MAP.put(5, "V");
        NUMERAL_MAP.put(4, "IV");
        NUMERAL_MAP.put(1, "I");
    }
}
