package dev.tmm.mirrorspleef.util.reflect;

import java.lang.reflect.Field;

public class FieldAccessor <T> {
    private Field field;

    FieldAccessor(Field field) {
        this.field = field;
        this.field.setAccessible(true);
    }

    public void set(Object src, T val) {
        try {
            this.field.set(src, val);
        } catch (IllegalAccessException ignored) {}
    }

    public T get(Object src) {
        try {
            return (T) this.field.get(src);
        } catch (IllegalAccessException ignored) {
            throw new RuntimeException("Cannot access field named \"" + this.field.getName());
        }
    }
}
