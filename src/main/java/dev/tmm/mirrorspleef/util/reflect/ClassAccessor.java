package dev.tmm.mirrorspleef.util.reflect;

public class ClassAccessor {
    private Class<?> internalClass;

    public ClassAccessor(Class<?> internalClass) {
        this.internalClass = internalClass;
    }

    public <T> FieldAccessor<T> getFieldAccessor(String fieldName) {
        try {
            return new FieldAccessor<>(internalClass.getDeclaredField(fieldName));
        } catch (NoSuchFieldException ignored) {
            throw new RuntimeException("No field name \"" + fieldName + "\" found");
        }

    }
}
