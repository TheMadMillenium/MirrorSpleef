package dev.tmm.mirrorspleef.util.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ObjectDissector<T> {
    private final Class<T> objectType;
    private final T object;

    public ObjectDissector(T object) {
        this.object = object;
        this.objectType = (Class<T>) object.getClass();
    }

    public Object getValue(String varName) {
        if (this.object == null) throw new IllegalStateException("No object was provided");

        try {
            Field f = this.objectType.getDeclaredField(varName);
            f.setAccessible(true);

            return f.get(this.object);
        } catch (IllegalAccessException e) {
            return null; // Should not happen. Most classes don't use SecurityManager to restrict reflection access.
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException("No field named \"" + varName + "\" exists in " + this.objectType.getSimpleName());
        }
    }

    public void setValue(String varName, Object val) {
        if (this.object == null) throw new IllegalStateException("No object was provided");

        try {
            Field f = this.objectType.getDeclaredField(varName);
            f.setAccessible(true);

            f.set(this.object, val);
        } catch (IllegalAccessException ignored) {
            // Should not happen. Most classes don't use SecurityManager to restrict reflection access.
        } catch (NoSuchFieldException e) {
            throw new IllegalArgumentException("No field named \"" + varName + "\" exists in " + this.objectType.getSimpleName());
        }
    }

    public Object invokeMethod(String methodName, Class<?>[] classes, Object... args) {
        if (this.object == null) throw new IllegalStateException("No object was provided");

        Method m;
        try {
            m = this.objectType.getDeclaredMethod(methodName, classes);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("No method named \"" + methodName + "\" exists in " + this.objectType.getSimpleName());
        }

        try {
            return m.invoke(this.object, args);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException ignored) {
            return null; // Should not happen. Most classes don't use SecurityManager to restrict reflection ccess.
        }
    }
}