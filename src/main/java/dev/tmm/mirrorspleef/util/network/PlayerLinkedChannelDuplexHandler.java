package dev.tmm.mirrorspleef.util.network;

import io.netty.channel.ChannelDuplexHandler;
import org.bukkit.entity.Player;

public class PlayerLinkedChannelDuplexHandler extends ChannelDuplexHandler {
    protected Player p;

    public PlayerLinkedChannelDuplexHandler(Player p) {
        this.p = p;
    }
}
