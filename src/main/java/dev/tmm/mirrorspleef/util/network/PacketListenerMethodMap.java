package dev.tmm.mirrorspleef.util.network;

import java.lang.reflect.Method;

public class PacketListenerMethodMap {
    public final Class<?> methodClass;
    public final Method method;

    public PacketListenerMethodMap(Method m) {
        this.methodClass = m.getDeclaringClass();
        this.method = m;
    }
}
