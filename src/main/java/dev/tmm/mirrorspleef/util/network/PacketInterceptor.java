package dev.tmm.mirrorspleef.util.network;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayInFlying;
import net.minecraft.server.v1_8_R3.PacketPlayOutMapChunk;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketInterceptor implements Listener {
    private Map<Class<? extends Packet>, List<PacketListenerMethodMap>> packetCallbacks;
    private Map<Class<?>, List<Object>> callbackObjects;
    private String packetInterceptorName;

    public PacketInterceptor(String pluginName) {
        this.packetCallbacks = new HashMap<>();
        this.callbackObjects = new HashMap<>();
        this.packetInterceptorName = "packet_interceptor_" + pluginName;
    }

    public final void registerCallback(Object listener) {
        Class<?> listenerClass = listener.getClass();

        if (this.callbackObjects.containsKey(listenerClass)) {
            this.callbackObjects.get(listenerClass).add(listener);
        } else {
            Method[] allMethods = listenerClass.getDeclaredMethods();

            for (Method m : allMethods) {
                Class<?>[] parameterTypes = m.getParameterTypes();

                if (m.isAnnotationPresent(PacketListener.class) && parameterTypes.length == 2) {
                    Class<?> packetParam = parameterTypes[0];
                    Class<?> playerParam = parameterTypes[1];

                    if (!Packet.class.isAssignableFrom(packetParam)) continue;
                    if (!Player.class.equals(playerParam)) continue;

                    List<PacketListenerMethodMap> methodMaps;

                    if (!this.packetCallbacks.containsKey(packetParam)) {
                        methodMaps = new ArrayList<>();
                        this.packetCallbacks.put((Class<? extends Packet>) packetParam, methodMaps);
                    } else {
                        methodMaps = this.packetCallbacks.get(packetParam);
                    }

                    methodMaps.add(new PacketListenerMethodMap(m));
                }
            }

            ArrayList<Object> list = new ArrayList<>();
            list.add(listener);

            this.callbackObjects.put(listenerClass, list);
        }
    }

    @EventHandler
    private void onPlayerJoin(PlayerJoinEvent e) {
        ChannelPipeline chPipeline = ((CraftPlayer) e.getPlayer())
                .getHandle()
                .playerConnection
                .networkManager
                .channel
                .pipeline();

        chPipeline.addBefore("packet_handler", this.packetInterceptorName, this.getNewHandler(e.getPlayer()));
    }

    private PlayerLinkedChannelDuplexHandler getNewHandler(Player p) {
        return new PlayerLinkedChannelDuplexHandler(p) {
            @Override
            public void channelRead(ChannelHandlerContext channelHandlerContext, Object packet) throws Exception {
                Class<?> packetClass = packet.getClass();


                if (!(packet instanceof PacketPlayInFlying)) {
                    System.out.println(packet);
                }

                List<PacketListenerMethodMap> methodMaps = PacketInterceptor.this.packetCallbacks.get(packetClass);

                if (methodMaps != null) {
                    for (PacketListenerMethodMap mMap : methodMaps) {
                        for (Object listener : PacketInterceptor.this.callbackObjects.get(mMap.methodClass)) {
                            try {
                                mMap.method.invoke(listener, packet, this.p);
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                super.channelRead(channelHandlerContext, packet);
            }

            @Override
            public void write(ChannelHandlerContext ctx, Object packet, ChannelPromise promise) throws Exception {
                Class<?> packetClass = packet.getClass();

                if(!(packet instanceof PacketPlayOutMapChunk)) {
                    System.out.println(packet);
                }

                List<PacketListenerMethodMap> methodMaps = PacketInterceptor.this.packetCallbacks.get(packetClass);

                if (methodMaps != null) {
                    for (PacketListenerMethodMap mMap : methodMaps) {
                        for (Object listener : PacketInterceptor.this.callbackObjects.get(mMap.methodClass)) {
                            try {
                                mMap.method.invoke(listener, packet, this.p);
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                super.write(ctx, packet, promise);
            }
        };
    }
}
