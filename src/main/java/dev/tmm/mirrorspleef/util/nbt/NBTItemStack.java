package dev.tmm.mirrorspleef.util.nbt;

import dev.tmm.mirrorspleef.util.NBTTypes;
import dev.tmm.mirrorspleef.util.reflect.ClassAccessor;
import dev.tmm.mirrorspleef.util.reflect.FieldAccessor;
import net.minecraft.server.v1_8_R3.*;
import net.minecraft.server.v1_8_R3.ItemStack;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.*;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.Collections;

public class NBTItemStack {
    private static final FieldAccessor<ItemStack> handleField;

    private org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack craftbukkitStack;
    private org.bukkit.inventory.meta.ItemMeta bukkitMeta;
    private net.minecraft.server.v1_8_R3.ItemStack nmsStack;
    private net.minecraft.server.v1_8_R3.NBTTagCompound nmsTag;

    public NBTItemStack(Material type) {
        this(type, 1, (short) 0);
    }

    public NBTItemStack(Material type, int amount) {
        this(type, amount, (short) 0);
    }

    public NBTItemStack(Material type, int amount, short damage) {
        this.craftbukkitStack = CraftItemStack.asCraftCopy(new org.bukkit.inventory.ItemStack(type, amount, damage));
        this.bukkitMeta = this.craftbukkitStack.getItemMeta();
        this.nmsStack = handleField.get(this.craftbukkitStack);
        this.nmsTag = this.nmsStack.getTag() == null ? new NBTTagCompound() : (NBTTagCompound) this.nmsStack.getTag().clone();
    }

    public NBTItemStack(org.bukkit.inventory.ItemStack stack) {
        if (stack == null) throw new IllegalArgumentException("stack cannot be null");

        this.craftbukkitStack = CraftItemStack.asCraftCopy(stack);
        this.bukkitMeta = this.craftbukkitStack.getItemMeta();
        this.nmsStack = handleField.get(this.craftbukkitStack);
        this.nmsTag = this.nmsStack.getTag() == null ? new NBTTagCompound() : (NBTTagCompound) this.nmsStack.getTag().clone();
    }

    public NBTItemStack setType(Material type) {
        this.craftbukkitStack.setType(type);

        return this;
    }

    public NBTItemStack setAmount(int amt) {
        this.craftbukkitStack.setAmount(amt);

        return this;
    }

    public NBTItemStack setDamage(short data) {
        this.craftbukkitStack.setDurability(data);

        return this;
    }

    public NBTItemStack setName(String name) {
        this.bukkitMeta.setDisplayName(name);

        return this;
    }

    public NBTItemStack setLore(String... lore) {
        if (lore.length == 0) this.bukkitMeta.setLore(null);
        else if (lore.length == 1) this.bukkitMeta.setLore(Collections.singletonList(lore[0]));
        else this.bukkitMeta.setLore(Arrays.asList(lore));

        return this;
    }

    public NBTItemStack addItemFlags(ItemFlag... flags) {
        this.bukkitMeta.addItemFlags(flags);

        return this;
    }

    public NBTItemStack removeItemFlags(ItemFlag... flags) {
        this.bukkitMeta.addItemFlags(flags);

        return this;
    }

    public NBTItemStack addEnchant(org.bukkit.enchantments.Enchantment e, short level) {
        this.bukkitMeta.addEnchant(e, level, true);
        return this;
    }

    public NBTItemStack removeEnchant(org.bukkit.enchantments.Enchantment e) {
        this.bukkitMeta.removeEnchant(e);
        return this;
    }

    public NBTItemStack setUnbreakable(boolean unbreakable) {
        this.nmsTag.setByte("Unbreakable", (byte) (unbreakable ? 1 : 0));
        return this;
    }

    public NBTItemStack setCanPlaceOn(Material... types) {
        NBTTagList canPlaceOn = this.nmsTag.getList("CanPlaceOn", NBTTypes.STRING.getType());

        for (Material m : types) {
            canPlaceOn.add(new NBTTagString(Item.REGISTRY.c(Item.getById(m.getId())).toString()));
        }

        this.nmsTag.set("CanPlaceOn", canPlaceOn);

        return this;
    }

    public NBTItemStack setCanDestroy(Material... types) {
        NBTTagList canDestroy = this.nmsTag.getList("CanDestroy", NBTTypes.STRING.getType());

        for (Material m : types) {
            canDestroy.add(new NBTTagString(Item.REGISTRY.c(Item.getById(m.getId())).toString()));
        }

        this.nmsTag.set("CanDestroy", canDestroy);

        return this;
    }

    public NBTItemStack setPotionEffect(PotionEffectType type, int duration, int amplifier) {
        NBTTagList potionEffects = this.nmsTag.getList("CustomPotionEffects", NBTTypes.COMPOUND.getType());

        NBTTagCompound potion = new NBTTagCompound();

        potion.setByte("Id", (byte) type.getId());
        potion.setByte("Amplifier", (byte) amplifier);
        potion.setInt("Duration", duration);

        potionEffects.add(potion);

        this.nmsTag.set("CustomPotionEffects", potionEffects);

        return this;
    }

    public NBTItemStack set(String key, byte val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagByte(val));
        } else {
            this.nmsTag.set(key, new NBTTagByte(val));
        }

        return this;
    }

    public NBTItemStack set(String key, short val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagShort(val));
        } else {
            this.nmsTag.set(key, new NBTTagShort(val));
        }

        return this;
    }

    public NBTItemStack set(String key, int val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagInt(val));
        } else {
            this.nmsTag.set(key, new NBTTagInt(val));
        }

        return this;
    }

    public NBTItemStack set(String key, long val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagLong(val));
        } else {
            this.nmsTag.set(key, new NBTTagLong(val));
        }

        return this;
    }

    public NBTItemStack set(String key, float val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagFloat(val));
        } else {
            this.nmsTag.set(key, new NBTTagFloat(val));
        }

        return this;
    }

    public NBTItemStack set(String key, double val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagDouble(val));
        } else {
            this.nmsTag.set(key, new NBTTagDouble(val));
        }

        return this;
    }

    public NBTItemStack set(String key, String val) {
        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            this.createParent(parent).set(valKey, new NBTTagString(val));
        } else {
            this.nmsTag.set(key, new NBTTagString(val));
        }

        return this;
    }

    public boolean isEqual(NBTItemStack other, String key) {
        return areEqual(this, other, key);
    }

    public org.bukkit.inventory.ItemStack getBukkitStack() {
        this.craftbukkitStack.setItemMeta(this.bukkitMeta);
        this.nmsTag.a(this.nmsStack.getTag() == null ? new NBTTagCompound() : this.nmsStack.getTag());
        this.nmsStack.setTag(this.nmsTag);

        return this.craftbukkitStack;
    }

    private static boolean areEqual(NBTItemStack a, NBTItemStack b, String key) {
        if (a == null) return b == null;

        int dotIdx = key.lastIndexOf('.');

        if (dotIdx != -1) {
            String parent = key.substring(0, dotIdx);
            String valKey = key.substring(dotIdx + 1);
            NBTTagCompound aParent = a.traverseKey(parent);
            NBTTagCompound bParent = b.traverseKey(parent);

            if (aParent == null) return bParent == null;
            if (bParent == null) return false;

            return areEqual(aParent.get(valKey), bParent.get(valKey));
        } else {
            return areEqual(a.nmsTag.get(key), b.nmsTag.get(key));
        }
    }

    private static boolean areEqual(NBTBase a, NBTBase b) {
        if (a == null) return b == null;

        return a.equals(b);
    }

    private NBTTagCompound traverseKey(String key) {
        String[] segments = key.split("\\.");

        NBTTagCompound out = this.nmsTag;

        for (String s : segments) {
            if (!out.hasKey(s)) return null;
            out = out.getCompound(s);
        }

        return out;
    }

    private NBTTagCompound createParent(String key) {
        String[] segments = key.split("\\.");

        NBTTagCompound out = this.nmsTag;

        for (int i = 0; i < segments.length; i++) {
            if (out.hasKey(segments[i])) {
                if (out.get(segments[i]).getTypeId() != NBTTypes.COMPOUND.getType()) {
                    throw new IllegalArgumentException("existing key is not a compound");
                }

                out = (NBTTagCompound) out.get(segments[i]);
            } else {
                NBTTagCompound newOut = new NBTTagCompound();
                out.set(segments[i], newOut);
                out = newOut;
            }
        }

        return out;
    }

    static {
        handleField = new ClassAccessor(CraftItemStack.class).getFieldAccessor("handle");
    }
}
